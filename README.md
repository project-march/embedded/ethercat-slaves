# 🤖 EtherCAT Slaves
[![Build Status](https://gitlab.com/project-march/embedded/ethercat-slaves/badges/main/pipeline.svg)](https://gitlab.com/project-march/embedded/ethercat-slaves/-/pipelines)

Repository for the General EtherCAT Slaves (GES) and the PDB EtherCAT Slave of the March exoskeleton.

The [DieBieSlave](https://github.com/DieBieEngineering/DieBieSlave) is used as the GES in the MARCH exoskeleton. The hardware of the PDB EtherCAT Slave is developed by the Project MARCH team.

Doxygen documentation can be found at [https://project-march.gitlab.io/embedded/ethercat-slaves](https://project-march.gitlab.io/embedded/ethercat-slaves)

# 📥 Installation
In order to build the code in this project you need to at least install [platformio](https://docs.platformio.org/en/latest/what-is-platformio.html).

There is an integration with most well known IDEs, so choose the download to your liking. However we suggest to use [VSCode](https://code.visualstudio.com/) or its fully open source little brother [VSCodium](https://vscodium.com/).

OPTIONAL: In order to build new documentation, the Doxygen project is used. To install Doxygen you either need [this](https://www.doxygen.nl/download.html) link for Windows. Or if you are on Linux and use the RPM (Fedora) of DEB (Ubuntu, Debian) packages, you can use your package manager.

For changing the EtherCAT files, this includes all files in the `esi/` folder, we used the [EtherCAT SDK](https://rt-labs.com/ethercat/ethercat-sdk/) by RT-Labs. Unfortunatly this is paid software, however this is the only EtherCAT editor which is usable with GNU/Linux. For this software you need a hardare absed license. A part of this software is also the master implementation of the EtherCAT network. This enables you to read all the data send from the slaves for easy debugging.

# 🛠️ Building
The project contains multiple [environments](https://docs.platformio.org/en/latest/projectconf/section_env.html#projectconf-section-env) for each slave. The environments you can choose are:

* `bp_ges`: Backpack GES
* `lll_ges`: Left lower leg GES
* `lul_ges`: Left upper leg GES
* `rll_ges`: Right lower leg GES
* `rul_ges`: Right upper leg GES
* `odrive_ges`: Slave on top of the ODrive motorcontroller
* `pdb`: Power distribution board slave
* `standalone_pdb`: Power distribution board, which does not use ethercat
* `native`: Runs all tests, not used for hardware

To build all of the projects you run:

    $ pio run

This will build all the projects sequentially. When you want to build one specific environment only you can use the `--environment` (`-e`) option:

    $ pio run -e <slave>

So, for example, to build the backpack GES you run `pio run -e bp_ges`.
To actually upload the project, connect the the GES you would like to upload to and use the `--target` (`-t`) option.

    $ pio run -e <slave> -t upload

There are some tests, defined in the `test` folder, these kan be run by using the `native` environment.

    $ pio test -e native

# 📘 Documentation
The documentation of this repository is based around the Doxygen. Doxygen automatically generates (realy simplistic) documentation of the code, however it is meant to be extended with comments around the code. This includes description of functions and their input variables. However a better explenation is desired. By creating special formatted comments around functions and classes, a more comprehensive documentation can be created.

We recommend to use modules, in order to create pages about specific libraries or files. These modules can be written about either namespaces and classes, in the code a combiantion of both is used. Also does it allow to add other sub-pages from different files, which helps to define create multi file documentation pages.

Every code block for documentation either needs to start with `/**` for multi line documentation, ending with `*/` or `///` for a single line. With specific commands the documentation can be created, see [this link](https://www.mitk.org/images/1/1c/BugSquashingSeminars$2013-07-17-DoxyReference.pdf) for an overview of most (usefull) commands. To aid you in writing comments fast, we recommend to use [this extension](https://marketplace.visualstudio.com/items?itemName=cschlosser.doxdocgen) in VSCode (or VSCodium).

## Example Documentation
```
/**
 * @defgroup module_name Module Name
 * @brief Short description of the module
 *
 * A larger description of the module with a lot of clear text about the subject and information 
 * to be conveyed.
 * @{
 */

/**
 * @class A
 * @brief Brief description of the class.
 *
 * More in depth information about the class in question.
 */
class A{
    /**
     * @brief Information about function.
     * @parameter input_variable Eplanation about parameter.
     * 
     * More about the function
     * 
     * @return Information about the returned value.
     */
    float function(int input_variable);
}
/// @}
```
All "functions" of Doxygen can be called with different starting characters, however in this repo we tried to stick with the `@`.
Some of the most used functions will be explained below, however know this is not an extensive or complete list.

Starting a `@defgroup` will create a module, we tried to create a module for most libraries. The first argument after the function is like the variable name of the group (this needs to be one consecutive string of letters), followed by the user friendly name (which is displayed in the documentation). In order to add documentation followed below the creation we add `@{` to open the addition and `@}` to close (at the end of the example). Sometimes items need to be added to the module which are not followed after the group definition or in another file, to still add these items to the group, we use `@ingroup` command followed by the variable name of the group.

The next part of documentation found in the example starts with the `@class` function, this defines the class, followed by the exact name of the class. In most of the documentation sections you will find the `@brief` function, this creates a brief description of the item in question (can be modules, classes, functions, etc.). These are meant to consist of only one line of text. However, often more information about a specific function or class is required, in this case you can write more text after a blank line. This text is formatted like markdown, so line breakes are only visible in the final documentation after a full empty line.

For functions there are some more specific functions available, like `@parameter` followed by the name of the parameter and aftwerwards a brief text about the parameter. Another prominent function is `@return`, giving the ability to write someting about the returned value of a function. To make documenting easier, the extension mentioned earlier will automatically create the correct functions if it detects a function. Sometimes it even fills in some of the documentation itself, with for example the constructor.
