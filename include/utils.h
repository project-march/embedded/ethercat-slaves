#ifndef UTILS_H
#define UTILS_H

/**
 * @defgroup utils Utils
 * @author J.Q. Bouman MARCH VI
 * @date July 2021
 * @brief Small utility functions which are not present in the Mbed framework.
 * 
 * # Introduction
 * Some small utlity functions are used in order to make the use of some other libraries easier.
 * These functions are not part of any specific library and can therefore be used in different situation.
 * 
 * # Functions
 * @{
 * 
 */


/**
 * @brief Remap the input value to a specific output range.
 * @param value Input value (between iMin and iMax).
 * @param iMin Minimum value of the input value.
 * @param iMax Maximum value of the input value.
 * @param oMin Minimum value of the output value.
 * @param oMax Maximum value of the output value.
 * @return Value between oMin and oMax.
 * 
 * The intended use of this function is for the current sensors of the PDB of MARCH VI.
 * Since the value retrieved from the external analog to digital converter does not provide a float value.
 * And a more logical place to implement such a function is outside the library to prevent code duplication.
 */
float remap(float value , float iMin, float iMax, float oMin, float oMax){
    return oMin + (value - iMin) * ((oMax - oMin) / (iMax - iMin));
}

/// @}


#endif // UTILS_H