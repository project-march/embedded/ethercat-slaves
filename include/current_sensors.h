#ifndef CURRENT_SENSORS_H
#define CURRENT_SENSORS_H

/**
 * @defgroup current_sensors Current Sensors
 * @author J.Q. Bouman MARCH VI
 * @date July 2021
 * @brief Contains definitions of important values for current sensors.
 * 
 * The values described here give the maximum and minimum values of the current sensors.
 * By utilizing a mapping function, the actual current can be calculated.
 * 
 * The following current sensors are implemented:
 * 
 * Sensor name | Link to module 
 * ----------- | ----------------
 * ACS723LLCTR | @ref acs723llctr 
 * ACS780KLRTR | @ref acs780klrtr 
 * @{
 */

/**
 * @defgroup acs723llctr ACS723LLCTR
 * @brief Values used for ACS723LLCTR-10AU current sensor. 
 * 
 * Dataseet: https://www.allegromicro.com/~/media/Files/Datasheets/ACS723-Datasheet.ashx
 * 
 * These current sensors are used for the low voltage nets on MARCH VI's PDB V2.0
 * @{
 */


/// @brief Minimum output voltage of the current sensor.
#define ACS723_MIN_VOLTAGE 0.5
/// @brief Maximum output voltage og the current sensor.
#define ACS723_MAX_VOLTAGE 4.5
/// @brief Minimum output value for 12-bit 5V ADC.
#define ACS723_MIN_VALUE_12BIT_5V ((ACS723_MIN_VOLTAGE / 5) * 4095)
/// @brief Maximum output value for 12-bit 5V ADC.
#define ACS723_MAX_VALUE_12BIT_5V ((ACS723_MAX_VOLTAGE / 5) * 4095)
/// @brief Minimum output value for Mbed.
#define ACS723_MIN_VALUE_MBED ACS723_MIN_VOLTAGE / 5
/// @brief Maximum output value for Mbed.
#define ACS723_MAX_VALUE_MBED ACS723_MAX_VOLTAGE / 5

/// @brief Minimum measureable current.
#define ACS723_MIN_CURRENT 0.0
/// @brief Maximum measureable current.
#define ACS723_MAX_CURRENT 10.0
/// @} acs723llctr

/**
 * @defgroup acs780klrtr ACS780KLRTR
 * @brief Values used for the ACS780KLRTR-150B-T current sensor.
 * 
 * Datasheet: https://www.micro-semiconductor.nl/datasheet/b2-ACS780KLRTR-150U-T.pdf
 * 
 * Sens_TA = 40mV/A.
 * 
 * Quiescent Output Voltage = Vcc/2.
 * 
 * With a range of -50A to 50A, the minimum and maximum value is `2.5 V +/- ((40 mV/A * 50 A ) / 1000 mV/V)`.
 * 
 * Resulting in 0.5V minimum and 4.5V maximum.
 * 
 * These current sensors are used for the high voltage nets on MARCH VI's PDB V2.0
 * @{
 */

 
/// @brief Minimum output voltage of the current sensor.
#define ACS780_MIN_VOLTAGE 0.5
/// @brief Maximum output voltage of the current sensor. See minimum for detailed calculation.
#define ACS780_MAX_VOLTAGE 4.5
/// @brief Minimum value based on a 12 bit ADC and 5V Vcc
#define ACS780_MIN_VALUE_12BIT_5V ((ACS780_MIN_VOLTAGE / 5) * 4095)
/// @brief Maximum value based on a 12 bit ADC and 5V Vcc
#define ACS780_MAX_VALUE_12BIT_5V ((ACS780_MAX_VOLTAGE / 5) * 4095)
/// @brief Minimum value based on the Mbed ADC (0 to 1)
#define ACS780_MIN_VALUE_MBED ACS780_MIN_VOLTAGE / 5
/// @brief Maximum value based on the Mbed ADC (0 to 1)
#define ACS780_MAX_VALUE_MBED ACS780_MAX_VOLTAGE / 5

/// @brief Minimum measurable current of the 050B current sensor variant.
#define ACS780_050B_MIN_CURRENT -50.0
/// @brief Maximum measurable current of the 050B current sensor variant.
#define ACS780_050B_MAX_CURRENT 50.0
/// @brief Minimum measurable current of the 100B current sensor variant.
#define ACS780_100B_MIN_CURRENT -100.0
/// @brief Maximum measurable current of the 100B current sensor variant.
#define ACS780_100B_MAX_CURRENT 100.0

/// @} acs780klrtr

/// @} current_sensors
#endif // CURRENT_SENSORS_H