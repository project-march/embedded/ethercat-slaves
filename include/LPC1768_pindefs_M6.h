#ifndef LPC_PINDEFS_
#define LPC_PINDEFS_
// Pindefs for MARCH 6 PDB

#define LPC_ECAT_MOSI (p5)
#define LPC_ECAT_MISO (p6)
#define LPC_ECAT_SCK (p7)
#define LPC_ECAT_SCS (p8)
#define LPC_ECAT_RST (p11)
#define LPC_ECAT_IRQ (p12)

#define LPC_LED1 (LED1)
#define LPC_LED2 (LED2)
#define LPC_LED3 (LED3)
#define LPC_LED4 (LED4)

#define LPC_TX (p9)
#define LPC_RX (p10)

#define LPC_ONOFFBUTTON_PRESSED (p19)
#define LPC_ONOFFBUTTON_LED (p25)
#define LPC_KEEP_PDB_ON (p18)

#define LPC_EMERGENCY_SWITCH_STATUS (p20)
#define LPC_EMERGENCY_SWITCH (p14)

#define LPC_LV1OKAY (p23)
#define LPC_LV2OKAY (p24)
#define LPC_LVON (p22)

#define LPC_I2C_SDA (p28)
#define LPC_I2C_SCL (p27)
#define LPC_I2C_INTERRUPT_HV_ON (p26)

#define LPC_PDB_CURRENT_SENSOR (p15)
#define LPC_HV_CURRENT_SENSOR (p17)

#define LPC_STOP_BUTTON_PRESSED (p16)

#endif  // LPC_PINDEFS_
