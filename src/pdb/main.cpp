#include <mbed.h>                  // Mbed framework
#include "LPC1768_pindefs_M6.h"    // Pin definitions for PDB MVI design
#include "PCA9534.h"               // IO extender for turning HV nets on/off
#include "ADS7828.h"               // ADC for current sensors
#include "utypes.h"                // EtherCAT structs
#include "Ethercat.h"              // EtherCAT communication library
#include "StateMachine.h"          // State machine for operating states of the PDB
#include "tinyBMS.h"               // BMS communication
#include "current_sensors.h"       // Curent sensors fixed values
#include "utils.h"                 // Utility functions (remap)
#include "SensorFilters.h"         // Filters out the outliers for LV current sensor

#define DEBUG
#define STAGED_STARTUP_WAIT_TIME 5E5 // In us (0.5s)
#define BOOT_TIME 2E6 // in us (2s)

#ifdef DEBUG
BufferedSerial pc(USBTX, USBRX, 9600);  // Serial communication for debugging

// Allows for printing in the console, over the USB connection 'pc'
FileHandle *mbed::mbed_override_console(int fd)
{
  return &pc;
}

Timer printTimer;  // Timer to print debug statements only once per second
#endif // DEBUG

// Different LEDs
DigitalOut onOffButtonLed(LPC_ONOFFBUTTON_LED, false);
DigitalOut mbedLed1(LPC_LED1, false); // Shows the keepPDBon state (LED on = PDB on)
DigitalOut mbedLed2(LPC_LED2, false); // Shows the LVon state (LED on = LVon)
DigitalOut mbedLed3(LPC_LED3, false); // Shows the HV state (LED on = all HV on)
DigitalOut mbedLed4(LPC_LED4, false); // Shows the desired state of the computer (not actually used)

// Low voltage related inputs/output
DigitalIn lv1Okay(LPC_LV1OKAY, PullDown);  // True means okay
DigitalIn lv2Okay(LPC_LV2OKAY, PullDown);  // True means okay

// Power control
DigitalOut emergencyButtonControl(LPC_EMERGENCY_SWITCH, true);          // Must be true to be used
PCA9534 hvControl(LPC_I2C_SDA, LPC_I2C_SCL);
DigitalOut keepPDBOn(LPC_KEEP_PDB_ON, false); // True means PDB stays on
DigitalOut lvOn(LPC_LVON, false); // True means LV net is on

// Buttons
DigitalIn emergencyButton(LPC_EMERGENCY_SWITCH_STATUS, PullDown);        // False means disconnected HV
DigitalIn stopButton(LPC_STOP_BUTTON_PRESSED, PullDown);  // This button is not used in the physical device
DigitalIn onOffButton(LPC_ONOFFBUTTON_PRESSED, PullDown);  // True means button(on off) pressed

// Current sensing related inputs/outputs
ADS7828 currentSensors(new I2C(LPC_I2C_SDA, LPC_I2C_SCL), false, false, true);
AnalogIn pdbCurrentSensor(LPC_PDB_CURRENT_SENSOR);
AnalogIn hvCurrentSensor(LPC_HV_CURRENT_SENSOR);

ExponentialMovingAverage lv2Filter(0.3f);

// EtherCAT
// Set PDO sizes
const int PDORX_size = 16;
const int PDOTX_size = 60;

// EtherCAT object
DigitalOut ecatReset(LPC_ECAT_RST, true);
DigitalIn ecatIRQ(LPC_ECAT_IRQ);

// Easy access to PDOs
#define miso Ethercat::pdoTx.Struct.miso
#define mosi Ethercat::pdoRx.Struct.mosi

#ifdef DEBUG
Ethercat ecat(LPC_ECAT_MOSI, LPC_ECAT_MISO, LPC_ECAT_SCK, LPC_ECAT_SCS, PDORX_size, PDOTX_size, &pc, 10);
#else
Ethercat ecat(LPC_ECAT_MOSI, LPC_ECAT_MISO, LPC_ECAT_SCK, LPC_ECAT_SCS, PDORX_size, PDOTX_size);
#endif // DEBUG

StateMachine stateMachine;                     // State machine object

tinyBMS_serial bms(new BufferedSerial(LPC_TX, LPC_RX), false);

// Retrieve all information from the HV and LV nets
void updateHVLV(){
  miso.LV1OK = lv1Okay.read();
  miso.LV2OK = lv2Okay.read();

  // Measure current sensors
  // HV current sensors
  miso.HV1Current = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH7), ACS780_MIN_VALUE_12BIT_5V, ACS780_MAX_VALUE_12BIT_5V, ACS780_050B_MIN_CURRENT, ACS780_050B_MAX_CURRENT);
  miso.HV2Current = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH6), ACS780_MIN_VALUE_12BIT_5V, ACS780_MAX_VALUE_12BIT_5V, ACS780_050B_MIN_CURRENT, ACS780_050B_MAX_CURRENT);
  miso.HV3Current = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH5), ACS780_MIN_VALUE_12BIT_5V, ACS780_MAX_VALUE_12BIT_5V, ACS780_050B_MIN_CURRENT, ACS780_050B_MAX_CURRENT);
  miso.HV4Current = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH4), ACS780_MIN_VALUE_12BIT_5V, ACS780_MAX_VALUE_12BIT_5V, ACS780_050B_MIN_CURRENT, ACS780_050B_MAX_CURRENT);
  miso.HVTotalCurrent = remap(hvCurrentSensor.read(), ACS780_MIN_VALUE_MBED, ACS780_MAX_VALUE_MBED, ACS780_100B_MIN_CURRENT, ACS780_100B_MAX_CURRENT);

  // LV current sensors
  miso.PDBCurrent = remap(pdbCurrentSensor.read(), ACS723_MIN_VALUE_MBED, ACS723_MAX_VALUE_MBED, ACS723_MIN_CURRENT, ACS723_MAX_CURRENT);
  miso.LV1Current = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH3), ACS723_MIN_VALUE_12BIT_5V, ACS723_MAX_VALUE_12BIT_5V, ACS723_MIN_CURRENT, ACS723_MAX_CURRENT);
  miso.LV2Current = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH2), ACS723_MIN_VALUE_12BIT_5V, ACS723_MAX_VALUE_12BIT_5V, ACS723_MIN_CURRENT, ACS723_MAX_CURRENT);
  // Prevent negative current from being used in filter (those are not valid anyway)
  if(miso.LV2Current > 0.0f){
    lv2Filter.add_value(miso.LV2Current);
  }
}

// Retrieve the button states for the EtherCAT train
void updateButtonStates(){
  miso.EmergencyButtonState = emergencyButton.read();
  if(!emergencyButton.read()){
    // Turn HV nets off
    hvControl.setAll(0);
  }
  miso.StopButton = stopButton.read();
}

// Retrieve information from BMS for the EtherCAT train
void updateBatteryStatus(){
  // Do not update when battery is not used (e.g. via PSU)
  if(bms.tinyBMS_connected()){
    miso.BatteryPercentage = bms.tinyBMS_estSOC();
    miso.BatteryVoltage = bms.tinyBMS_voltage();
    // Average two internal temperature
    miso.BatteryTemperature = (bms.tinyBMS_extTemp1() + bms.tinyBMS_extTemp2()) / 2.0;
    bms.tinyBMS_readData();
  }
}

// Turn the HV nets on 
void stagedStartup(){
  hvControl.setAll(0b00000001);
  wait_us(STAGED_STARTUP_WAIT_TIME);
  hvControl.setAll(0b00000011);
  wait_us(STAGED_STARTUP_WAIT_TIME);
  hvControl.setAll(0b00000111);
  wait_us(STAGED_STARTUP_WAIT_TIME);
  hvControl.setAll(0b00001111);
}

int main()
{
  wait_us(BOOT_TIME);
  #ifdef DEBUG
  printf("\r\nMBED gets power now!");
  printTimer.start();
  #endif // DEBUG

  // Set initial EtherCAT outputs
  miso.EmergencyButtonState = 0;  // Updated in: updateButtonStates()
  miso.PDBCurrent = 0;            // Updated in: updateHVLV()
  miso.HVTotalCurrent = 0;        // Updated in: updateBatteryStatus()
  miso.StopButton = 0;            // Updated in: updateButtonStates()
  miso.HV1Current = 0;            // Updated in: updateHVLV()
  miso.HV2Current = 0;            // Updated in: updateHVLV()
  miso.HV3Current = 0;            // Updated in: updateHVLV()
  miso.HV4Current = 0;            // Updated in: updateHVLV()
  miso.LV1Current = 0;            // Updated in: updateHVLV()
  miso.LV2Current = 0;            // Updated in: updateHVLV()
  miso.LV1OK = 0;                 // Updated in: updateHVLV()
  miso.LV2OK = 0;                 // Updated in: updateHVLV()
  miso.BatteryPercentage = 0;     // Updated in: updateBatteryStatus()
  miso.BatteryVoltage = 0;        // Updated in: updateBatteryStatus()
  miso.BatteryTemperature = 0;    // Updated in: updateBatteryStatus()

  while (1)
  {
    // Update EtherCAT variables
    ecat.update();

    // Information retrieval
    updateHVLV();
    updateButtonStates();
    updateBatteryStatus();

    // Update system state absed on the retrieved data
    stateMachine.updateState(onOffButton.read(), lv2Filter.get_current_value() > 0.20f, miso.LV1OK && miso.LV2OK);

    // Debug prints
    #ifdef DEBUG
    if (printTimer.read_ms() > 1000){  // Print once every x ms
      printf("PDB DEBUG:\n");

      printf("PDB main values:\n");
      printf("\tButtonPressed: %i \tKeepPDBon: %i \tEmergencyButton: %i \tEmergencyButtonControl: %i\n", onOffButton.read(), keepPDBOn.read(), emergencyButton.read(), emergencyButtonControl.read());
      printf("\tStopButtonPressed: %li\n", miso.StopButton);
      printf("\tState: %s ", stateMachine.getState().c_str());
      printf("\tPDB Current: %f\n\n", miso.PDBCurrent);

      printf("Low Voltage Net:\n");
      printf("\tLVon: %i \n\tLV1Okay: %li \n\tLV2Okay: %li", lvOn.read(), miso.LV1OK, miso.LV2OK);
      printf("\n\tLV1Current: %f \n\tLV2Current: %f\tLV2Current Filtered: %f\n\n", miso.LV1Current, miso.LV2Current, lv2Filter.get_current_value());

      printf("High Voltage Net\n");
      printf("\tHV states: %x\n", hvControl.readAllOn() & 0b00001111); // Mask out the LEDs
      printf("\tHV Currents:\n");
      printf("\t\t1: %f, 2: %f, 3: %f, 4: %f\n", miso.HV1Current, miso.HV2Current, miso.HV3Current, miso.HV4Current);

      printf("BMS\n");
      printf("Battery:\n");
      printf("\tPercentage (SoC): %f, Voltage: %f, Temperature: %f\n", miso.BatteryPercentage, miso.BatteryVoltage, miso.BatteryTemperature);
    
      printf("\n");
      printTimer.reset();
    }
    #endif // DEBUG

    // Update outputs based on State Machine
    onOffButtonLed = stateMachine.getOnOffButtonLedState();
    keepPDBOn = stateMachine.getKeepPDBOn();
    lvOn = stateMachine.getLVOn();
    if(stateMachine.getHVOn()){
      if(!emergencyButtonControl.read()){
        emergencyButtonControl = true;
      }else{
        if(emergencyButton.read() && ((hvControl.readAllOn() & 0b00001111) == 0)){
          stagedStartup();
        }
      }
    }

    // Not used for now:
    // stateMachine.getComputerOn()

    // Set LEDs
    mbedLed1 = keepPDBOn;      // LED on if button LED is on
    mbedLed2 = lvOn;  // LED on if Low Voltage nets are on
    // HV is on if the outputs are set to on and the control signal is set and the emergency button is not physically pressed
    mbedLed3 = (hvControl.readAllOn() & 0b00001111) && emergencyButtonControl.read() && emergencyButton.read();  // LED on if any HV is on
    mbedLed4 = stateMachine.getComputerOn();  // LED on if computer is on

    // Read data from EtherCAT master
    //emergencyButtonControl = mosi.EmergencyButtonControl > 0;
    // pin 456 on de hvControl IO extender are connected to the LEDs NOT USED ATM
    // In the current setup HV nets cannot be turned on or off, since SOEM will crash when a slave stops accidentally
    // masterShutdown allowed is not used due to missing implementation
  }
}