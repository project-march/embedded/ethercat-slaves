#ifndef __UTYPES_H__
#define __UTYPES_H__

#include <cc.h>

/* Inputs */
CC_PACKED_BEGIN
typedef struct
{
  CC_PACKED_BEGIN
  struct
  {
    uint32_t EmergencyButtonState;
    float PDBCurrent;
    float HVTotalCurrent;
    uint32_t StopButton;
    float HV1Current;
    float HV2Current;
    float HV3Current;
    float HV4Current;
    float LV1Current;
    float LV2Current;
    uint32_t LV1OK;
    uint32_t LV2OK;
    float BatteryPercentage;
    float BatteryVoltage;
    float BatteryTemperature;
  } CC_PACKED miso;
  CC_PACKED_END
} CC_PACKED _Rbuffer;
CC_PACKED_END

/* Outputs */
CC_PACKED_BEGIN
typedef struct
{
  CC_PACKED_BEGIN
  struct
  {
    uint32_t EmergencyButtonControl;
    uint32_t RGBLed;
    uint32_t HVControl;
    uint32_t MasterShutdownAllowed;
  } CC_PACKED mosi;
  CC_PACKED_END
} CC_PACKED _Wbuffer;
CC_PACKED_END

/* Parameters */
CC_PACKED_BEGIN
typedef struct
{
} CC_PACKED _Cbuffer;
CC_PACKED_END

/* Manufacturer specific data */
CC_PACKED_BEGIN
typedef struct
{
} CC_PACKED _Mbuffer;
CC_PACKED_END

extern _Rbuffer Rb;
extern _Wbuffer Wb;
extern _Cbuffer Cb;
extern _Mbuffer Mb;

#endif /* __UTYPES_H__ */
