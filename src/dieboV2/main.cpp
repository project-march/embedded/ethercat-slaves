#include <mbed.h>
// Pin definitions of the DieBieSlave (odrive_ges/DieBOSlave has the same pinout)
#include "DBS_pindefs.h"
// For communicating with ODrive via ASCII protocol
#include "ODriveASCII.h"
//Include the RTD to digital converter library
#include "max31865.h"
//Include the Orbis UART library
#include "OrbisUART.h"
//Include the Aksim UART library
#include "AksimUART.h"
//Include the custom Mantracourt UART library
// #include "MantracourtUART.h"

#include "axis.h"
#define DEBUG
// #define TORQUEDEBUG
//#define PRESSURE_SOLES 

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#include "esc.h"
#include "ecat_slv.h"
#include "utypes.h"

#ifdef __cplusplus
}
#endif // __cplusplus


/* CANopen Object Dictionary */
_Objects    Obj;

void cb_get_inputs()
{

}

void cb_set_outputs()
{

}

/* SOES configuration */
static esc_cfg_t config = { 
    .user_arg = NULL,
    .use_interrupt = 0,
    .watchdog_cnt = 500,
    .set_defaults_hook = NULL,
    .pre_state_change_hook = NULL,
    .post_state_change_hook = NULL,
    .application_hook = NULL,
    .safeoutput_override = NULL,
    .pre_object_download_hook = NULL,
    .post_object_download_hook = NULL,
    .rxpdo_override = NULL,
    .txpdo_override = NULL,
    .esc_hw_interrupt_enable = NULL,
    .esc_hw_interrupt_disable = NULL,
    .esc_hw_eep_handler = NULL,
    .esc_check_dc_handler = NULL,
};

#define PC_BAUDRATE (9600)
#define ODRIVE_BAUDRATE (460800)
#define WAIT_TIME (2000000)                    // micro-seconds
#define APP_TITLE "DieBOSlaveV2"  // Application name to be printed to terminal
#define ORBIS_UART_BAUDRATE 256000
#define MAX_COMMUNICATION_FAILS 5
#define STATE_CHANGE_RETRIES 1 // Maximum number of retries before giving up on changin the state.
//#define PRESSURE_SOLES



uint8_t DIEBOSLAVE_COMMAND_FLAG = 0x20;
enum DieBOSlaveCommand: uint8_t {
    CLEAR_ODRIVE_ERRORS     = 0x20,     // Clear all errors on the ODrive
    CLEAR_DIEBOSLAVE_ERRORS = 0x21,     // Clear all errors on the DieBOSlave
    CLEAR_ALL_ERRORS        = 0x22      // Clear all errors on the DieBOSlave and on the ODrive
};

//Different modes for the LED
enum LedMode: uint8_t {
  OFF = 0x0,
  ON = 0x1,
  BLINK_FAST = 0x2,
  BLINK_MEDIUM = 0x3,
  BLINK_SLOW = 0x4,
};

// Pressure Soles
#define PADS 9 //The amount of pads in the pressure soles
typedef struct sensors {
	DigitalOut *Pin;
	float raw;
  float old_raw = 0;
	float mvoltage;
	float voltage;
	float resistance;
  std::string name;
  sensors(DigitalOut *Pinname, std::string nameOfPad) {
    this->Pin = Pinname;
    this->name = nameOfPad;
  }
}; 

//Initializing all the pins for the pressure soles
DigitalOut RightHEEL_PIN(DBS_RightHeel, false);  
DigitalOut LeftHeel_PIN(DBS_LeftHeel, false); 
DigitalOut HALLUX_PIN(DBS_Hallux, false);   
DigitalOut MET1_PIN(DBS_Met1, false);  
DigitalOut MET3_PIN(DBS_Met3, false);  
DigitalOut RFIX_PIN(DBS_Rfix, false);  
DigitalOut TOES_PIN(DBS_Toes, false); 
DigitalOut MET5_PIN(DBS_Met5, false);  
DigitalOut ARCH_PIN(DBS_Arch, false); 

sensors RightHeel(&RightHEEL_PIN, "Rightheel");
sensors LeftHeel(&LeftHeel_PIN, "Leftheel");
sensors Hallux(&HALLUX_PIN, "Hallux");
sensors Met1(&MET1_PIN, "Met1");
sensors Met3(&MET3_PIN, "Met3");
sensors Rfix(&RFIX_PIN, "Rref");
sensors Toes(&TOES_PIN, "Toes");
sensors Met5(&MET5_PIN, "Met5");
sensors Arch(&ARCH_PIN, "Arch");
typedef enum {RIGHTHEEL, LEFTHEEL, MET1, HALLUX, MET3, RFIX, TOES, MET5, ARCH } pads;  
sensors* measurement [9] = {&RightHeel, &LeftHeel, &Met1, &Hallux, &Met3, &Rfix, &Toes, &Met5, &Arch}; //Initialization of all sensors and putting them in an array for easy looping
AnalogIn   ain(DBS_Com12); 

//Axis classes for storing the values for each axis
Axis axes[2] = {Axis(0), Axis(1)};
int32_t ODriveError;

//Communication with PC
#ifdef DEBUG
#if MBED_MAJOR_VERSION >= 6
BufferedSerial pc(USBTX, USBRX, PC_BAUDRATE);
FileHandle *mbed::mbed_override_console(int fd)
{
   return &pc;
}
#else
Serial pc(DBS_SWD_UART_TX, DBS_SWD_UART_RX, PC_BAUDRATE);
#endif
#endif // Debug
InterruptIn ecat_irq(DBS_ECAT_IRQ);
bool ecat_network_interrupt = false;

// Setup communication with ODrive via Unbuffered Serial
#if MBED_MAJOR_VERSION >= 6
ODriveASCII odrive = ODriveASCII(new BufferedSerial(DBS_ODRIVE_UART_TX, DBS_ODRIVE_UART_RX, ODRIVE_BAUDRATE));
#else
ODriveASCII odrive = ODriveASCII(new UARTSerial(DBS_ODRIVE_UART_TX, DBS_ODRIVE_UART_RX, ODRIVE_BAUDRATE));
#endif

//Absolute encoder classes
AksimUART M0_absolute(DBS_M0_UART_TX, DBS_M0_UART_RX, ORBIS_UART_BAUDRATE);
OrbisUART M1_absolute(DBS_M1_UART_TX, DBS_M1_UART_RX, ORBIS_UART_BAUDRATE);

//Troque sensor init - commented out in absence of torque sensor
// MantracourtUART torqueSensor(DBS_TORQUE_UART_TX, DBS_TORQUE_UART_RX, 115200);
// DigitalOut DE(DBS_TORQUE_DE, false);


//Green LED for showing DieBie status
DigitalOut DieBieLed(DBS_GREEN_LED, false);  // DieBieSlave
Ticker DieBieTicker;
void flipDieBieLed(){
  DieBieLed = !DieBieLed;
}
// Red LED for showing ODrive error status
DigitalOut ODriveLed(DBS_RED_LED, false);  // DieBieSlave
Ticker ODriveTicker;
void flipODriveLed(){
  ODriveLed = !ODriveLed;
}

#ifdef DEBUG
Timer t;
#endif

/**
 * @brief get all the values from the ODrive using UART serial communication
 */
void updateOdriveValues(){
  static int commFails = 0;
  bool commandSucces = odrive.customMARCHCommand(&axes[0].State, &axes[0].AnyOdriveError, &axes[0].MotorPosition,
                                                 &axes[0].Velocity, &axes[0].ActualCurrent, &axes[0].ODriveTemperature, &axes[0].Torque,
                                                 &axes[1].State, &axes[1].AnyOdriveError, &axes[1].MotorPosition, 
                                                 &axes[1].Velocity, &axes[1].ActualCurrent, &axes[1].ODriveTemperature, &axes[1].Torque);

  if(!commandSucces){
    commFails++;
  } else{
    commFails = 0;
  }
  for(Axis& axis : axes){
    if(axis.AnyOdriveError){
      odrive.customErrorCommand(axis.AxisNum, &axis.Error, &axis.MotorError, &axis.EncoderError, &axis.ControllerError);
    }
  }
  
  if(commFails > MAX_COMMUNICATION_FAILS){
    axes[0].setErrorvalue(true, DieBOSlaveError::NO_ODRIVE_COMMUNICATION);
    axes[1].setErrorvalue(true, DieBOSlaveError::NO_ODRIVE_COMMUNICATION);
  }
  else{
    axes[0].setErrorvalue(false, DieBOSlaveError::NO_ODRIVE_COMMUNICATION);
    axes[1].setErrorvalue(false, DieBOSlaveError::NO_ODRIVE_COMMUNICATION);
  }
}

void sendAbsPos(uint8_t axisnumber){
  if(axes[axisnumber].AbsPosition != 0){
    bool commandSucces = odrive.setAbsPostion(axisnumber, axes[axisnumber].AbsPosition);
    if(!commandSucces){
      axes[axes[axisnumber].AxisNum].setErrorvalue(true, DieBOSlaveError::NO_ABS_POS_SEND);
    } else{
       axes[axes[axisnumber].AxisNum].setErrorvalue(false, DieBOSlaveError::NO_ABS_POS_SEND);
    }
  }
  else{
    axes[axes[axisnumber].AxisNum].setErrorvalue(true, DieBOSlaveError::NO_ABS_POS_SEND);
  }
}

// void sendDebugMoment(float dummy_value){
//   for(Axis& axis : axes){
//     // if(axis.TargetTorque != 0){
//       bool commandSucces = odrive.setMoment(axis.AxisNum, dummy_value);
//       if(!commandSucces){
//         axes[axis.AxisNum].setErrorvalue(true, DieBOSlaveError::NO_TORQUE_SEND);
//       } else{
//         axes[axis.AxisNum].setErrorvalue(false, DieBOSlaveError::NO_TORQUE_SEND);
//       }
//     // }
//     // else{
//       // axes[axis.AxisNum].setErrorvalue(true, DieBOSlaveError::NO_TORQUE_SEND);
//     // }
//     // thread_sleep_for(3000);
//     // bool commandSucces = odrive.setMoment(axis.AxisNum, 0.0f);
//   }
// }

void sendPOSPID(uint8_t axisnumber){
  if(axes[axisnumber].PostionP != 0){ //TODO make sure this check makes sense
    bool commandSucces = odrive.setPositionPID(axisnumber, axes[axisnumber].PostionP, axes[axisnumber].PostionI, axes[axisnumber].PostionD);
    if(!commandSucces){
      axes[axisnumber].setErrorvalue(true, DieBOSlaveError::NO_PID_SEND);
    } else{
      axes[axisnumber].setErrorvalue(false, DieBOSlaveError::NO_PID_SEND);
    }
  }
  else{
    axes[axisnumber].setErrorvalue(true, DieBOSlaveError::NO_PID_SEND);
  }
}

void sendMomentPD(uint8_t axisnumber){
  if(axes[axisnumber].TorqueP != 0){ //TODO make sure this check makes sense
    bool commandSucces = odrive.setMomentPD(axisnumber, axes[axisnumber].TorqueP, axes[axisnumber].TorqueD);
    if(!commandSucces){
      axes[axisnumber].setErrorvalue(true, DieBOSlaveError::NO_PID_SEND);
    } else{
      axes[axisnumber].setErrorvalue(false, DieBOSlaveError::NO_PID_SEND);
    }
  }
  else{
    axes[axisnumber].setErrorvalue(true, DieBOSlaveError::NO_PID_SEND);
  }
}

/**
 * @brief Receive end stop values from ODrive
 * @param axis: Axis object containing all the values for each axis
 */
void ConfigureEncoders(Axis* axis) {
  int minEndstop = 0, maxEndstop = 0, encoderMode = 0;
  odrive.readParameter(axis->AxisNum, "encoder.config.min_pos_abs", &minEndstop);
  odrive.readParameter(axis->AxisNum, "encoder.config.max_pos_abs", &maxEndstop);
  odrive.readParameter(axis->AxisNum, "encoder.config.mode", &encoderMode);
  if(axis->AxisNum == 0){
    // Hardcode the limits of the rotational axis
    axis->setEncoderParameters(encoderMode, 343165, 707858);
  }
  else{
    // Harcode the limits of the linear axis
      axis->setEncoderParameters(encoderMode, 2215, 3361);
  }
}

/**
 * @brief Execute a DieBOSlave command
 * @param axis: Axis object to affect
 * @param command: Command to execute
 */
void executeDieBOSlaveCommand(Axis* axis, DieBOSlaveCommand command) {
  switch (command) {
    case DieBOSlaveCommand::CLEAR_ALL_ERRORS: {
      odrive.clearErrors();
      axis->clearDieBoslaveErrors();
    } break;
    case DieBOSlaveCommand::CLEAR_ODRIVE_ERRORS: {
      odrive.clearErrors();
    } break;
    case DieBOSlaveCommand::CLEAR_DIEBOSLAVE_ERRORS: {
      axis->clearDieBoslaveErrors();
    } break;
  }
}

/**
 * @brief Check whether the motor is ready, set errors if it is not.
 * @param axis: Axis object to check on
 * @return True if the motor is ready, false otherwise
 */
bool isMotorReady(Axis* axis) {
  int motor_is_calibrated;
  odrive.readParameter(axis->AxisNum, "motor.is_calibrated", &motor_is_calibrated);
  axis->setErrorvalue(!motor_is_calibrated, DieBOSlaveError::MOTOR_NOT_CALIBRATED);
  return motor_is_calibrated;
}


/**
 * @brief Check whether the encoder is ready, set errors if it is not.
 * @param axis: Axis object to check
 * @return True if the encoder is ready, false otherwise
 */
bool isEncoderReady(Axis* axis) {
  bool encoder_is_ready;
  odrive.readParameter(axis->AxisNum, "encoder.is_ready", &encoder_is_ready);
  axis->setErrorvalue(!encoder_is_ready, DieBOSlaveError::ENOCDER_NOT_READY);
  
  return encoder_is_ready;
}

/**
 * @brief Check whether an axis of the ODrive can go in closed loop control.
 * @param axis: Axis object to check 
 * @return Returns a boolean. 
 *         If true is returned, then the ODrive is ready for closed loop control.
 *         If false is returned, then the ODrive is not ready for closed loop control.
 */
bool isReadyForClosedLoopControl(Axis* axis) {
  bool motor_is_ready = isMotorReady(axis);  
  bool encoder_is_ready = isEncoderReady(axis);
  #ifdef DEBUG
  printf("Motor is ready: %i, encoder is ready: %i\n", motor_is_ready, encoder_is_ready);
  #endif
  return motor_is_ready && encoder_is_ready;
}

/**
 * @brief Check the requested state of the ODrive
 * @param axis: Axis object to check
 * @param current_state: Current state of the axis
 * @param requested_state: State to check
 * @return Returns a boolean. 
 *         If true is returned, then the state should be set on the ODrive.
 *         If false is returned, then the state should not be set on the ODrive.
 */
bool shouldODriveStateBeRequested(Axis* axis, uint32_t requested_state) {

  if (requested_state == axis->last_requested_state || requested_state == axis->State) { 
      return false;
  }

  #ifdef DEBUG
  printf("Checking requested state %u for axis %i, current state is %u\n", requested_state, axis->AxisNum, axis->State);
  #endif

  axis->last_requested_state = requested_state;

  if (requested_state & DIEBOSLAVE_COMMAND_FLAG) {
    executeDieBOSlaveCommand(axis, (DieBOSlaveCommand) requested_state);
    return false;
  }

  switch (requested_state) {
    case ODriveASCII::MotorState::UNDEFINED: return false;
    case ODriveASCII::MotorState::ENCODER_INDEX_SEARCH: {
      return !odrive.isEncoderIndexFound(axis->AxisNum);
    }
    case ODriveASCII::MotorState::CLOSED_LOOP_CONTROL: {
      //Todo: Send PID values torque and position here
      return isReadyForClosedLoopControl(axis);
    } 
    case ODriveASCII::MotorState::IDLE:
    case ODriveASCII::MotorState::STARTUP_SEQUENCE:
    case ODriveASCII::MotorState::FULL_CALIBRATION_SEQUENCE:
    case ODriveASCII::MotorState::MOTOR_CALIBRATION:
    case ODriveASCII::MotorState::SENSORLESS_CONTROL:
    case ODriveASCII::MotorState::ENCODER_OFFSET_CALIBRATION: {
      // No need to verify these states
      return true;
    }
    default:
      // Return false for undefined states
      printf("In fault state %u", requested_state);
      return false;
  }
}

void ecat_interrupt(){
  ecat_network_interrupt = true;
}

/**
 * @brief Changes the LED output of the DieBo
 * @param ledmode: The way the led should flash
 * @param led: digital output to affect 
 * @param ticker: timer used to change the led frequency
 * @param func: function needed to switch the led on and off
 */
void changeLed(LedMode ledmode, DigitalOut& led, Ticker& ticker, mbed::Callback<void ()> func){
  ticker.detach();
  switch (ledmode)
  {
  case LedMode::OFF:
    led = 0;
    break;
  case LedMode::ON:
    led =1;
    break;
  case LedMode::BLINK_SLOW:
    ticker.attach(func, 3s);
    break;
  case LedMode::BLINK_MEDIUM:
    ticker.attach(func, 1s);
    break;
  case LedMode::BLINK_FAST:
    ticker.attach(func, 250ms);
    break;
  default:
    break;
  }
}

/**
 * @brief: Sets the correct led mode for each led based on the errors
 */
void SetErrorLeds(){
  static LedMode currentStatus = LedMode::ON;
  LedMode newStatus = LedMode::ON; 
  if(axes[0].DieBOSlaveError || axes[1].DieBOSlaveError){
      if(!axes[1].DieBOSlaveError){
        newStatus = LedMode::BLINK_FAST;
      } else if (!axes[0].DieBOSlaveError){
        newStatus = LedMode::BLINK_MEDIUM;
      } else {
        newStatus = LedMode::BLINK_MEDIUM;
      }  
  }

  if(currentStatus != newStatus){
    changeLed(newStatus, DieBieLed, DieBieTicker, flipDieBieLed);
    currentStatus = newStatus;
  }

  static LedMode currentError = LedMode::OFF;
  LedMode newError = LedMode::OFF; 
  if(axes[0].AnyOdriveError || axes[1].AnyOdriveError){
      if(!axes[1].AnyOdriveError){
        newError = LedMode::BLINK_FAST;
      } else if (!axes[0].AnyOdriveError){
        newError = LedMode::BLINK_MEDIUM;
      } else{
        newError = LedMode::BLINK_MEDIUM;
      }  
  }

  if(currentError != newError){
    changeLed(newError, ODriveLed, ODriveTicker, flipODriveLed);
    currentError = newError;
  }
}

/**
 * @brief Uses the ADC of the STM32 chip to read out pressure soles sensor 
 */
void getPadVoltage(sensors *sensor){
  // Make the pin high so that the transistor opens.
  *(sensor->Pin) = true;
  // Get ADC value
  sensor->raw = ain;
  // Set GPIO pin low
  *(sensor->Pin) = false;
  // Checks if the value has changed otherwise it is not send to preserve UART utility and other bandwidth
  if(sensor->raw!=sensor->old_raw)
    {
   	// Map it from bit value to mili-volts. bit range is 0-4095 since STM32 has a 12-bit ADC
   	sensor->mvoltage = (3300*sensor->raw);
   	//Make it volts
   	sensor->voltage = sensor->mvoltage/1000.00;
   	//Calculate the resistance. Rref is here 6.2 kOhm. Yours may vary. Measure the resistance that you use to get accurate results
   	//sensor->resistance = ((3.3*6200)/(3.3-sensor->voltage))-6200;
    }
  // makes the new data old.
  sensor->old_raw=sensor->raw;    
}

/**
 * @brief: wirte all the values to the miso object which is send to the LAN chip
 */
void updateMiso(){
  Obj.TxPDO.Axis0AbsolutePosition = axes[0].AbsPosition;
  Obj.TxPDO.Axis0Current = axes[0].ActualCurrent;
  Obj.TxPDO.Axis0MotorVelocity = axes[0].Velocity;
  Obj.TxPDO.Axis0Error = axes[0].Error;
  Obj.TxPDO.Axis0MotorError = axes[0].MotorError;
  Obj.TxPDO.Axis0EncoderError = axes[0].EncoderError;
  Obj.TxPDO.Axis0ControllerError = axes[0].ControllerError;
  Obj.TxPDO.Axis0DieBoError = axes[0].DieBOSlaveError;
  Obj.TxPDO.Axis0State = axes[0].State;
  Obj.TxPDO.Axis0ODriveTemperature = axes[0].ODriveTemperature;
  Obj.TxPDO.Axis0MotorTemperature = axes[0].MotorTemperature;
  Obj.TxPDO.Axis0ShadowCount = axes[0].MotorPosition;
  Obj.TxPDO.Axis0Torque = axes[0].Torque;
  Obj.TxPDO.Axis1AbsolutePosition = axes[1].AbsPosition;
  Obj.TxPDO.Axis1Current = axes[1].ActualCurrent;
  Obj.TxPDO.Axis1MotorVelocity = axes[1].Velocity;
  Obj.TxPDO.Axis1Error = axes[1].Error;
  Obj.TxPDO.Axis1MotorError = axes[1].MotorError;
  Obj.TxPDO.Axis1EncoderError = axes[1].EncoderError;
  Obj.TxPDO.Axis1ControllerError = axes[1].ControllerError;
  Obj.TxPDO.Axis1DieBoError = axes[1].DieBOSlaveError;
  Obj.TxPDO.Axis1State = axes[1].State;
  Obj.TxPDO.Axis1ODriveTemperature = axes[1].ODriveTemperature;
  Obj.TxPDO.Axis1MotorTemperature = axes[1].MotorTemperature;
  Obj.TxPDO.Axis1ShadowCount = axes[1].MotorPosition;
  Obj.TxPDO.Axis1Torque = axes[1].Torque;
  Obj.TxPDO.OdriveError = ODriveError;
  Obj.TxPDO.Heel_right = measurement[RIGHTHEEL]->voltage; 
  Obj.TxPDO.Heel_left = measurement[LEFTHEEL]->voltage;
  Obj.TxPDO.Met1 = measurement[MET1]->voltage;
  Obj.TxPDO.Hallux = measurement[HALLUX]->voltage;
  Obj.TxPDO.Met3 = measurement[MET3]->voltage;
  Obj.TxPDO.Rfix = measurement[RFIX]->voltage;
  Obj.TxPDO.Toes = measurement[TOES]->voltage;
  Obj.TxPDO.Met5 = measurement[MET5]->voltage;
  Obj.TxPDO.Arch = measurement[ARCH]->voltage;
  axes[0].TargetTorque = Obj.RxPDO.Axis0TargetTorque;
  axes[0].TargetPosition = Obj.RxPDO.Axis0TargetPosition;
  axes[0].FuzzyTorque = Obj.RxPDO.Axis0FuzzyTorque;
  axes[0].FuzzyPosition = Obj.RxPDO.Axis0FuzzyPosition;
  axes[0].PostionP = Obj.RxPDO.Axis0PositionP;
  axes[0].PostionI = Obj.RxPDO.Axis0PositionI;
  axes[0].PostionD = Obj.RxPDO.Axis0PostionD;
  axes[0].TorqueP = Obj.RxPDO.Axis0TorqueP;
  axes[0].TorqueD = Obj.RxPDO.Axis0TorqueD;
  axes[0].RequestedState = Obj.RxPDO.Axis0RequestedState;
  axes[1].TargetTorque = Obj.RxPDO.Axis1TargetTorque;
  axes[1].TargetPosition = Obj.RxPDO.Axis1TargetPosition;
  axes[1].FuzzyTorque = Obj.RxPDO.Axis1FuzzyTorque;
  axes[1].FuzzyPosition = Obj.RxPDO.Axis1FuzzyPosition;
  axes[1].PostionP = Obj.RxPDO.Axis1PositionP;
  axes[1].PostionI = Obj.RxPDO.Axis1PositionI;
  axes[1].PostionD = Obj.RxPDO.Axis1PositionD;
  axes[1].TorqueP = Obj.RxPDO.Axis1TorqueP;
  axes[1].TorqueD = Obj.RxPDO.Axis1TorqueD;
  axes[1].RequestedState = Obj.RxPDO.Axis1RequestedState; 
}



int main() {
  wait_us(WAIT_TIME);
  // Print application title and compile information
  #ifdef DEBUG
  printf("\f\r\n%s\r\n------------------\r\nTime compiled = %s.\r\nDate = %s.\n", APP_TITLE, __TIME__, __DATE__);
  #endif
  DieBieLed = true;
  for(Axis axis: axes){
    executeDieBOSlaveCommand(&axis, (DieBOSlaveCommand) CLEAR_ODRIVE_ERRORS);
  }

  odrive.requestMARCHData();
  
  //Enable watchdog timer and attach interrupt
  ecat_slv_init(&config);
  //ecat.set_watchdog();
  ecat_irq.rise(&ecat_interrupt);

  //get the encoder mode and endstops from the odrive for each axis
  ConfigureEncoders(&axes[0]);
  ConfigureEncoders(&axes[1]);

  //Request position from absolute encoders
  M0_absolute.PositionRequest();
  M1_absolute.PositionRequest();

  // Update General information before sending
  updateOdriveValues();

    //update the absolute encoder position
  axes[0].updateAbsPos(M0_absolute.GetPosition());
  axes[1].updateAbsPos(M1_absolute.GetPosition());

  sendAbsPos(axes[0].AxisNum);
  sendAbsPos(axes[1].AxisNum);

  // TORQUEDEBUG LINE - this will send a dummy value (1.0f) for torque to an axis on startup
  // #ifdef TORQUEDEBUG
  // float dummy_value = 0.15f;
  // printf("Setting the debug torque to dummy value %f", dummy_value);
  // sendDebugMoment(dummy_value);
  //odrive.setFuzzy(axes[0].AxisNum, axes[0].TargetPosition, axes[0].TargetTorque, axes[0].FuzzyPosition, axes[0].FuzzyTorque);
  // #endif



  #ifdef DEBUG
  uint32_t timer_loop_counts = 0;
  uint32_t total_time = 0;
  #endif

  while(1){
    // printf("testtsetataerr");
    #ifdef DEBUG
    t.start();
    #endif

    odrive.requestMARCHData();
    
    ecat_slv();
    updateMiso();
    //Request position from absolute encoders
    M0_absolute.PositionRequest();
    M1_absolute.PositionRequest();

    //get all the values from the odrive
    updateOdriveValues();

    //update the absolute encoder position
    axes[0].updateAbsPos(M0_absolute.GetPosition());
    axes[1].updateAbsPos(M1_absolute.GetPosition());

    #ifdef PRESSURE_SOLES
    //update Pressure Soles readings
    for(sensors* pad : measurement){
      getPadVoltage(pad);
    }  
    //printf("The voltage of %")
    #endif



    //if a network interrupt happend set the axes to idle 
    if(ecat_network_interrupt){
      axes[0].setErrorvalue(true, DieBOSlaveError::NO_ETHERCAT_COMMUNICATION);
      axes[1].setErrorvalue(true, DieBOSlaveError::NO_ETHERCAT_COMMUNICATION);
      //axes[0].RequestedState = ODriveASCII::MotorState::IDLE;
      axes[1].RequestedState = ODriveASCII::MotorState::IDLE;

      //if both the axis are set to idle the network error can be removed since there is no danger anymore
      if(axes[0].State == ODriveASCII::MotorState::IDLE && axes[1].State == ODriveASCII::MotorState::IDLE){
        axes[0].setErrorvalue(false, DieBOSlaveError::NO_ETHERCAT_COMMUNICATION);
        axes[1].setErrorvalue(false, DieBOSlaveError::NO_ETHERCAT_COMMUNICATION);
        ecat_network_interrupt = false;          
      }
    }

    for(Axis& axis : axes){

      //change odrive state if necessary
      if (shouldODriveStateBeRequested(&axis, axis.RequestedState)) {
        #ifdef DEBUG
        printf("Changing state for axis 0 from %i to %i\n", Obj.TxPDO.Axis0State, Obj.RxPDO.Axis0RequestedState);
        #endif
        if((ODriveASCII::MotorState)(axis.RequestedState) == ODriveASCII::MotorState::CLOSED_LOOP_CONTROL){
          // switch in case of torque control:
          sendPOSPID(axis.AxisNum);
          sendMomentPD(axis.AxisNum);
          sendAbsPos(axis.AxisNum);
        }
        if(!odrive.setState(axis.AxisNum, (ODriveASCII::MotorState)(axis.RequestedState))) {
            axis.setErrorvalue(true, DieBOSlaveError::REQUEST_STATE_FAILED);
        }
        else {
            axis.setErrorvalue(false, DieBOSlaveError::REQUEST_STATE_FAILED);
        }
      }

      //Update the current input
      if(axis.DieBOSlaveError){
        // Always set current to 0 if there is an DieBo error
        // odrive.setMoment(axis.AxisNum, 0.0f);
      } else if(axis.State == ODriveASCII::MotorState::CLOSED_LOOP_CONTROL){
        // printf("The fuzzy settings gotten are: \n p: %f, \n p_W: %f, \n t: %f, \n t_W: %f \n",
        //   axes[0].TargetPosition, axes[0].FuzzyPosition, axes[0].TargetTorque, axes[0].FuzzyTorque);
        // odrive.setMoment(axis.AxisNum, axis.TargetTorque);

          odrive.setFuzzy(axis.AxisNum, axis.TargetPosition, axis.TargetTorque, axis.FuzzyPosition, axis.FuzzyTorque);
      }
      
    }

    //set the corrrect mode for the error leds
    SetErrorLeds();
    
    #ifdef DEBUG
    #if MBED_MAJOR_VERSION >= 6
    total_time += std::chrono::duration_cast<std::chrono::microseconds>(t.elapsed_time()).count();
    #else
    total_time += t.read_us();
    #endif
    t.reset();
    timer_loop_counts += 1;
    if (timer_loop_counts >= 100) {
      printf("There were %li milliseconds in %lu loop iterations, for an average of %li per loop\n", (uint32_t)(((float)total_time)/1000), timer_loop_counts, (uint32_t)((float)total_time)/(timer_loop_counts*1000));
      timer_loop_counts = 0;
      total_time = 0;
    }
    #endif
  }
}