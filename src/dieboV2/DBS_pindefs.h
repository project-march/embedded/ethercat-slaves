#ifndef DBS_PINDEFS_
#define DBS_PINDEFS_

#define DBS_GREEN_LED (PC_0)
#define DBS_RED_LED (PC_1)

#define DBS_SWD_UART_RX (PB_11)
#define DBS_SWD_UART_TX (PB_10)

#define DBS_M0_UART_RX (PA_10)
#define DBS_M0_UART_TX (PA_9)

#define DBS_M1_UART_RX (PC_11_ALT0) //UART4
#define DBS_M1_UART_TX (PC_10_ALT0)

#define DBS_ODRIVE_UART_RX (PA_3)
#define DBS_ODRIVE_UART_TX (PA_2)

#define DBS_TORQUE_UART_RX (PD_2)
#define DBS_TORQUE_UART_TX (PC_12)
#define DBS_TORQUE_DE (PC_4)

#define DBS_RESETODRIVE (PB_2)

#define DBS_ECAT_MOSI (PB_15)
#define DBS_ECAT_MISO (PB_14)
#define DBS_ECAT_SCK (PB_13)
#define DBS_ECAT_NCS (PB_12)
#define DBS_ECAT_IRQ (PC_8)

#define DBS_RightHeel (PC_2)
#define DBS_LeftHeel (PC_3)
#define DBS_Met1 (PB_9)
#define DBS_Hallux (PB_8)
#define DBS_Met3 (PB_7)
#define DBS_Rfix (PB_6)
#define DBS_Toes (PB_5)
#define DBS_Met5 (PB_4)
#define DBS_Arch (PB_3)
#define DBS_Com12 (PA_0)

#endif  // DBS_PINDEFS_
