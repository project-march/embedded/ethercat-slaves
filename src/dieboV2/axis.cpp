#include "axis.h"

Axis::Axis(uint8_t AxisNum) :
            AxisNum(AxisNum), AbsPosition(0), Error(0), MotorError(0), DieBOSlaveError(0), 
            EncoderError(0), ControllerError(0), State(0), MotorTemperature(0), ODriveTemperature(0), MotorPosition(0), ActualCurrent(0), Velocity(0),
            EncoderMode(0), MinPosAbs(0), MaxPosAbs(0), last_requested_state(0),  AnyOdriveError(false), Torque(0){};

void Axis::setErrorvalue(bool setError, int32_t Error){
    if(setError){
        this->DieBOSlaveError |= Error;
    }
    else{
        (this->DieBOSlaveError) &= ~Error;
    }
}
void Axis::updateAbsPos(int32_t AbsPos){
    //If encoder mode is 0 don't update absolute position
    // if(!EncoderMode){
    //     return;
    // }
    this->AbsPosition = AbsPos;
    //Set error values based on the measure position
    this->setErrorvalue(!IsPositionValid(), DieBOSlaveError::NO_ENCODER_DATA);
    this->setErrorvalue(IsPositionInEndstop(), DieBOSlaveError::JOINT_IN_ENDSTOP);
    return;
}

void Axis::setEncoderParameters(int EncoderMode, int MinPosAbs, int MaxPosAbs){
    this->EncoderMode = EncoderMode;
    this->MinPosAbs = MinPosAbs;
    this->MaxPosAbs = MaxPosAbs;
}

bool Axis::IsPositionValid(){
    return this->AbsPosition > 0;
}

bool Axis::IsPositionInEndstop(){
    return this->AbsPosition < this->MinPosAbs || this->AbsPosition > this->MaxPosAbs; 
}

void Axis::clearDieBoslaveErrors(){
    this->DieBOSlaveError = 0;
}