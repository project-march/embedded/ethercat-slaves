#include <cc.h>

enum DieBOSlaveError : int32_t {
  NONE                                    = 0x0,
  INVALID_AXIS                            = 0x1,
  REQUEST_STATE_FAILED                    = 0x2,
  NO_ENCODER_DATA                         = 0x4,
  ENCODER_ERROR                           = 0x8,
  ENCODER_WARNING                         = 0x10,
  JOINT_IN_ENDSTOP                        = 0x20,
  NO_ETHERCAT_COMMUNICATION               = 0x40,
  MOTOR_NOT_CALIBRATED                    = 0x80,
  ENOCDER_NOT_READY                       = 0x100,
  NO_ODRIVE_COMMUNICATION                 = 0x200,
  NO_ABS_POS_SEND                         = 0x400,
  NO_PID_SEND                             = 0x1000,
  NO_TORQUE_SEND                          = 0x2000,
};

//Axis interface to interact with variables for each axis
class Axis{
public:
    uint8_t AxisNum;
    int32_t AbsPosition;
    int EncoderMode;
    int MinPosAbs;
    int MaxPosAbs;
    uint32_t last_requested_state;
    float ActualCurrent;
    float Velocity;
    int32_t AnyOdriveError;
    uint32_t Error;
    uint32_t MotorError;
    uint32_t DieBOSlaveError;
    uint32_t EncoderError;
    uint32_t ControllerError;
    int32_t State;
    int32_t MotorPosition;
    float MotorTemperature;
    float ODriveTemperature;
    float Torque;
    float TargetTorque;
    float TargetPosition;
    float FuzzyTorque;
    float FuzzyPosition;
    float PostionP;
    float PostionI;
    float PostionD;
    float TorqueP;
    float TorqueD;
    uint32_t RequestedState;

    Axis(uint8_t AxisNum);

    /**
     * @brief Update the absolute position variable and do all the checks 
     * @param AbsPos: the integer position variable from the encoder
     */
    void updateAbsPos(int32_t AbsPos);

    /**
     * @brief Update the error value for the axis
     * @param setError: for true set error for false unset error
     * @param Error: Error value to update
     */
    void setErrorvalue(bool setError, int32_t Error);

    /**
     * @brief Set the encoder parameters gotten from the ODrive
     * @param EnoderMode: determines if the absolute encode is used for the axis
     * @param MinPosAbs: Minimum endstop
     * @param MaxPosAbs: maximum endtop
     */
    void setEncoderParameters(int EncoderMode, int MinPosAbs, int MaxPosAbs);

    /**
     * @brief Check whether the encoder sends a valid position
     * @return Returns a boolean indiciating whether the position is valid
     */
    bool IsPositionValid();

    /**
     * @brief Check whether the encoder is outside the valid limits
     * @return Returns a boolean indiciating whether the encoder is pre calibrated
     */
    bool IsPositionInEndstop();

    /**
     * @brief Clear all current errors from the DieBoSlave
     */
    void clearDieBoslaveErrors();

};