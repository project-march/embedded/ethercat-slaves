#ifndef __UTYPES_H__
#define __UTYPES_H__

#include <cc.h>

/* Inputs */
CC_PACKED_BEGIN
typedef struct
{
  CC_PACKED_BEGIN
  struct
  {
    int32_t Axis0ActualPosition;
    float Axis0ActualCurrent;
    float Axis0Velocity;
    uint32_t Axis0Error;
    uint32_t Axis0MotorError;
    uint32_t Axis0DieBOSlaveError;
    uint32_t Axis0EncoderError;
    uint32_t Axis0ControllerError;
    uint32_t Axis0State;
    float Axis0Temperature;
    int32_t Axis0MotorPosition;
    int32_t Axis1ActualPosition;
    float Axis1ActualCurrent;
    float Axis1Velocity;
    uint32_t Axis1Error;
    uint32_t Axis1MotorError;
    uint32_t Axis1DieBOSlaveError;
    uint32_t Axis1EncoderError;
    uint32_t Axis1ControllerError;
    uint32_t Axis1State;
    float Axis1Temperature;
    int32_t Axis1MotorPosition;
  } CC_PACKED miso;
  CC_PACKED_END
} CC_PACKED _Rbuffer;
CC_PACKED_END

/* Outputs */
CC_PACKED_BEGIN
typedef struct
{
  CC_PACKED_BEGIN
  struct
  {
    float Axis0TargetTorque;
    uint32_t Axis0RequestedState;
    float Axis1TargetTorque;
    uint32_t Axis1RequestedState;
  } CC_PACKED mosi;
  CC_PACKED_END
} CC_PACKED _Wbuffer;
CC_PACKED_END

/* Parameters */
CC_PACKED_BEGIN
typedef struct
{
} CC_PACKED _Cbuffer;
CC_PACKED_END

/* Manufacturer specific data */
CC_PACKED_BEGIN
typedef struct
{
} CC_PACKED _Mbuffer;
CC_PACKED_END

extern _Rbuffer Rb;
extern _Wbuffer Wb;
extern _Cbuffer Cb;
extern _Mbuffer Mb;

#endif /* __UTYPES_H__ */
