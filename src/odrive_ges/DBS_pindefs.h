#ifndef DBS_PINDEFS_
#define DBS_PINDEFS_

#define DBS_GREEN_LED (PB_15)
#define DBS_RED_LED (PC_6)

#define DBS_SWD_UART_RX (PA_10)
#define DBS_SWD_UART_TX (PA_9)

#define DBS_M0_UART_RX (PB_11)
#define DBS_M0_UART_TX (PB_10)

#define DBS_M1_UART_RX (PC_11_ALT0)
#define DBS_M1_UART_TX (PC_10_ALT0)

#define DBS_ODRIVE_UART_RX (PA_3)
#define DBS_ODRIVE_UART_TX (PA_2)

#define DBS_TEMP_SCK (PB_3)
#define DBS_TEMP_MISO (PB_4)
#define DBS_TEMP_MOSI (PB_5)
#define DBS_TEMP_SC1 (PC_0)
#define DBS_TEMP_SC2 (PC_2)
#define DBS_TEMP_DRDY1 (PC_1)
#define DBS_TEMP_DRDY2 (PC_3)

#define DBS_ECAT_MOSI (PA_7)
#define DBS_ECAT_MISO (PA_6)
#define DBS_ECAT_SCK (PA_5)
#define DBS_ECAT_NCS (PB_12)
#define DBS_ECAT_IRQ (PB_13)

#endif  // DBS_PINDEFS_