#include <mbed.h>
// Pin definitions of the DieBieSlave (odrive_ges/DieBOSlave has the same pinout)
#include "DBS_pindefs.h"
// For communicating with ODrive via ASCII protocol
#include "ODriveASCII.h"
// Predefined instructions for ODrive communication
#include "odrive_ascii_instructions.h"
// Include slave-specific utypes.h before Ethercat.h
#include "utypes.h"
// Include the EtherCAT library
#include "Ethercat.h"

 #define DEBUG

#define PC_BAUDRATE (9600)
#define ODRIVE_BAUDRATE (460800)
#define WAIT_TIME (2000000)                    // micro-seconds
#define APP_TITLE "DieBOSlave"  // Application name to be printed to terminal

#define STATE_CHANGE_RETRIES 1 // Maximum number of retries before giving up on changin the state.

enum DieBOSlaveError : int32_t {
  NONE                                    = 0x0,
  INVALID_AXIS                            = 0x1,
  REQUEST_STATE_FAILED                    = 0x2,
  MOTOR_NOT_READY                         = 0x4,
  MOTOR_NOT_CALIBRATED                    = 0x8,
  ENCODER_NOT_READY                       = 0x10,
  ENCODER_INDEX_NOT_YET_FOUND             = 0x20,
  ENCODER_NOT_PRECALIBRATED               = 0x40,
  ENCODER_ABSOLUTE_POSITION_NOT_VALID     = 0x80,
  ENCODER_ABSOLUTE_GPIO_PIN_NOT_VALID     = 0x100,
};

uint8_t DIEBOSLAVE_COMMAND_FLAG = 0x20;
enum DieBOSlaveCommand: uint8_t {
    CLEAR_ODRIVE_ERRORS     = 0x20,     // Clear all errors on the ODrive
    CLEAR_DIEBOSLAVE_ERRORS = 0x21,     // Clear all errors on the DieBOSlave
    CLEAR_ALL_ERRORS        = 0x22      // Clear all errors on the DieBOSlave and on the ODrive
};

// Easy access to PDOs. Needs to be changed if different PDOs are used
#define miso Ethercat::pdoTx.Struct.miso
#define mosi Ethercat::pdoRx.Struct.mosi

// Set PDO sizes
const int PDORX_size = 16;
const int PDOTX_size = 88;

uint32_t last_requested_state[2] = {0, 0};

// Communication with PC
#ifdef DEBUG
#if MBED_MAJOR_VERSION >= 6
BufferedSerial pc(DBS_SWD_UART_TX, DBS_SWD_UART_RX, PC_BAUDRATE);
FileHandle *mbed::mbed_override_console(int fd)
{
  return &pc;
}
#else
Serial pc(DBS_SWD_UART_TX, DBS_SWD_UART_RX, PC_BAUDRATE);
#endif
#endif // Debug


// Ethercat communication with master
Ethercat ecat(DBS_ECAT_MOSI, DBS_ECAT_MISO, DBS_ECAT_SCK, DBS_ECAT_NCS, PDORX_size, PDOTX_size);
InterruptIn ecat_irq(DBS_ECAT_IRQ);
bool ecat_network_interrupt = false;

// Setup communication with ODrive via Unbuffered Serial
#if MBED_MAJOR_VERSION >= 6
ODriveASCII odrive = ODriveASCII(new BufferedSerial(PB_3, PB_4, ODRIVE_BAUDRATE));
#else
ODriveASCII odrive = ODriveASCII(new UARTSerial(PB_3, PB_4, ODRIVE_BAUDRATE));
#endif

// LED for showing status
DigitalOut statusLed(DBS_LED, false);  // DieBieSlave

#ifdef DEBUG
Timer t;
#endif

void updateGeneralMiSo(){
  int32_t axis0state = 0;
  int32_t axis0absolutePosition = 0;
  int32_t axis0motorPosition = 0.0f;
  float axis0velocity = 0.0f;
  float axis0current = 0.0f;
  float axis0temperature = 0.0f;
  int32_t axis1state = 0;
  int32_t axis1absolutePosition = 0;
  int32_t axis1motorPosition = 0.0f;
  float axis1velocity = 0.0f;
  float axis1current = 0.0f;
  float axis1temperature = 0.0f;
  
  odrive.customMARCHCommand(0, &axis0state, &axis0absolutePosition, &axis0motorPosition, &axis0velocity, &axis0current, &axis0temperature);
  odrive.customMARCHCommand(1, &axis1state, &axis1absolutePosition, &axis1motorPosition, &axis1velocity, &axis1current, &axis1temperature);
  
  miso.Axis0State = axis0state;
  miso.Axis0ActualPosition = axis0absolutePosition;
  miso.Axis0MotorPosition = axis0motorPosition;
  miso.Axis0Velocity = axis0velocity;
  miso.Axis0ActualCurrent = axis0current;
  miso.Axis0Temperature = axis0temperature;
  miso.Axis1State = axis1state;
  miso.Axis1ActualPosition = axis1absolutePosition;
  miso.Axis1MotorPosition = axis1motorPosition;
  miso.Axis1Velocity = axis1velocity;
  miso.Axis1ActualCurrent = axis1current;
  miso.Axis1Temperature = axis1temperature;
}

void updateErrors() {
  uint32_t axis_error = 0;
  uint32_t motor_error = 0;
  uint32_t encoder_error = 0;
  uint32_t controller_error = 0;

  odrive.customErrorCommand(0, &axis_error, &motor_error, &encoder_error, &controller_error);

  miso.Axis0Error = axis_error;
  miso.Axis0MotorError = motor_error;
  miso.Axis0EncoderError = encoder_error;
  miso.Axis0ControllerError = controller_error;

  axis_error = 0;
  motor_error = 0;
  encoder_error = 0;
  controller_error = 0;

  odrive.customErrorCommand(1, &axis_error, &motor_error, &encoder_error, &controller_error);

  miso.Axis1Error = axis_error;
  miso.Axis1MotorError = motor_error;
  miso.Axis1EncoderError = encoder_error;
  miso.Axis1ControllerError = controller_error;
}

/**
 * @brief Clear the DieBOSlaveError of an axis
 * @param axis: Number of the axis, either 0 or 1
 */
void clearDieBOSlaveErrors(int axis) {
  if (axis == 0) {
    miso.Axis0DieBOSlaveError = 0;
  } else {
    miso.Axis1DieBOSlaveError = 0;
  }
}

/**
 * @brief Set a DieBOSlave error for an axis
 * @param axis: Number of the axis, either 0 or 1
 * @param error: error to set
 */
void setDieBOSlaveError(int axis, DieBOSlaveError error) {
  if (axis == 0) {
    miso.Axis0DieBOSlaveError |= error;
  } else {
    miso.Axis1DieBOSlaveError |= error;
  }
}

/**
 * @brief Set the DieBOSlave error to INVALID_AXIS for both axes
 */
void setInvalidAxisError() {
  setDieBOSlaveError(0, DieBOSlaveError::INVALID_AXIS);
  setDieBOSlaveError(1, DieBOSlaveError::INVALID_AXIS);
}

/**
 * @brief Execute a DieBOSlave command
 * @param axis: Number of the axis, either 0 or 1
 * @param command: Command to execute
 */
void executeDieBOSlaveCommand(int axis, DieBOSlaveCommand command) {
  switch (command) {
    case DieBOSlaveCommand::CLEAR_ALL_ERRORS: {
      executeDieBOSlaveCommand(axis, DieBOSlaveCommand::CLEAR_ODRIVE_ERRORS);
      executeDieBOSlaveCommand(axis, DieBOSlaveCommand::CLEAR_DIEBOSLAVE_ERRORS);
    } break;
    case DieBOSlaveCommand::CLEAR_ODRIVE_ERRORS: {
      odrive.clearErrors();
    } break;
    case DieBOSlaveCommand::CLEAR_DIEBOSLAVE_ERRORS: {
      clearDieBOSlaveErrors(axis);
    } break;
  }
}

/**
 * @brief Check whether the motor is ready, set errors if it is not.
 * @param axis: Number of the axis, either 0 or 1
 * @return True if the motor is ready, false otherwise
 */
bool isMotorReady(int axis) {
  bool motor_is_calibrated;
  odrive.readParameter(axis, "motor.is_calibrated", &motor_is_calibrated);

  if (motor_is_calibrated) {
    return true;
  }

  setDieBOSlaveError(axis, DieBOSlaveError::MOTOR_NOT_READY);
  setDieBOSlaveError(axis, DieBOSlaveError::MOTOR_NOT_CALIBRATED);
  return false;
}

/**
 * @brief Check whether the absolute position is greater than 0
 * @param axis: Number of the axis, either 0 or 1
 * @return True if absolute position is greater than 0, false otherwise
 */
bool isAbsolutePositionValid(int axis) {
  if (axis == 0) {
    return miso.Axis0ActualPosition > 0;
  } else {
    return miso.Axis1ActualPosition > 0;
  }
}

/**
 * @brief Check whether the encoder is ready, set errors if it is not.
 * @param axis: Number of the axis, either 0 or 1
 * @return True if the encoder is ready, false otherwise
 */
bool isEncoderReady(int axis) {
  bool encoder_is_ready;
  odrive.readParameter(axis, "encoder.is_ready", &encoder_is_ready);
  
  if (encoder_is_ready) {
    return true;
  }
  
  setDieBOSlaveError(axis, DieBOSlaveError::ENCODER_NOT_READY);

  bool index_is_found = odrive.isEncoderIndexFound(axis);
  bool encoder_is_precalibrated = odrive.isEncoderPreCalibrated(axis);
  bool absolute_position_is_valid = isAbsolutePositionValid(axis);
  bool absolute_gpio_pin_is_valid = odrive.isAbsoluteGPIOPinValid(axis);
  if (!index_is_found) {
    setDieBOSlaveError(axis, DieBOSlaveError::ENCODER_INDEX_NOT_YET_FOUND);
  }
  if (!encoder_is_precalibrated) {
    setDieBOSlaveError(axis, DieBOSlaveError::ENCODER_NOT_PRECALIBRATED);
  }
  if (!absolute_position_is_valid) {
    setDieBOSlaveError(axis, DieBOSlaveError::ENCODER_ABSOLUTE_POSITION_NOT_VALID);
  }
  if (!absolute_gpio_pin_is_valid) {
    setDieBOSlaveError(axis, DieBOSlaveError::ENCODER_ABSOLUTE_GPIO_PIN_NOT_VALID);
  }
  return false;
}

/**
 * @brief Check whether an axis of the ODrive can go in closed loop control.
 * @param axis: Number of the axis, either 0 or 1 
 * @return Returns a boolean. 
 *         If true is returned, then the ODrive is ready for closed loop control.
 *         If false is returned, then the ODrive is not ready for closed loop control.
 */
bool isReadyForClosedLoopControl(int axis) {
  bool motor_is_ready = isMotorReady(axis);  
  bool encoder_is_ready = isEncoderReady(axis);
  #ifdef DEBUG
  printf("Motor is ready: %i, encoder is ready: %i\n", motor_is_ready, encoder_is_ready);
  #endif
  if (!motor_is_ready || !encoder_is_ready) {
    return false;
  }
  return true;
}

/**
 * @brief Check the requested state of the ODrive
 * @param axis: Number of the axis, either 0 or 1
 * @param current_state: Current state of the axis
 * @param requested_state: State to check
 * @return Returns a boolean. 
 *         If true is returned, then the state should be set on the ODrive.
 *         If false is returned, then the state should not be set on the ODrive.
 */
bool shouldODriveStateBeRequested(int axis, uint32_t current_state, uint32_t requested_state) {
  if (!(axis == 0 || axis == 1)) {
    setInvalidAxisError();
    return false;
  }

  if (requested_state == last_requested_state[axis] || requested_state == current_state) { 
      return false;
  }

  #ifdef DEBUG
  printf("Checking requested state %u for axis %i, current state is %u\n", requested_state, axis, current_state);
  #endif

  last_requested_state[axis] = requested_state;

  if (requested_state & DIEBOSLAVE_COMMAND_FLAG) {
    executeDieBOSlaveCommand(axis, (DieBOSlaveCommand) requested_state);
    return false;
  }

  switch (requested_state) {
    case ODriveASCII::MotorState::UNDEFINED: return false;
    case ODriveASCII::MotorState::ENCODER_INDEX_SEARCH: {
      return !odrive.isEncoderIndexFound(axis);
    }
    case ODriveASCII::MotorState::CLOSED_LOOP_CONTROL: {
      return isReadyForClosedLoopControl(axis);
    } 
    case ODriveASCII::MotorState::IDLE:
    case ODriveASCII::MotorState::STARTUP_SEQUENCE:
    case ODriveASCII::MotorState::FULL_CALIBRATION_SEQUENCE:
    case ODriveASCII::MotorState::MOTOR_CALIBRATION:
    case ODriveASCII::MotorState::SENSORLESS_CONTROL:
    case ODriveASCII::MotorState::ENCODER_OFFSET_CALIBRATION: {
      // No need to verify these states
      return true;
    }
    default:
      // Return false for undefined states
      return false;
  }
}

void ecat_interrupt(){
  ecat_network_interrupt = true;
}

int main() {
  wait_us(WAIT_TIME);
  // Print application title and compile information
  #ifdef DEBUG
  printf("\f\r\n%s\r\n------------------\r\nTime compiled = %s.\r\nDate = %s.\n", APP_TITLE, __TIME__, __DATE__);
  #endif
  statusLed = true;
  wait_us(WAIT_TIME);
  statusLed = false;

  // Enable watchdog timer and attach interrupt
  ecat.set_watchdog();
  ecat_irq.rise(&ecat_interrupt);
  
  miso.Axis0ActualPosition = 0;
  miso.Axis0ActualCurrent = 0;
  miso.Axis0Velocity = 0;
  miso.Axis0Error = 0;
  miso.Axis0MotorError = 0;
  miso.Axis0DieBOSlaveError = 0;
  miso.Axis0EncoderError = 0;
  miso.Axis0ControllerError = 0;
  miso.Axis0State = 0;
  miso.Axis0Temperature = 0.0f;
  miso.Axis0MotorPosition = 0;
  miso.Axis1ActualPosition = 0;
  miso.Axis1ActualCurrent = 0;
  miso.Axis1Velocity = 0;
  miso.Axis1Error = 0;
  miso.Axis1MotorError = 0;
  miso.Axis1DieBOSlaveError = 0;
  miso.Axis1EncoderError = 0;
  miso.Axis1ControllerError = 0;
  miso.Axis1State = 0;
  miso.Axis1Temperature = 0.0f;
  miso.Axis1MotorPosition = 0;

  // Update General information before sending
  updateGeneralMiSo();

  #ifdef DEBUG
  uint32_t timer_loop_counts = 0;
  uint32_t total_time = 0;
  t.start();
  #endif

  while(1){
    #ifdef DEBUG
    t.start();
    #endif
    ecat.update();

    updateGeneralMiSo();
    // Only retrieve errors when the main error is set
    if(miso.Axis0State != ODriveASCII::MotorState::CLOSED_LOOP_CONTROL || miso.Axis1State != ODriveASCII::MotorState::CLOSED_LOOP_CONTROL){
      updateErrors();
      if(miso.Axis0Error != 0 || miso.Axis0MotorError != 0 || miso.Axis0EncoderError != 0 || miso.Axis0ControllerError != 0
      || miso.Axis1Error != 0 || miso.Axis1MotorError != 0 || miso.Axis1EncoderError != 0 || miso.Axis1ControllerError != 0){
        statusLed = true;
      }
    }

    if (shouldODriveStateBeRequested(0, miso.Axis0State, mosi.Axis0RequestedState)) {
      #ifdef DEBUG
      printf("Changing state for axis 0 from %i to %i\n", miso.Axis0State, mosi.Axis0RequestedState);
      #endif
      if(!odrive.setState(0, (ODriveASCII::MotorState)(mosi.Axis0RequestedState))) {
          miso.Axis0DieBOSlaveError |= DieBOSlaveError::REQUEST_STATE_FAILED;
      }
    }

    if (shouldODriveStateBeRequested(1, miso.Axis1State, mosi.Axis1RequestedState)) {
      #ifdef DEBUG
      printf("Changing state for axis 1 from %i to %i\n", miso.Axis1State, mosi.Axis1RequestedState);
      #endif
      if(!odrive.setState(1, (ODriveASCII::MotorState)(mosi.Axis1RequestedState))) {
          miso.Axis1DieBOSlaveError |= DieBOSlaveError::REQUEST_STATE_FAILED;
      }
    }

    // Update the current input
    if(ecat_network_interrupt){
      // Always set current to 0 if network connection is interrupted
      odrive.setTorque(0, 0.0f);
      odrive.setTorque(1, 0.0f);
    }else{
      if(miso.Axis0State == ODriveASCII::MotorState::CLOSED_LOOP_CONTROL){
        float current = mosi.Axis0TargetTorque;
        odrive.setTorque(0, current);
      }
      if(miso.Axis1State == ODriveASCII::MotorState::CLOSED_LOOP_CONTROL){
        float current = mosi.Axis1TargetTorque;
        odrive.setTorque(1, current);
      }
    }
    
    #ifdef DEBUG
    #if MBED_MAJOR_VERSION >= 6
    total_time += std::chrono::duration_cast<std::chrono::microseconds>(t.elapsed_time()).count();
    #else
    total_time += t.read_us();
    #endif
    t.reset();
    timer_loop_counts += 1;
    if (timer_loop_counts >= 100) {
      //printf("There were %li milliseconds in %lu loop iterations, for an average of %li per loop\n", (uint32_t)(((float)total_time)/1000), timer_loop_counts, (uint32_t)((float)total_time)/(timer_loop_counts*1000));
      timer_loop_counts = 0;
      total_time = 0;
    }
    #endif
  }
}