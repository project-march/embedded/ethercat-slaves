/** 
 * This file contains a list of all instructions needed by the odrive_ges
 * The instruction names are based on the variable names for EtherCAT
 * 
 * First setup by J. Bouman MARCH VI at March 2021
 */

// Instructions for EtherCAT values 
// #define ASCII_INSTR_ACTUAL_POSITION     "encoder.pos_abs"
#define ASCII_INSTR_ACTUAL_POSITION     "encoder.pos_estimate"
#define ASCII_INSTR_ACTUAL_TORQUE       "motor.current_control.Iq_measured"
#define ASCII_INSTR_ACTUAL_VELOCITY     "encoder.vel_estimate"
#define ASCII_INSTR_AXIS_ERROR          "error"
#define ASCII_INSTR_MOTOR_ERROR         "motor.error"
#define ASCII_INSTR_EM_ERROR            ""
#define ASCII_INSTR_ENCODER_ERROR       "encoder.error"
#define ASCII_INSTR_CONTROLLER_ERROR    "controller.error"
#define ASCII_INSTR_STATE               "current_state"
//#define ASCII_INSTR_TARGET_TORQUE       "" Has special function


// Instructions for default settings