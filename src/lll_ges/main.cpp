#include <mbed.h>
// Include pin definitions for the DieBieSlave
#include "DBS_pindefs.h"
// Include slave-specific utypes.h before Ethercat.h
#include "utypes.h"
// Include the EtherCAT library
#include "Ethercat.h"
// Include Thermistor Library
#include "Thermistor.h"
// Include Pressure Soles
#include "pressureSole.h"

#define WAIT_TIME (2000000)                   // micro-seconds
#define APP_TITLE "MARCH Left Lower Leg GES"  // Application name to be printed to terminal
#define PC_BAUDRATE (9600)                    // per second

// Easy access to PDOs. Needs to be changed if different PDOs are used
#define miso Ethercat::pdoTx.Struct.miso
#define mosi Ethercat::pdoRx.Struct.mosi

// Set PDO sizes
const int PDORX_size = 32;
const int PDOTX_size = 40;

// LED for showing status
DigitalOut statusLed(DBS_LED);  // DieBieSlave

// Serial communication with the pc for debugging
BufferedSerial pc(DBS_UART_USB_TX, DBS_UART_USB_RX, PC_BAUDRATE);

// Ethercat communication with master
Ethercat ecat(DBS_ECAT_MOSI, DBS_ECAT_MISO, DBS_ECAT_SCK, DBS_ECAT_NCS, PDORX_size, PDOTX_size);

// PTC Thermistor
Thermistor ptcThermistorLAPD(DBS_P14);

// Allows for printing in the console, over the USB connection 'pc'
FileHandle *mbed::mbed_override_console(int fd)
{
  return &pc;
}

int main()
{
  wait_us(WAIT_TIME);
  // Print application title and compile information
  printf("\f\r\n%s\r\n------------------\r\nTime compiled = %s.\r\nDate = %s.\n", APP_TITLE, __TIME__, __DATE__);
  statusLed = true;
  wait_us(WAIT_TIME);
  statusLed = false;

  // Set all initial misos
  miso.TemperatureLAPD = 0;
  miso.OverTemperatureTriggerLLL = 0;
  miso.HeelRightL = 0;
  miso.HeelLeftL  = 0;
  miso.MET1L      = 0;
  miso.HalluxL    = 0;
  miso.MET3L      = 0;
  miso.ToesL      = 0;
  miso.MET5L      = 0;
  miso.ArchL      = 0;

  // Initialise the pressure soles
  pressureSole::startSoles();
  float* solesData;

  while (1)
  {
    // Update the EtherCAT buffer
    ecat.update();

    // Get Temperature
    uint32_t thermistorOverTemperatureLAPD = uint32_t (ptcThermistorLAPD.read());

    // Get most recent pressure sole conversion data
    solesData = pressureSole::getData();

    // Set all misos to be sent back to the master
    miso.OverTemperatureTriggerLLL = thermistorOverTemperatureLAPD;
    miso.HeelRightL  = solesData[0];
    miso.HeelLeftL   = solesData[1];
    miso.MET1L       = solesData[2];
    miso.HalluxL     = solesData[3];
    miso.MET3L       = solesData[4];
    miso.ToesL       = solesData[5];
    miso.MET5L       = solesData[6];
    miso.ArchL       = solesData[7];
  }
}
