#include <mbed.h>
#include "utils.h"
#include "current_sensors.h"
#include "LPC1768_pindefs_M6.h"
#include "PCA9534.h"
#include "ADS7828.h"
#include "StateMachine.h"

#define DEBUG

BufferedSerial pc(USBTX, USBRX, 9600);  // Serial communication for debugging

// Allows for printing in the console, over the USB connection 'pc'
FileHandle *mbed::mbed_override_console(int fd)
{
  return &pc;
}

#include "tinyBMS.h"

StateMachine stateMachine;  // State machine instance
Timer printTimer;           // Timer to print debug statements only once per second

// On/off-related inputs/outputs
DigitalOut onOffButtonLed(LPC_ONOFFBUTTON_LED, true);     // True means LED on
DigitalOut mbedLed1(LPC_LED1, false);                      // Shows same as button led
DigitalIn onOffButtonPressed(LPC_ONOFFBUTTON_PRESSED, PullDown);  // True means button pressed
DigitalOut keepPDBOn(LPC_KEEP_PDB_ON, true);              // True means keep PDB on
DigitalOut mbedLed4(LPC_LED4, false);                      // Shows if in Shutdown state

// Low voltage related inputs/outputs
DigitalOut LVOn(LPC_LVON, false);        // True means on
DigitalIn LV1Okay(LPC_LV1OKAY, PullDown);  // True means okay
DigitalIn LV2Okay(LPC_LV2OKAY, PullDown);  // True means okay
//AnalogIn lvCurrentSensor(LPC_LV_CURRENT_SENSOR);

// Master communication related inputs/outputs
DigitalOut mbedLed2(LPC_LED2, false);  // Shows if in MasterOk state
// EtherCAT is left out in standalone mode

// High voltage related inputs/outputs
DigitalOut mbedLed3(LPC_LED3, false);  // Shows if any HV is on
PCA9534 extendedIO(LPC_I2C_SDA, LPC_I2C_SCL);
ADS7828 currentSensors(new I2C(LPC_I2C_SDA, LPC_I2C_SCL), false, false, true);
AnalogIn hvCurrentTotal(LPC_HV_CURRENT_SENSOR);
//DigitalOut interupt(LPC_I2C_INTERRUPT_HV_CURRENT_SENSORS, false);

// Emergency button related inputs/outputs
DigitalIn emergencyButton(LPC_EMERGENCY_SWITCH_STATUS, PullDown);  // False means disconnected HV
DigitalOut emergencyButtonControl(LPC_EMERGENCY_SWITCH, true);    // False means disconnect HV

// Stop button
DigitalIn stopButton(LPC_STOP_BUTTON_PRESSED, PullDown);

// Current sensing related inputs/outputs
AnalogIn pdbCurrentSensor(LPC_PDB_CURRENT_SENSOR);

tinyBMS_serial bms(new BufferedSerial(LPC_TX, LPC_RX), false);

int main() {
  mbedLed1 = true;
  printTimer.start();  // Start print timer right before entering infinite loop
  extendedIO.turnOnAll(); // Turn on all the HV
  bool blinking = false;
  mbedLed2 = true;
  bms.set_debug(true);


  while(true){
  
    
    
    
    if (printTimer.read_ms() > 2000){
      bms.tinyBMS_readData();
      blinking = !blinking;
      if(blinking){
        //extendedIO.setAll(0b011111111); // Turn on LEDs keep HV on
        mbedLed3 = true;
        mbedLed4 = false;
        extendedIO.turnOnAll();
      } else {
        //extendedIO.setAll(0b011111111); // Turn off LEDs keep HV on
        mbedLed3 = false;
        mbedLed4 = true;
        extendedIO.turnOffAll();
      }
      #ifdef DEBUG
      printf("PDB DEBUG:\n");

      printf("PDB main values:\n");
      printf("\tButtonPressed: %i \n\tKeepPDBon: %i\n", onOffButtonPressed.read(), keepPDBOn.read());
      printf("\tStopButtonPressed: %i\n", stopButton.read());
      printf("\tPDB Current: %f\n\n", pdbCurrentSensor.read());

      printf("Low Voltage Net:\n");
      printf("\tLVon: %i \n\tLV1Okay: %i, LV2Okay: %i\n", LVOn.read(), LV1Okay.read(), LV2Okay.read());
      float pdbcur = remap(pdbCurrentSensor.read(), ACS723_MIN_VALUE_MBED, ACS723_MAX_VALUE_MBED, ACS723_MIN_CURRENT, ACS723_MAX_CURRENT);
      float lvcur1 = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH3), ACS723_MIN_VALUE_12BIT_5V, ACS723_MAX_VALUE_12BIT_5V, ACS723_MIN_CURRENT, ACS723_MAX_CURRENT);
      float lvcur2 = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH2), ACS723_MIN_VALUE_12BIT_5V, ACS723_MAX_VALUE_12BIT_5V, ACS723_MIN_CURRENT, ACS723_MAX_CURRENT);
      printf("\tPDBCurrent: %f, LV1Current: %f, \tLV2Current: %f\n\n", pdbcur, lvcur1, lvcur2);
      
      printf("High Voltage Net\n");
      float hvcurtotal = remap(hvCurrentTotal.read(), ACS780_MIN_VALUE_MBED, ACS780_MAX_VALUE_MBED, ACS780_100B_MIN_CURRENT, ACS780_100B_MIN_CURRENT);
      printf("\tHV states: %x, Total HV Current: %f\n", extendedIO.readAllOn() & 0b00001111, hvcurtotal); // Mask out the LEDs
      float hvcur1 = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH7), ACS780_MIN_VALUE_12BIT_5V, ACS780_MAX_VALUE_12BIT_5V, ACS780_050B_MIN_CURRENT, ACS780_050B_MAX_CURRENT);
      float hvcur2 = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH6), ACS780_MIN_VALUE_12BIT_5V, ACS780_MAX_VALUE_12BIT_5V, ACS780_050B_MIN_CURRENT, ACS780_050B_MAX_CURRENT);
      float hvcur3 = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH5), ACS780_MIN_VALUE_12BIT_5V, ACS780_MAX_VALUE_12BIT_5V, ACS780_050B_MIN_CURRENT, ACS780_050B_MAX_CURRENT);
      float hvcur4 = remap(currentSensors.readChannel(ADS7828::ADOn, ADS7828::CH4), ACS780_MIN_VALUE_12BIT_5V, ACS780_MAX_VALUE_12BIT_5V, ACS780_050B_MIN_CURRENT, ACS780_050B_MAX_CURRENT);
      printf("\tHV1Current: %f, HV2Current: %f, HV3Current: %f, HV4Current: %f\n\n", hvcur1, hvcur2, hvcur3, hvcur4);

      printf("Battery:\n");
      printf("\tVoltage: %f, Current: %f, Temperature1: %f, Temp 2: %f, Temp 3: %f\n", bms.tinyBMS_voltage(), bms.tinyBMS_current(), bms.tinyBMS_intTemp(), bms.tinyBMS_extTemp1(), bms.tinyBMS_extTemp2());
      printf("\n\n");
      #endif
      printTimer.reset();
    }
  }
}