#ifndef __UTYPES_H__
#define __UTYPES_H__

#include "cc.h"

/* Object dictionary storage */

typedef struct
{
   /* Identity */

   uint32_t serial;

   /* Inputs */

   struct
   {
      uint32_t Axis0AbsolutePosition;
      float Axis0Current;
      float Axis0MotorVelocity;
      uint32_t Axis0Error;
      uint32_t Axis0MotorError;
      uint32_t Axis0DieBoError;
      uint32_t Axis0EncoderError;
      uint32_t Axis0ControllerError;
      uint32_t Axis0State;
      float Axis0ODriveTemperature;
      float Axis0MotorTemperature;
      int32_t Axis0ShadowCount;
      float Axis0Torque;
      uint32_t Axis1AbsolutePosition;
      float Axis1Current;
      float Axis1MotorVelocity;
      uint32_t Axis1Error;
      uint32_t Axis1MotorError;
      uint32_t Axis1DieBoError;
      uint32_t Axis1EncoderError;
      uint32_t Axis1ControllerError;
      uint32_t Axis1State;
      float Axis1ODriveTemperature;
      float Axis1MotorTemperature;
      int32_t Axis1ShadowCount;
      float Axis1Torque;
      int32_t OdriveError;
      float Heel_right;
      float Heel_left;
      float Met1;
      float Hallux;
      float Met3;
      float Toes;
      float Met5;
      float Arch;
      float Rfix;
   } TxPDO;

   /* Outputs */

   struct
   {
      float Axis0TargetTorque;
      float Axis0TargetPosition;
      float Axis0FuzzyTorque;
      float Axis0FuzzyPosition;
      float Axis0PositionP;
      float Axis0PositionI;
      float Axis0PostionD;
      float Axis0TorqueP;
      float Axis0TorqueI;
      float Axis0TorqueD;
      uint32_t Axis0RequestedState;
      float Axis1TargetTorque;
      float Axis1TargetPosition;
      float Axis1FuzzyTorque;
      float Axis1FuzzyPosition;
      float Axis1PositionP;
      float Axis1PositionI;
      float Axis1PositionD;
      float Axis1TorqueP;
      float Axis1TorqueI;
      float Axis1TorqueD;
      uint32_t Axis1RequestedState;
   } RxPDO;

} _Objects;

extern _Objects Obj;

#endif /* __UTYPES_H__ */
