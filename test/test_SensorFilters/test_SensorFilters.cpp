#include "SensorFilters.h"
#include "unity.h"

// Simple Moving Average
void test_SMA_empty(){
  SimpleMovingAverage sma;
  TEST_ASSERT_EQUAL_FLOAT(0.0f, sma.get_current_value());
}

void test_SMA_average(){
  SimpleMovingAverage sma;
  for(int i = 0; i < SMA_BUF_SIZE; i++){
    sma.add_value(i);
  }
  TEST_ASSERT_EQUAL_FLOAT((SMA_BUF_SIZE-1) / 2.0f, sma.get_current_value());
}

// Push out the first value
void test_SMA_push_out(){
  SimpleMovingAverage sma;
  sma.add_value(9999999999);
  for(int i = 0; i < SMA_BUF_SIZE; i++){
    sma.add_value(i);
  }
  TEST_ASSERT_EQUAL_FLOAT((SMA_BUF_SIZE-1) / 2.0f, sma.get_current_value());
}

// Add less values than the max buffer
void test_SMA_less_values_than_buffer_size(){
  SimpleMovingAverage sma;
  sma.add_value(1);
  sma.add_value(2);
  TEST_ASSERT_EQUAL_FLOAT(1.5f, sma.get_current_value());
}

// Exponential Moving Average
void test_EMA_empty(){
  ExponentialMovingAverage ema(0.1f);
  TEST_ASSERT_EQUAL_FLOAT(0.0f, ema.get_current_value());
}

void test_EMA_first_value(){
  ExponentialMovingAverage ema(0.1f);
  ema.add_value(5);
  TEST_ASSERT_EQUAL_FLOAT(5.0f, ema.get_current_value());
}

void test_EMA_alpha_one(){
  ExponentialMovingAverage ema(1);
  ema.add_value(5);
  ema.add_value(10);
  TEST_ASSERT_EQUAL_FLOAT(10.0f, ema.get_current_value());
}

void test_EMA_alpha_zero_point_one(){
  ExponentialMovingAverage ema(0.1f);
  ema.add_value(5);
  ema.add_value(10);
  // result = (0.9*5) + (0.1*10) = 5,5
  TEST_ASSERT_EQUAL_FLOAT(5.5f, ema.get_current_value());
}

int main(int argc, char **argv){
  UNITY_BEGIN();
  // Simple Moving Average
  RUN_TEST(test_SMA_empty);
  RUN_TEST(test_SMA_average);
  RUN_TEST(test_SMA_push_out);
  RUN_TEST(test_SMA_less_values_than_buffer_size);
  // Exponential Moving Average
  RUN_TEST(test_EMA_empty);
  RUN_TEST(test_EMA_first_value);
  RUN_TEST(test_EMA_alpha_one);
  RUN_TEST(test_EMA_alpha_zero_point_one);
  UNITY_END();
  return 0;
}