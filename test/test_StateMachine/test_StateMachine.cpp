#include <StateMachine.h>
#include <unity.h>

/**
 * @defgroup teststatemachine TestStateMachine
 * @ingroup test
 * 
 * These tests are written for the StateMachine library.
 * The class creates an state machine which determines
 * the output of the Power Distribution Board.
 * It utilizes a timer to determine some actions and 
 * 
 * @{
 */

/**
 * @brief Wrapper for testing StateMachine
 * 
 * This wrapper utilizes the Timer class, which implements a mock version of the Mbed timer.
 * The timer is used to set the elapsed to exact milliseconds, 
 * such that specific parts of the state machine can be tested.
 */
class TestStateMachine: public StateMachine {
    public:
        /// Set the value of internal variable currentState
        void setCurrentState(State newState){
            this->currentState = newState;
        }
        /// Get the current state as a State type 
        State getCurrentState(){
            return this->currentState;
        }
        /// Set the value of internal variable keepPDBOn
        void setKeepPDBOn(bool state){
            this->keepPDBOn = state;
        }
        /// Set the value of internal variable LVon
        void setLVon(bool state){
            this->LVon = state;
        }
        /// Set the value of internal variable HVon
        void setHVOn(bool state){
            this->HVon = state;
        }
        /// Set the value of internal variable computerOn
        void setcomputerOn(bool state){
            this->computerOn = state;
        }
        /// Set the value of internal variable onOffButtonPressedLong
        void setOnOffButtonPressedLong(bool state){
            this->onOffButtonPressedLong = state;
        }
        /// Set the value of internal variable onOffButtonPressedShort
        void setOnOffButtonPressedShort(bool state){
            this->onOffButtonPressedShort = state;
        }
        /// Calls the private function updateInternalStates with the same input variables
        void updateInternal(bool buttonPressed){
            this->updateInternalStates(buttonPressed);
        }
        /// Get the current value of the itnernal variable onOffButtonPressedLong
        bool getButtonPressedLong(){
            return this->onOffButtonPressedLong;
        }
        /// Get the current value of the internal variable onOffButtonPressedShort
        bool getButtonPressedShort(){
            return this->onOffButtonPressedShort;
        }
        /// Sets the elapsed time of the button timer
        void setButtonTimer(int ms){
            this->onOffButtonTimer.set_ms(ms);
        }
        /// Sets the elapsed time of the state timer
        void setStateTimer(int ms){
            this->stateTimer.set_ms(ms);
        }
};
/// @} teststatemachine

void test_updateInternalStates_pressed(){
    TestStateMachine sm;
    sm.setButtonTimer(ON_OFF_BUTTON_TIME_PRESSED_LONG+1);
    sm.updateInternal(true);
    TEST_ASSERT_EQUAL(true, sm.getButtonPressedLong());
    TEST_ASSERT_EQUAL(false, sm.getButtonPressedShort());
}

void test_updateInternalStates_long_press(){
    TestStateMachine sm;
    sm.setButtonTimer(ON_OFF_BUTTON_TIME_PRESSED_LONG+1);
    sm.updateInternal(false);
    TEST_ASSERT_EQUAL(true, sm.getButtonPressedLong());
    TEST_ASSERT_EQUAL(false, sm.getButtonPressedShort());
}

void test_updateInternalStates_short_press(){
    TestStateMachine sm;
    sm.setButtonTimer(ON_OFF_BUTTON_TIME_PRESSED_SHORT+1);
    sm.updateInternal(false);
    TEST_ASSERT_EQUAL(false, sm.getButtonPressedLong());
    TEST_ASSERT_EQUAL(true, sm.getButtonPressedShort());
}


void test_updateInternalStates_short_press_unreleased(){
    TestStateMachine sm;
    sm.setButtonTimer(ON_OFF_BUTTON_TIME_PRESSED_SHORT+1);
    sm.updateInternal(true);
    TEST_ASSERT_EQUAL(false, sm.getButtonPressedLong());
    TEST_ASSERT_EQUAL(false, sm.getButtonPressedShort());
}

void test_update_init_to_lvon() {
    // A correct transition is as follows: sm.updateState(false, *, true);
    TestStateMachine sm1;
    sm1.setCurrentState(State::Init_s);
    sm1.updateState(false, false, true);
    
    TestStateMachine sm2;
    sm2.setCurrentState(State::Init_s);
    sm2.updateState(false, true, true);
    TEST_ASSERT_EQUAL(State::LVOn_s, sm1.getCurrentState());
    TEST_ASSERT_EQUAL(State::LVOn_s, sm2.getCurrentState());
}

void test_update_init_to_shutdown_long_press() {
    TestStateMachine sm;
    sm.setCurrentState(State::Init_s);
    sm.setOnOffButtonPressedLong(true);
    sm.updateState(false, false, false);
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm.getCurrentState());
}

void test_update_init_to_shutdown_short_press() {
    TestStateMachine sm;
    sm.setCurrentState(State::Init_s);
    sm.setOnOffButtonPressedShort(true);
    sm.updateState(false, false, false);
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm.getCurrentState());
}

void test_update_init_to_shutdown_not_lvon() {
    // A correct transition is as follows: sm.updateState(false, *, true);
    TestStateMachine sm1;
    sm1.setCurrentState(State::Init_s);
    sm1.setOnOffButtonPressedLong(true);
    sm1.updateState(false, false, true);
    
    TestStateMachine sm2;
    sm2.setCurrentState(State::Init_s);
    sm2.setOnOffButtonPressedLong(true);
    sm2.updateState(false, true, true);
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm1.getCurrentState());
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm2.getCurrentState());
}

void test_update_lvon() {
    TestStateMachine sm;
    sm.setCurrentState(State::LVOn_s);
    sm.updateState(false, false, false);
    TEST_ASSERT_EQUAL(State::LVOn_s, sm.getCurrentState());
    TEST_ASSERT_EQUAL(true, sm.getKeepPDBOn());
    TEST_ASSERT_EQUAL(true, sm.getLVOn());
    TEST_ASSERT_EQUAL(false, sm.getHVOn());
    TEST_ASSERT_EQUAL(false, sm.getComputerOn());
}

void test_update_lvon_no_transition() {
    TestStateMachine sm;
    sm.setCurrentState(State::LVOn_s);
    // A correct transition is as follows: sm.updateState(false, true, true);
    sm.updateState(false, false, false);
    // sm.updateState(false, false, true); Correct transition
    sm.updateState(false, true, false);
    // sm.updateState(false, true, true); Correct transition
    sm.updateState(true, false, false);
    // sm.updateState(true, false, true); Correct transition
    sm.updateState(true, true, false);
    // sm.updateState(true, true, true); Correct transition
    TEST_ASSERT_EQUAL(State::LVOn_s, sm.getCurrentState());
}

void test_update_lvon_to_operational() {
    TestStateMachine sm;
    sm.setCurrentState(State::LVOn_s);
    // A correct transition is as follows: sm.updateState(false, true, true);
    sm.updateState(false, true, true);
    TEST_ASSERT_EQUAL(State::Operational_s, sm.getCurrentState());
}

void test_update_lvon_to_shutdown_short_press() {
    TestStateMachine sm;
    sm.setCurrentState(State::LVOn_s);
    sm.setOnOffButtonPressedShort(true);
    sm.updateState(false, false, true);
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm.getCurrentState());
}

void test_update_lvon_to_shutdown_not_operational() {
    // A correct transition is as follows: sm.updateState(false, true, true);
    TestStateMachine sm;
    sm.setCurrentState(State::LVOn_s);
    sm.setOnOffButtonPressedLong(true);
    sm.updateState(false, true, true);
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm.getCurrentState());
}

void test_update_operational() {
    TestStateMachine sm;
    sm.setCurrentState(State::Operational_s);
    sm.updateState(false, true, false);
    TEST_ASSERT_EQUAL(State::Operational_s, sm.getCurrentState());
    TEST_ASSERT_EQUAL(true, sm.getKeepPDBOn());
    TEST_ASSERT_EQUAL(true, sm.getLVOn());
    TEST_ASSERT_EQUAL(true, sm.getHVOn());
    TEST_ASSERT_EQUAL(true, sm.getComputerOn());
}

void test_update_operational_no_transition() {
    TestStateMachine sm;
    sm.setCurrentState(State::Operational_s);
    // A correct transition is as follows: sm.updateState(*, false, *);
    // sm.updateState(false, false, false);
    // sm.updateState(false, false, true);
    sm.updateState(false, true, false);
    sm.updateState(false, true, true);
    // sm.updateState(true, false, false);
    // sm.updateState(true, false, true);
    sm.updateState(true, true, false);
    sm.updateState(true, true, true);
    TEST_ASSERT_EQUAL(State::Operational_s, sm.getCurrentState());
}

void test_update_operational_to_shutdown_not_lvon() {
    // A correct transition is as follows: sm.updateState(false, *, true);
    TestStateMachine sm;
    sm.setCurrentState(State::Operational_s);
    sm.setStateTimer(MINIMAL_LV_ON_TIME+1);
    sm.updateState(false, false, true);
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm.getCurrentState());
}

void test_update_shutdown() {
    TestStateMachine sm;
    sm.setCurrentState(State::Shutdown_s);
    sm.updateState(false, false, false);
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm.getCurrentState());
    TEST_ASSERT_EQUAL(false, sm.getKeepPDBOn());
    TEST_ASSERT_EQUAL(false, sm.getLVOn());
    TEST_ASSERT_EQUAL(false, sm.getHVOn());
    TEST_ASSERT_EQUAL(false, sm.getComputerOn());
}

void test_update_shutdown_no_transition() {
    TestStateMachine sm;
    sm.setCurrentState(State::Shutdown_s);
    // There are not transitions from shutdown
    sm.updateState(false, false, false);
    sm.updateState(false, false, true);
    sm.updateState(false, true, false);
    sm.updateState(false, true, true);
    sm.updateState(true, false, false);
    sm.updateState(true, false, true);
    sm.updateState(true, true, false);
    sm.updateState(true, true, true);
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm.getCurrentState());
}

void test_stay_in_operational_for_sampling() {
    // Stay a bit longer in operational to make sure the sampled values are correct.
    TestStateMachine sm;
    sm.setCurrentState(State::Operational_s);
    sm.setStateTimer(MINIMAL_LV_ON_TIME);
    sm.updateState(false, false, true);
    TEST_ASSERT_EQUAL(State::Operational_s, sm.getCurrentState());
}

void test_update_full_cycle() {
    // Cycles from Init to LVOn to Operational to LVOn to Shutdown
    TestStateMachine sm;
    sm.setCurrentState(State::Init_s); // Start at Init
    sm.updateState(false, false, true); // From Init to LVOn
    TEST_ASSERT_EQUAL(State::LVOn_s, sm.getCurrentState());
    sm.updateState(false, false, true); // From LVOn to Operational
    TEST_ASSERT_EQUAL(State::Operational_s, sm.getCurrentState());
    sm.updateState(false, true, true); // Stay in Operational
    TEST_ASSERT_EQUAL(State::Operational_s, sm.getCurrentState());
    sm.setStateTimer(MINIMAL_LV_ON_TIME+1); // Make sure minimal sampling time is reached
    sm.updateState(false, false, true); // Computer turns off
    TEST_ASSERT_EQUAL(State::Shutdown_s, sm.getCurrentState());
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(test_updateInternalStates_pressed);
    RUN_TEST(test_updateInternalStates_long_press);
    RUN_TEST(test_updateInternalStates_short_press);
    RUN_TEST(test_updateInternalStates_short_press_unreleased);
    RUN_TEST(test_update_init_to_lvon);
    RUN_TEST(test_update_init_to_shutdown_long_press);
    RUN_TEST(test_update_init_to_shutdown_short_press);
    RUN_TEST(test_update_init_to_shutdown_not_lvon);
    RUN_TEST(test_update_lvon);
    RUN_TEST(test_update_lvon_no_transition);
    RUN_TEST(test_update_lvon_to_operational);
    RUN_TEST(test_update_lvon_to_shutdown_not_operational);
    RUN_TEST(test_update_operational);
    RUN_TEST(test_update_operational_no_transition);
    RUN_TEST(test_update_operational_to_shutdown_not_lvon);
    RUN_TEST(test_update_shutdown);
    RUN_TEST(test_update_shutdown_no_transition);
    RUN_TEST(test_stay_in_operational_for_sampling);
    RUN_TEST(test_update_full_cycle);
    UNITY_END();
    return 0;
}
