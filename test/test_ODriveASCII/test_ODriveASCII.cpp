#include <ODriveASCII.h>
#include <unity.h>
/**
 * @defgroup test Tests
 * Group with different test lists. Tests are based around a specific library or class 
 * and test at least the most basic parts of the implemenation.
 * 
 * For the tests are often wrapper classes written to change the input or state of hardware. 
 */

/**
 * @defgroup testodriveascii TestODriveASCII
 * @ingroup test
 * 
 * These tests are written for the ODriveASCII library.
 * The class uses a serial connection to communicate with the ODrive.
 * Therefore a `MockSerial` class is implemented.
 * 
 * @{
 */

/**
 * @brief Wrapper class for testing ODriveASCII
 * 
 * Creates a "wrapper" class to test protected methods.
 * This can reexport protected functions for the
 * tests.
 * 
 * For more detailed description of specific version check the ODriveASCII module
 * @{
 */
class TestODriveASCII : public ODriveASCII {
    public:
        /// Constructor
        TestODriveASCII(MockSerial* ms) : ODriveASCII(ms) {};
        /// Converts a float to a string
        string floatToString(float f) {
            return ODriveASCII::floatToString(f);
        }
        /// Find integer in string
        uint32_t findInt(const char* buffer, uint32_t startPosition, int32_t* result) {
            return ODriveASCII::findInt(buffer, startPosition, result);
        }
        /// Find float in string
        uint32_t findFloat(const char* buffer, uint32_t startPosition, float* result){
            return ODriveASCII::findFloat(buffer, startPosition, result);
        }
        /// Read string from serial interface
        bool readCharArray(char* buffer) {
            return ODriveASCII::readCharArray(buffer);
        }
        /// Read a single float
        float readFloat() {
            return ODriveASCII::readFloat();
        }
        /// Read a single integer
        int32_t readInt() {
            return ODriveASCII::readInt();
        }
        /// Write a message to serial interface
        void writeToSerial(const string message) {
            return ODriveASCII::writeToSerial(message);
        }
};
/// @} testodriveascii
/// @} test
MockSerial ms;
TestODriveASCII od {&ms};

void setUp(void) {
    // Replace the existing buffers with empty
    // buffers.
    string empty_buffer = string();
    ms.write(empty_buffer.c_str(), 0);
    ms.set_buffer_content(empty_buffer);
}

void test_float_to_string_zero(void) {
    TEST_ASSERT_EQUAL_STRING("0.0000", od.floatToString(0.0).c_str());
}

void test_float_to_string_negative_zero(void) {
    TEST_ASSERT_EQUAL_STRING("0.0000", od.floatToString(-0.0).c_str());
}

void test_float_to_string_whole_number(void) {
    TEST_ASSERT_EQUAL_STRING("112.0000", od.floatToString(112).c_str());
}

void test_float_to_string_whole_negative_number(void) {
    TEST_ASSERT_EQUAL_STRING("-112.0000", od.floatToString(-112).c_str());
}

void test_float_to_string_close_to_zero(void) {
    TEST_ASSERT_EQUAL_STRING("0.1024", od.floatToString(0.10248).c_str());
}

void test_float_to_string_close_to_negative_zero(void) {
    TEST_ASSERT_EQUAL_STRING("-0.1024", od.floatToString(-0.10248).c_str());
}

void test_float_to_string_closer_to_zero(void) {
    TEST_ASSERT_EQUAL_STRING("0.0010", od.floatToString(0.0010248).c_str());
}

void test_float_to_string_closer_to_negative_zero(void) {
    TEST_ASSERT_EQUAL_STRING("-0.0010", od.floatToString(-0.0010248).c_str());
}

void test_float_to_string_big_number(void) {
    TEST_ASSERT_EQUAL_STRING("14712.1240", od.floatToString(14712.124).c_str());
}

void test_float_to_string_big_negative_number(void) {
    TEST_ASSERT_EQUAL_STRING("-14712.1240", od.floatToString(-14712.124).c_str());
}

void test_float_to_string_nan(void) {
    TEST_ASSERT_EQUAL_STRING("0.0", od.floatToString(NAN).c_str());
}

void test_float_to_string_infinity(void) {
    TEST_ASSERT_EQUAL_STRING("0.0", od.floatToString(INFINITY).c_str());
}

void test_float_to_string_negative_infinity(void) {
    TEST_ASSERT_EQUAL_STRING("0.0", od.floatToString(-INFINITY).c_str());
}

void test_float_to_string_a_bit_over_one(void) {
    TEST_ASSERT_EQUAL_STRING("1.0123", od.floatToString(1.01237).c_str());
}

void test_float_to_string_a_bit_over_negative_one(void) {
    TEST_ASSERT_EQUAL_STRING("-1.0123", od.floatToString(-1.01237).c_str());
}

void test_setTorque(void) {
    od.setTorque(1, 654.24504);
    TEST_ASSERT_EQUAL_STRING("c 1 654.2450\n", ms.get_write_buffer().c_str());
}

void test_writeToSerial_normal_message() {
    string msg = string("Hello!");
    od.writeToSerial(msg.c_str());
    TEST_ASSERT_EQUAL_STRING(msg.c_str(), ms.get_write_buffer().c_str());
}

void test_writeToSerial_byte_sequence() {
    char bytes[] = {'A', 'B', 'C'};
    od.writeToSerial(bytes);
    TEST_ASSERT_EQUAL_STRING(bytes, ms.get_write_buffer().c_str());
}

void test_readInt_without_newline() {
    string buffer_content = string("456789");
    ms.set_buffer_content(buffer_content);

    int32_t i = od.readInt();
    TEST_ASSERT_EQUAL(0, i);
}

void test_readInt_with_newline() {
    string buffer_content = string("456789\n");
    ms.set_buffer_content(buffer_content);

    int32_t i = od.readInt();
    TEST_ASSERT_EQUAL(456789, i);
}

void test_readInt_empty_string() {
    string buffer_content = string();
    ms.set_buffer_content(buffer_content);

    int32_t i = od.readInt();
    TEST_ASSERT_EQUAL(0, i);
}

void test_readInt_not_an_int() {
    string buffer_content = string("hello\n");
    ms.set_buffer_content(buffer_content);

    int32_t i = od.readInt();
    TEST_ASSERT_EQUAL(0, i);
}

void test_readInt_overflow() {
    string buffer_content = string("2147483648\n");
    ms.set_buffer_content(buffer_content);

    int32_t i = od.readInt();
    // note the minus appears due to overflow
    TEST_ASSERT_EQUAL(-2147483648, i);
}

void test_readFloat_without_newline() {
    string buffer_content = string("4.5");
    ms.set_buffer_content(buffer_content);

    float f = od.readFloat();
    TEST_ASSERT_EQUAL_FLOAT(0.0, f);
}

void test_readFloat_with_newline() {
    string buffer_content = string("4.5\n");
    ms.set_buffer_content(buffer_content);

    float f = od.readFloat();
    TEST_ASSERT_EQUAL_FLOAT(4.5, f);
}

void test_readFloat_empty_string() {
    string buffer_content = string();
    ms.set_buffer_content(buffer_content);

    float f = od.readFloat();
    TEST_ASSERT_EQUAL_FLOAT(0.0, f);
}

void test_readFloat_not_a_float() {
    string buffer_content = string("hello");
    ms.set_buffer_content(buffer_content);

    float f = od.readFloat();
    TEST_ASSERT_EQUAL_FLOAT(0.0, f);
}

void test_readCharArray_without_newline() {
    string buffer_content = string("This is a line");
    ms.set_buffer_content(buffer_content);

    char buffer[128];
    bool success = od.readCharArray(buffer);
    TEST_ASSERT_FALSE(success);
    TEST_ASSERT_EQUAL_STRING("This is a line", buffer);
}

void test_readCharArray_with_newline() {
    string buffer_content = string("This is a line with a newline \n");
    ms.set_buffer_content(buffer_content);

    char buffer[128];
    bool success = od.readCharArray(buffer);
    TEST_ASSERT_TRUE(success);
    TEST_ASSERT_EQUAL_STRING("This is a line with a newline \n", buffer);
}

void test_readCharArray_empty_string() {
    string buffer_content = string();
    ms.set_buffer_content(buffer_content);

    char buffer[128] {0};
    bool success = od.readCharArray(buffer);
    TEST_ASSERT_FALSE(success);
    TEST_ASSERT_EQUAL_STRING("", buffer);
}

void test_findFloat_normal_number(void) {
    string buffer = string("1234.5678");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 0, &result);
    TEST_ASSERT_EQUAL_FLOAT(1234.5678, result);
    TEST_ASSERT_EQUAL_FLOAT(9, position);
}

void test_findFloat_zero(void) {
    string buffer = string("abcd0.0efgh");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL_FLOAT(0.0, result);
    TEST_ASSERT_EQUAL_FLOAT(7, position);
}

void test_findFloat_negative_zero(void) {
    string buffer = string("abcd-0.0efgh");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL_FLOAT(-0.0, result);
    TEST_ASSERT_EQUAL_FLOAT(8, position);
}

void test_findFloat_around_zero(void) {
    string buffer = string("abcd0.05678efgh");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL_FLOAT(0.05678, result);
    TEST_ASSERT_EQUAL_FLOAT(11, position);
}

void test_findFloat_around_negative_zero(void) {
    string buffer = string("abcd-0.05678efgh");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL_FLOAT(-0.05678, result);
    TEST_ASSERT_EQUAL_FLOAT(12, position);
}

void test_findFloat_bigger_number(void) {
    string buffer = string("abcd12.05678efgh");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL_FLOAT(12.05678, result);
    TEST_ASSERT_EQUAL_FLOAT(12, position);
}

void test_findFloat_negative_bigger_number(void) {
    string buffer = string("abcd-12.05678efgh");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL_FLOAT(-12.05678, result);
    TEST_ASSERT_EQUAL_FLOAT(13, position);
}

void test_findFloat_integer_number(void) {
    string buffer = string("abcd1234efgh");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL_FLOAT(1234.0, result);
    TEST_ASSERT_EQUAL_FLOAT(8, position);
}

void test_findFloat_negative_integer_number(void) {
    string buffer = string("abcd-1234efgh");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL_FLOAT(-1234, result);
    TEST_ASSERT_EQUAL_FLOAT(9, position);
}

void test_findFloat_with_command(void) {
    string buffer = string("command 12.34\n");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 8, &result);
    TEST_ASSERT_EQUAL_FLOAT(12.34, result);
    TEST_ASSERT_EQUAL_FLOAT(13, position);
}

void test_findFloat_no_number(void) {
    string buffer = string("just some text");
    float result = 0;
    int position = od.findFloat(buffer.c_str(), 8, &result);
    TEST_ASSERT_EQUAL_FLOAT(0, result);
    TEST_ASSERT_EQUAL_FLOAT(8, position);
}

void test_findInt_just_value(void) {
    string buffer = string("123456789");
    int32_t result = 0;
    int position = od.findInt(buffer.c_str(), 0, &result);
    TEST_ASSERT_EQUAL(123456789, result);
    TEST_ASSERT_EQUAL(9, position);
}

void test_findInt_normal_value(void) {
    string buffer = string("abcd123456789");
    int32_t result = 0;
    int position = od.findInt(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL(123456789, result);
    TEST_ASSERT_EQUAL(13, position);
}

void test_findInt_normal_value_between_letters(void) {
    string buffer = string("abcd123456789efgh");
    int32_t result = 0;
    int position = od.findInt(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL(123456789, result);
    TEST_ASSERT_EQUAL(13, position);
}

void test_findInt_negative_value_between_letters(void) {
    string buffer = string("abcd-123456789efgh");
    int32_t result = 0;
    int position = od.findInt(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL(-123456789, result);
    TEST_ASSERT_EQUAL(14, position);
}

void test_findInt_no_int_at_location(void) {
    string buffer = string("abcdefgh");
    int32_t result = 0;
    int position = od.findInt(buffer.c_str(), 3, &result);
    TEST_ASSERT_EQUAL(3, position);
}

void test_findInt_multiple_ints(void) {
    string buffer = string("ab1234567890 987654321defgh");
    int32_t result = 0;
    int position = od.findInt(buffer.c_str(), 2, &result);
    TEST_ASSERT_EQUAL(1234567890, result);
    TEST_ASSERT_EQUAL(12, position);

    position = od.findInt(buffer.c_str(), position+1, &result);
    TEST_ASSERT_EQUAL(987654321, result);
    TEST_ASSERT_EQUAL(22, position);
}

void test_findInt_starts_with_zero(void) {
    string buffer = string("abcd0123efgh");
    int32_t result = 0;
    int position = od.findInt(buffer.c_str(), 4, &result);
    TEST_ASSERT_EQUAL(123, result);
    TEST_ASSERT_EQUAL(8, position);
}

void test_findInt_typical_odrive_message(void) {
    string buffer = string("command 1234567890 12.34\n");
    int32_t result = 0;
    int position = od.findInt(buffer.c_str(), 8, &result);
    TEST_ASSERT_EQUAL(1234567890, result);
    TEST_ASSERT_EQUAL(18, position);
}

int main(int argc, char **argv) {
    UNITY_BEGIN();
    RUN_TEST(test_float_to_string_zero);
    RUN_TEST(test_float_to_string_negative_zero);
    RUN_TEST(test_float_to_string_whole_number);
    RUN_TEST(test_float_to_string_whole_negative_number);
    RUN_TEST(test_float_to_string_close_to_zero);
    RUN_TEST(test_float_to_string_close_to_negative_zero);
    RUN_TEST(test_float_to_string_closer_to_zero);
    RUN_TEST(test_float_to_string_closer_to_negative_zero);
    RUN_TEST(test_float_to_string_big_number);
    RUN_TEST(test_float_to_string_big_negative_number);
    RUN_TEST(test_float_to_string_nan);
    RUN_TEST(test_float_to_string_infinity);
    RUN_TEST(test_float_to_string_negative_infinity);
    RUN_TEST(test_float_to_string_a_bit_over_one);
    RUN_TEST(test_float_to_string_a_bit_over_negative_one);
    RUN_TEST(test_setTorque);
    RUN_TEST(test_writeToSerial_normal_message);
    RUN_TEST(test_writeToSerial_byte_sequence);
    RUN_TEST(test_readInt_without_newline);
    RUN_TEST(test_readInt_with_newline);
    RUN_TEST(test_readInt_empty_string);
    RUN_TEST(test_readInt_not_an_int);
    RUN_TEST(test_readInt_overflow);
    RUN_TEST(test_readFloat_without_newline);
    RUN_TEST(test_readFloat_with_newline);
    RUN_TEST(test_readFloat_empty_string);
    RUN_TEST(test_readFloat_not_a_float);
    RUN_TEST(test_readCharArray_without_newline);
    RUN_TEST(test_readCharArray_with_newline);
    RUN_TEST(test_readCharArray_empty_string);
    RUN_TEST(test_findFloat_normal_number);
    RUN_TEST(test_findFloat_zero);
    RUN_TEST(test_findFloat_negative_zero);
    RUN_TEST(test_findFloat_around_zero);
    RUN_TEST(test_findFloat_around_negative_zero);
    RUN_TEST(test_findFloat_bigger_number);
    RUN_TEST(test_findFloat_negative_bigger_number);
    RUN_TEST(test_findFloat_integer_number);
    RUN_TEST(test_findFloat_negative_integer_number);
    RUN_TEST(test_findFloat_with_command);
    RUN_TEST(test_findFloat_no_number);
    RUN_TEST(test_findInt_just_value);
    RUN_TEST(test_findInt_normal_value);
    RUN_TEST(test_findInt_normal_value_between_letters);
    RUN_TEST(test_findInt_negative_value_between_letters);
    RUN_TEST(test_findInt_no_int_at_location);
    RUN_TEST(test_findInt_multiple_ints);
    RUN_TEST(test_findInt_starts_with_zero);
    RUN_TEST(test_findInt_typical_odrive_message);
    UNITY_END();
    return 0;
}
