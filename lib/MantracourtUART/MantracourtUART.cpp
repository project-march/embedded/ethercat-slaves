#include "MantracourtUART.h"


MantracourtUART::MantracourtUART(PinName tx, PinName rx, int baud) 
: BufferedSerial(tx, rx, baud), baudrate(baud), write_buffer(), read_buffer(){//, DE(DigitalOut(DE_PIN, 0)
            set_blocking(false);

};

uint8_t MantracourtUART::ReadBuffer(){
    char message_type;
    volatile ssize_t bytes_read = 1;
    //First check the message type to determine how the message should be interpeted
    bytes_read = read(&message_type, 1);
    
    //Read the buffer until it is empty
    while(bytes_read > 0){
        
        //ACK
        if(message_type == '\r'){
        }
        //ACK with data
        else if((message_type == '+') || (message_type == '-') ){
            this->length_message = DP+DPB+3;
            read(&read_buffer, length_message);
            this->torque = atof(read_buffer);
        }
        //NACK
        else if(message_type == '?'){
            this->length_message = 2;
            read(&read_buffer, length_message);
        }
        else{
            sync();
            bytes_read = 0;
        }
        bytes_read = read(&message_type, 1);
    }
    return 0;
};

ssize_t MantracourtUART::TorqueRequest(int stationNumber){

    // this->write_buffer = torque_request_command;
    memcpy ( &this->write_buffer, &torque_request_command, sizeof(torque_request_command) );
    if(stationNumber == 0){
        this->write_buffer[3] = '0';
        //DE = true;
        return this->write(this->write_buffer, 11);
        //DE = false;
    }
    else if(stationNumber == 1){
        this->write_buffer[3] = '1';
        //DE = true;
        return this->write(this->write_buffer, 11);
        //DE = false;
    }
    else{
        return 0;
    }
};

float MantracourtUART::GetTorque(){
    //Check if there is something in the buffer
    if(readable()){
        //empty buffer to get latest infromation
        this->ReadBuffer();
    }

    return this->torque;
};

uint8_t MantracourtUART::StatusRequest(){
    this->write_buffer[0] = STATUS_REQUEST;
    return this->write(this->write_buffer, 1);
};

uint8_t MantracourtUART::GetEncoderStatus(){
    //Check if there is something in the buffer
    if(this->readable()){
        //empty buffer to get latest infromation
        this->ReadBuffer();
    }

    return this->encoder_status;
};
