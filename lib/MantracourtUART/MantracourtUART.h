/**
 * @defgroup self made Mantracourt RS485 over UART lib 
 * @brief Library for interfacing with Mantracourt Dcell 
 * 
 * Library now implements the most basic functionality. Namely all the main messges that contain the position and status.
 * Some advanced features are avaible to increase data rate and get more messages but do not seem necessary at the moment.
 * 
 * Made by Casper Hamster and Lena Grossman and heavilly based on Askim UART library
 */

#ifndef MANTRACOURTUART
#define MANTRACOURTUART

#include <mbed.h>

#define MAX_WRITE_BUFFER 11
#define MAX_READ_BUFFER 14

#define STATUS_REQUEST 'd'
#define DP 3
#define DPB 8

class MantracourtUART : private BufferedSerial{
    private:
        float torque;
        int baudrate;
        uint8_t encoder_status;
        int length_message;
        char torque_request_command[11] = {'!','0','0','0',':','S','O','U','T','?','\r' };
        char write_buffer[MAX_WRITE_BUFFER];
        char read_buffer[MAX_READ_BUFFER];
        //DigitalOut DE;

        /**
         * @brief (NOT IMPLEMENTED!) Sends sequence to Dcell to enable programming of configuration
         * 
         */
        void SendUnlockingSequence();

        /**
         * @brief Read entire buffer and store last received messages
         * 
         * @return 0 if bytes have been read; 1 if nothing is read
         */
        uint8_t ReadBuffer();

    public:

        //construcor
        MantracourtUART(PinName tx, PinName rx, int baud=MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE);
        //destructor
        ~MantracourtUART() = default;

        /**
         * @brief Returns last received position
         * 
         * @return 2 byte position value
         */
        float GetTorque();

        /**
         * @brief Returns encoder status
         * 
         * @return 1 bytes of information about the encoder status
         */
        uint8_t GetEncoderStatus();

        /**
         * @brief Request the status from the Aksim
         * 
         * @return the number of bytes written negative on failure
         */

        uint8_t StatusRequest();

        /**
         * @brief Request the position from the Aksim
         * 
         * @return The number of bytes written, negative error on failure
         */

        ssize_t TorqueRequest(int stationNumber);

        /**
         * @brief (NOT IMPLEMENTED) Could be used to shortern position request
         * 
         */
        uint8_t ShortPositionRequest();

        /**
         * @brief (NOT IMPLEMENTED) Could be used to change baudrate of communication 
         * 
         */
        uint8_t ChangeBaudrate(int new_baudrate);

        /**
         * @brief (NOT IMPLEMENTED) Self calibration could improve encoder accuracy
         * 
         */
        uint8_t SelfCalibration();

};

#endif //MantracourtUART
