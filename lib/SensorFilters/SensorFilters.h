#ifndef SENSOR_FILTERS_H
#define SENSOR_FILTERS_H

/**
 * @defgroup sensor_filters Sensor Filters
 * @author J.Q. Bouman MARCH VI
 * @date July 2021
 * @brief Different types of filters to smooth results from sensors. 
 * 
 * Sometimes sensors can give strong fluctuating values or with outliers,
 * making directly using the sensor results for decision making undoable.
 * Therefore a few well known filtering functions are implemented in this library.
 * @{ 
 */

/**
 * @brief Size of the Simple Moving Average buffer used to calculate the average.
 */
#define SMA_BUF_SIZE 10

/**
 * @class SimpleMovingAverage
 * @brief A filtering algorithm where the average of the last `n` values is taken.
 * 
 * The total number of values used in the filter are based on the `SMA_BUF_SIZE` value.
 * The dataset is called `buffer` and creates a ring buffer where values are stored.
 * The whole class is based around floats, which makes it possible to use either integers
 * or float as input for the filter.
 * 
 * This filter should be used when a more gradual change is needed, for example with temperature sensors.
 * Note that due to the buffer, this filter has a high memory footprint.
 */
class SimpleMovingAverage{
public:
  /**
   * @brief Construct a new Simple Moving Average object.
   * 
   * Nothing is done in this constructor. 
   */
  SimpleMovingAverage();

  /**
   * @brief Adds a new value to the buffer.
   * 
   * @param new_value Value to be added to the 
   * 
   * If the buffer is not yet filled the new value is just added to the buffer,
   * if the buffer is full the new value will overwrite the last value.
   */
  void add_value(float new_value);

  /**
   * @brief Get the average of the last n added values.
   * 
   * If the buffer is empty the value returned is zero.
   * If the buffer is not yet entirely filled,
   * the returned value is the average of the values present in the buffer.
   * Otherwise the result is the average of the enitre buffer.
   * 
   * @return The average of the buffer as float type.
   */
  float get_current_value();

  /**
   * @brief Resets the class to a newly created class.
   * 
   * The buffer is cleared (set to 0),
   * the total amount of items in the buffer is set 0 
   * and next overwritten value is the first in the buffer.
   */
  void reset();
private:
  /**
   * @brief Dataset on which the average is calculated.
   */
  float buffer[SMA_BUF_SIZE] = {0.0f};
  /// @brief Counter of total items in the buffer, maximum value is `SMA_BUF_SIZE`.
  int items_filled = 0;
  /// @brief The next position 
  int next_position = 0;
};

/**
 * @class ExponentialMovingAverage
 * @brief A filtering algorithm where new value is added based on a trust factor.
 * 
 * A simple filter with the aim to significantly reduce the impact of outliers.
 * This filter calculates the new value by adding the newly measured value
 * multiplied by a thrust factor. This thrust factor is set at creation of the filter.
 * 
 * This filter should be used when more recent values are deemed more worthy compared to older values.
 * The main advantage of this filter above other fitlers is it's low memory footprint.
 */
class ExponentialMovingAverage{
public:
  /**
   * @brief Construct a new Exponential Moving Average object.
   * 
   * @param alpha Directly sets the `trust_factor` and the `inverse_trust_factor`.
   * 
   * @note Setting `alpha` to 1 will be same as directly using the last entered value.
   * Setting `alpha` to zero will result in always returning a zero in `get_current_value()`.
   * 
   * `inverse_trust_factor` is calulated by removing `alpha` from 1.
   */
  ExponentialMovingAverage(float alpha);

  /**
   * @brief Add a newly measured value to the already filtered value.
   * 
   * @param new_value Newly measured value.
   * 
   * The current filtered value is calculated with the following formula:
   * `current_value = (trust_factor * new_value) + (inverse_trust_factor * current_value)`
   */
  void add_value(float new_value);

  /**
   * @brief Get the filtered value.
   * 
   * If `initial_value_set` is false it returns 0, 
   * otherwise the current filtered value is returned.
   * 
   * @return float 
   */
  float get_current_value();

  /// @brief Resets the filter by setting `initial_value_measured` to false.
  void reset();
private:
  /// @brief The current fitlered value.
  float current_value;
  /// @brief Trust factor, set by alfa in the constructor.
  float trust_factor;
  /// @brief Inverse of `trust_factor`, added for calculation speed.
  float inverse_trust_factor;
  /// @brief Boolean on whether the first value is added.
  bool initial_value_set = false;
};
/// @} sensor_filters
#endif // SENSOR_FILTERS_H