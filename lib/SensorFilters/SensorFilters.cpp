#include "SensorFilters.h"

// SimpleMovingAverage
SimpleMovingAverage::SimpleMovingAverage(){};

void SimpleMovingAverage::add_value(float new_value){
  // Add one to the number of items in the buffer, until full
  if(this->items_filled < SMA_BUF_SIZE){
    this->items_filled++;
  }
  // Add value to the buffer
  this->buffer[this->next_position] = new_value;
  // Move to the next position for the next value
  if(this->next_position < SMA_BUF_SIZE-1){
    this->next_position++;
  }else{
    this->next_position = 0;
  }
}

float SimpleMovingAverage::get_current_value(){
  float calculated_value = 0.0f;
  for(int i = 0; i < this->items_filled; i++){
    // Calculating it this way instead of dividing afterwards prevents float overflows 
    calculated_value += this->buffer[i] / (float)(this->items_filled);
  }
  return calculated_value;
}

void SimpleMovingAverage::reset(){
  for(int i = 0; i < SMA_BUF_SIZE; i++){
    this->buffer[i] = 0.0f;
  }
  this->next_position = 0;
  this->items_filled = 0;
}

// ExponentialMovingAverage
ExponentialMovingAverage::ExponentialMovingAverage(float alpha){
  this->trust_factor = alpha;
  this->inverse_trust_factor = 1-alpha;
}

void ExponentialMovingAverage::add_value(float new_value){
  // Check if inital value is set
  if(this->initial_value_set){
    this->current_value = (this->inverse_trust_factor * this->current_value) + (this->trust_factor * new_value);
  }else{
    this->current_value = new_value;
    this->initial_value_set = true;
  }
}

float ExponentialMovingAverage::get_current_value(){
  if(this->initial_value_set){
    return this->current_value;
  }
  return 0.0f;
}

void ExponentialMovingAverage::reset(){
  this->initial_value_set = false;
}