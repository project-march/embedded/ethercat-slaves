#include "spi.hpp"
// #include <Arduino.h>
#include <mbed.h>


SPI spi(DBS_ECAT_MOSI, DBS_ECAT_MISO, DBS_ECAT_SCK);
DigitalOut cs(DBS_ECAT_NCS);

void spi_setup(void)
{   
  spi_unselect(0);
  spi.format(8, SPI_MODE0);
  spi.frequency(SPIX_ESC_SPEED);

}

void spi_select (int8_t board)
{
    // Soft CSN
    #if SCS_ACTIVE_POLARITY == SCS_LOW
    cs = 0;
    #endif
}

void spi_unselect (int8_t board)
{
    // Soft CSN
    #if SCS_ACTIVE_POLARITY == SCS_LOW
    cs = 1;
    #endif
}

inline static uint8_t spi_transfer_byte(uint8_t byte)
{
    return spi.write(byte);
    // AVR will need handling last byte transfer difference,
    // but then again they pobably wont even fit EtherCAT stack in RAM
    // so no need to care for now
}

void write_ (int8_t board, uint8_t *data, uint8_t size)
{
    for(int i = 0; i < size; ++i)
    {
        spi_transfer_byte(data[i]);
    }
}

void read_ (int8_t board, uint8_t *result, uint8_t size)
{
	for(int i = 0; i < size; ++i)
    {
        result[i] = spi_transfer_byte(DUMMY_BYTE);
    }
}


void spi_bidirectionally_transfer (int8_t board, uint8_t *result, uint8_t *data, uint8_t size)
{
	for(int i = 0; i < size; ++i)
    {
        result[i] = spi_transfer_byte(data[i]);
    }
}
