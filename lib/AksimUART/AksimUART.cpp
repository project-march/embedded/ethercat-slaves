#include "AksimUART.h"


AksimUART::AksimUART(PinName tx, PinName rx, int baud) 
: BufferedSerial(tx, rx, baud), baudrate(baud), write_buffer(), read_buffer(){
            set_blocking(false);
};

uint8_t AksimUART::ReadBuffer(){
    char message_type;
    volatile ssize_t bytes_read = 1;
    //First check the message type to determine how the message should be interpeted
    bytes_read = read(&message_type, 1);

    //Read the buffer until it is empty
    while(bytes_read > 0){
        
        switch (message_type)
        {
        //position request
        //structure of datapackets can be found in Datasheets/MBD01_10EN_datasheet.pdf
        case POS_REQUEST:
            bytes_read = read(this->read_buffer, 3);
            //b23 to b4 is absolute position, b3 and b2 are zeropadding, b1 and b0 are encoder status
            this->abs_position = ((this->read_buffer[0]) << 12) | (this->read_buffer[1] << 4) | (this->read_buffer[2] >> 4);
            this->encoder_status = this->read_buffer[2] & 0x03;
            break;
        //position request + detailed status
        case STATUS_REQUEST:
            bytes_read = read(this->read_buffer, 4);
            //b23 to b4 is absolute position, b3 and b2 are zeropadding, b1 and b0 are encoder status
            this->abs_position = (this->read_buffer[0] << 12) | (this->read_buffer[1] << 4) | ((this->read_buffer[2]) >> 4);
            this->encoder_status = (this->read_buffer[4] ) | (this->read_buffer[3] << 8);
            break;
        //If message is unkown flush the buffer
        default:
            sync();
            bytes_read = 0;
            break;
        }

        bytes_read = read(&message_type, 1);
    }
    return 0;
};

ssize_t AksimUART::PositionRequest(){
    this->write_buffer[0] = POS_REQUEST;
    return this->write(this->write_buffer, 1);
};

uint32_t AksimUART::GetPosition(){
    //Check if there is something in the buffer
    if(readable()){
        //empty buffer to get latest infromation
        this->ReadBuffer();
    }

    return this->abs_position;
};

uint8_t AksimUART::StatusRequest(){
    this->write_buffer[0] = STATUS_REQUEST;
    return this->write(this->write_buffer, 1);
};

uint8_t AksimUART::GetEncoderStatus(){
    //Check if there is something in the buffer
    if(this->readable()){
        //empty buffer to get latest infromation
        this->ReadBuffer();
    }

    return this->encoder_status;
};