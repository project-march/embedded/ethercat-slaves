/**
 * @defgroup Aksim UART library
 * @brief Library for interfacing with RLS AKSIM encoder using UART communication
 * 
 * Library now implements the most basic functionality. Namely all the main messges that contain the position and status.
 * Some advanced features are avaible to increase data rate and get more messages but do not seem necessary at the moment.
 */

#ifndef AKSIMUART
#define AKSIMUART

#include <mbed.h>

#define MAX_WRITE_BUFFER 6
#define MAX_READ_BUFFER 6

#define POS_REQUEST '1'
#define STATUS_REQUEST 'd'

class AksimUART : private BufferedSerial{
    private:

        uint32_t abs_position;
        int baudrate;
        uint8_t encoder_status;
        int16_t temperature;
        char write_buffer[MAX_WRITE_BUFFER];
        uint8_t read_buffer[MAX_READ_BUFFER];

        /**
         * @brief (NOT IMPLEMENTED!) Sends sequence to Orbis to enable programming of configuration
         * 
         */
        void SendUnlockingSequence();

        /**
         * @brief Read entire buffer and store last received messages
         * 
         * @return 0 if bytes have been read; 1 if nothing is read
         */
        uint8_t ReadBuffer();

    public:

        //construcor
        AksimUART(PinName tx, PinName rx, int baud=MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE);
        //destructor
        ~AksimUART() = default;

        /**
         * @brief Returns last received position
         * 
         * @return 2 byte position value
         */
        uint32_t GetPosition();

        /**
         * @brief Returns encoder status
         * 
         * @return 1 bytes of information about the encoder status
         */
        uint8_t GetEncoderStatus();

        /**
         * @brief Request the status from the Aksim
         * 
         * @return the number of bytes written negative on failure
         */

        uint8_t StatusRequest();

        /**
         * @brief Request the position from the Aksim
         * 
         * @return The number of bytes written, negative error on failure
         */

        ssize_t PositionRequest();

        /**
         * @brief (NOT IMPLEMENTED) Could be used to shortern position request
         * 
         */
        uint8_t ShortPositionRequest();

        /**
         * @brief (NOT IMPLEMENTED) Could be used to change baudrate of communication 
         * 
         */
        uint8_t ChangeBaudrate(int new_baudrate);

        /**
         * @brief (NOT IMPLEMENTED) Self calibration could improve encoder accuracy
         * 
         */
        uint8_t SelfCalibration();

};

#endif //ORBISUART