#include "ADC128D818.h"

// Constructor
ADC128D818::ADC128D818(PinName SDA_PIN, PinName SCL_PIN) : bus(SDA_PIN, SCL_PIN)
{
  this->ADC128D818_read = (ADC128D818_address << 1) | 0x01;   // Shift left and set LSB to one
  this->ADC128D818_write = (ADC128D818_address << 1) & 0xFE;  // Shift left and set LSB to zero

  applyConfig();
}

// This helper function swaps the two bytes of the input argument
uint16_t ADC128D818::swapBytes(uint16_t data)
{
  uint8_t MSB, LSB;
  LSB = ((data >> 8) & 0xFF);           // The MSB of the data should be the LSB...
  MSB = (data & 0xFF);                  // ...and vice versa
  uint16_t swapped = (MSB << 8) | LSB;  // Stitch the two bytes back together
  return swapped;
}

bool ADC128D818::applyConfig(){
  int8_t results = 0;
  uint16_t command = 0x0000;

  // Set conversion ratings
  command = ( conversionRateSetting << 8) | conversionRateRegister;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);
  
  // Set standard configuration and start
  command = ( configurationSetting << 8) | configurationRegister;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);

  // Set advanced configuration
  command = ( advancedConfigurationSetting << 8) | advancedConfigurationRegister;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);

  // Set high and low limits of the different channels 0-7
  command = ( limitSettingHigh << 8) | limitRegisterIN0High;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);
  command = ( limitSettingHigh << 8) | limitRegisterIN1High;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);
  command = ( limitSettingHigh << 8) | limitRegisterIN2High;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);
  command = ( limitSettingHigh << 8) | limitRegisterIN3High;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);
  command = ( limitSettingHigh << 8) | limitRegisterIN4High;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);
  command = ( limitSettingHigh << 8) | limitRegisterIN5High;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);
  command = ( limitSettingHigh << 8) | limitRegisterIN6High;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);
  command = ( limitSettingHigh << 8) | limitRegisterIN7High;
  results |= bus.write(ADC128D818_write, (char*)&command, 2);

  return results;
}

uint16_t ADC128D818::readChannel(uint8_t reg){
  uint8_t cmd = reg;
  uint16_t value = 0;
  bus.write(ADC128D818_read, (char*)&cmd, 1);
  bus.read(ADC128D818_read, (char*)&value, 2);
  return swapBytes(value) >> 4;
}

uint16_t ADC128D818::readCurrent(uint8_t channel){
  uint8_t reg = channelReadingsRegisterIN0 + channel;
  if( reg >= channelReadingsRegisterIN0 && reg <= channelReadingsRegisterIN7)
    return readChannel(reg);
  else
    return 0;
}
