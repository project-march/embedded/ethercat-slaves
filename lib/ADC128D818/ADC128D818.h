#ifndef ADC128D818_H
#define ADC128D818_H

#include <mbed.h>
/**
 * @defgroup adc128d818 ADC128D818 
 * @brief A library to interface with the TI ADC182D818
 * 
 * Datasheet: https://www.ti.com/lit/ds/symlink/adc128d818.pdf
 * 
 * This ADC is quite extensive and has therefore many features.
 * Not all possible features are implemented in this library.
 * 
 * When using this library, check `ADC128D818ConfigurationSettings` first.
 * Change the settings when necessary.
 * 
 * @{
 */

/// Class to interface with the ADC128D818 analog to digital converter
class ADC128D818
{
private:
  /// Interface for transmitting data to and from the IC
  I2C bus;
  /// Base address of the IC, determined by hardware
  const uint8_t ADC128D818_address = 0x1D;
  /// Final read address
  uint8_t ADC128D818_read;
  /// Final write address
  uint8_t ADC128D818_write;

  /// Enumeration with register addresses (not complete)
  enum ADC128D818RegisterAddresses
  {
    configurationRegister = 0x00,
    interruptStatusRegister = 0x01,
    interruptMaskRegister = 0x03,
    conversionRateRegister = 0x07,
    channelDisableRegister = 0x08,
    oneShotRegister = 0x09,
    deepShutdownRegister = 0x0A,
    advancedConfigurationRegister = 0x0B,
    busyChannelRegisters = 0x0C,

    channelReadingsRegisterIN0 = 0x20,
    channelReadingsRegisterIN1 = 0x21,
    channelReadingsRegisterIN2 = 0x22,
    channelReadingsRegisterIN3 = 0x23,
    channelReadingsRegisterIN4 = 0x24,
    channelReadingsRegisterIN5 = 0x25,
    channelReadingsRegisterIN6 = 0x26,
    channelReadingsRegisterIN7 = 0x27,

    limitRegisterIN0High = 0x2A,
    limitRegisterIN0Low = 0x2B,
    limitRegisterIN1High = 0x2C,
    limitRegisterIN1Low = 0x2D,
    limitRegisterIN2High = 0x2E,
    limitRegisterIN2Low = 0x2F,
    limitRegisterIN3High = 0x30,
    limitRegisterIN3Low = 0x31,
    limitRegisterIN4High = 0x32,
    limitRegisterIN4Low = 0x33,
    limitRegisterIN5High = 0x34,
    limitRegisterIN5Low = 0x35,
    limitRegisterIN6High = 0x36,
    limitRegisterIN6Low = 0x37,
    limitRegisterIN7High = 0x38,
    limitRegisterIN7Low = 0x39,

    manufacturerIDRegister = 0x3E,
    revisionIDRegister = 0x3F,
  };

  /// Settings set at the start of creating the class
  enum ADC128D818ConfigurationSettings
  {
    configurationSetting = 0b00000001,
    conversionRateSetting = 0x01,
    advancedConfigurationSetting = 0x03,
    limitSettingLow = 0x00,
    limitSettingHigh = 0xFF,
  };

  /**
   * @brief Swap the position of two bytes
   * 
   * @param data Input data
   * @return Swapped version of the Input data
   */
  /// 
  uint16_t swapBytes(uint16_t data);

  /**
   * @brief Reads a specific channel of the IC
   * 
   * @param reg Register of the channel to be read
   * @return 12 bit value of the ADC
   */
  uint16_t readChannel(uint8_t reg);

  /**
   * @brief Sets all settings configured in the ADC128D818ConfigurationSettings
   * 
   * @return true When all data is set correctly
   * @return false When one or more data fields are not set correctly
   */
  bool applyConfig();

public:
  /**
   * @brief Construct a new ADC128D818 object
   * 
   * @param SDA_PIN Data pin for the I2C interface
   * @param SCL_PIN Clock pin for the I2C interface
   */
  ADC128D818(PinName SDA_PIN, PinName SCL_PIN);

  /**
   * @brief Reads one of the pins and returns the value as a 12-bit usnigned integer
   * 
   * @param channel Pin number on the IC to be read out
   * @return The 12 bit value read by the ADC
   */
  uint16_t readCurrent(uint8_t channel);
};
/// @}
#endif  // ADC128D818_H
