#include "ADS7828.h"

ADS7828::ADS7828(I2C* bus, bool A0, bool A1, bool singleEnded){
    this->bus = bus;

    // Determine read and write address according to A0 and A1
    if(A0){
        writeAddress |= A0_ADDRESS;
        readAddress |= A0_ADDRESS;
    }
    if(A1){
        writeAddress |= A1_ADDRESS;
        readAddress |= A1_ADDRESS;
    }

    // Single ended configuration determines the most significant bit of the 
    if(singleEnded){
        baseCommand |= SINGLE_ENDED_MODE;
    }
}

uint16_t ADS7828::readChannel(PowerDownSelection powerSelection, Channel channel){
    // Generate commands and 
    uint8_t writeCmd;
    // Set the channel and power selction bits in the command byte (page 9 of datasheet)
    // Command byte: (MSB) | SD | C2 | C1 | C0 | PD1 | PD0 | X | X | (LSB)
    // SD = Single ended or differential (is set in constructor part of baseCommand)
    // C2-C0 = Channel selection bits (powerSelection)
    // PD1-PD0 = Power down selection (channel)
    // X = Not used
    writeCmd = baseCommand | ((channel << 4) & CHANNEL_BITS) | ((powerSelection << 2) & POWER_SELECTION_BITS);
    bus->write(writeAddress, (char*) &writeCmd, 1);

    // Give IC time to 
    wait_us(ADC_CONVERSION_TIME);

    uint8_t readValue[2] = {0};
    bus->read(readAddress, (char*) readValue, 2);
    
    // First bit contains the MSB and the second bit the LSB
    return readValue[0] << 8 | readValue[1];
}