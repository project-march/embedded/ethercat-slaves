/**
 * @defgroup ads7828 ADS7828
 * @brief Analog to digital conversion IC by Texas Instruments.
 * @author J.Q. Bouman (MARCH VI)
 * 
 * Datasheet: https://www.ti.com/lit/ds/symlink/ads7828.pdf
 * 
 * This library is build for ADS7828 12-bit 8-channel analog to digital converter IC
 * and is based around the Mbed framework (tested on version 6) and inteded to be used 
 * on the Power Distribution Board of MARCH VI.
 * @{
 */
#ifndef ADS7828_H
#define ADS7828_H

#include <mbed.h>

// Configurable defines
/// Sleep time between command send and read register (us)
#define ADC_CONVERSION_TIME 500


// Fixed value defines
/// Bit to be set in address when A0 is high
#define A0_ADDRESS 0b00000010
/// Bit to be set in address when A1 is high
#define A1_ADDRESS 0b00000100
/// Bit to be set in base command when in single ended mode
#define SINGLE_ENDED_MODE 0b10000000
/// Power selection bits in command byte
#define POWER_SELECTION_BITS 0b00001100
/// Channel bits in command byte
#define CHANNEL_BITS 0b01110000

/// Interface for the ADS7828 analog to digital converter
class ADS7828 {
  public:
    /**
     * @enum PowerDownSelection
     * @brief All different power modes.
     * 
     * Power modes are activated after the conversion of the ADC is performed.
     */
    enum PowerDownSelection : uint8_t {
        AllOff = 0, ///< Power Down between A/D conversions
        ADOn = 1,   ///< Internal Reference OFF, A/D Converter ON
        IROn = 2,   ///< Internal Reference ON, A/D Converter OFF
        AllOn = 3   ///< Internal Reference On, A/D Converter ON
    };

    /**
     * @enum Channel
     * @brief Conversion layer from pin name to command number.
     * 
     * The command byte does not use a direct number conversion between the pin number and
     * command to be send. Therefore this enumeration will do the conversion.
     */
    enum Channel : uint8_t {
        CH0 = 0, 
        CH1 = 4,
        CH2 = 1,
        CH3 = 5,
        CH4 = 2,
        CH5 = 6,
        CH6 = 3,
        CH7 = 7
    };

    /**
     * @brief Constructor.
     * @param bus pointer to I2C interface (at 100kHz, 400kHZ or high speed mode (1.7MHz or 3.4MHz))
     * @param A0 Address pin (true = high, false = low)
     * @param A1 Address pin (true = high, false = low) 
     * @param singleEnded Selecting single ended or differential mode
     * 
     * Variables set in the constructor are all based on the hardware design
     */
    ADS7828(I2C* bus, bool A0, bool A1, bool singleEnded);

    /**
     * @brief Read out a specific channel and set the ADS7828 in a specific power mode afterwards.
     * @param powerSelection PowerDown mode after the conversion is done
     * @param channel The positive pole of the channel to be read out
     * @return Value read from the ADC, this is a 12 bit value
     * 
     * Variables set for this function can change per read iteration, in contrast to
     * variables set in the constructor, which are hardware based.
     * When in differential mode, the selected Channel is the positive lead
     */
    uint16_t readChannel(PowerDownSelection powerSelection, Channel channel);
  private:
    I2C* bus;

    // Final values of writeAddress and readAddress will be determined in the constructor
    uint8_t writeAddress = 0b10010001;
    uint8_t readAddress = 0b10010000;
    uint8_t baseCommand = 0b00000000;

};
///@}
#endif // ADS7828_H