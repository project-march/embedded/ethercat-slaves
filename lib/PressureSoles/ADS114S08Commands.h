//
// Created by ttveen on 04-12-20.
//

#ifndef ETHERCAT_SLAVES_ADS114S08COMMANDS_H
#define ETHERCAT_SLAVES_ADS114S08COMMANDS_H

#define WAIT_TIME_CSSC (20) //Wait time first sclk rising edge, after cs falling edge, in nanoseconds
#define WAIT_TIME_SCCS (20) //Wait time cs rising edge, after final sclk falling edge, in nanoseconds
#define WAIT_TIME_RST (2200) //Wait time after reset, in miliseconds

#define SPIFREQUENCY 1000000 // Clock frequency of the SPI

// Register Write Address
#define  INPMUXWRITE 0x42

// ADC commands
#define DATACOMMAND 0x12
#define STARTCOMMAND 0x08
#define RESETCOMMAND 0x06
#define WAKECOMMAND 0x02

#define DUMMYBYTE 0x00 // Sends only clock commands to receive data or register values

// ADC setting
#define PGA_GAIN 1
#define VREF_SOLES 5

// The whole sole consists of 8 sensors
#define SOLE_SENSORS 8

// AIN on the ADC mapped to the name of the 8 resistors in the sensor
// 0->HEEL R; 1->HEEL R; 2->MET 1; 3->HALLUX; 4->MET 3; 5->TOES; 6->MET 5; 7->ARCH
const int INPMUX_CONFIG [SOLE_SENSORS] {0x0B, 0x1B, 0x2B, 0x3B, 0x4B, 0x5B, 0x6B, 0x7B};
const int IDACMUX_CONFIG [SOLE_SENSORS] {0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7};

// The input the ADC will convert, and read. Will cycle trough AIN0 to AIN7
int selectedInput = 0;

#endif  // ETHERCAT_SLAVES_ADS114S08COMMANDS_H
