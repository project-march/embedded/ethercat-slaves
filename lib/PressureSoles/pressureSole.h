#ifndef ETHERCAT_SLAVES_PRESSURESOLE_H
#define ETHERCAT_SLAVES_PRESSURESOLE_H

#include <mbed.h>

namespace pressureSole
{
void startSoles();
void spiInit();
void csOn();
void csOff();
float* getData();
void startConv();
void addToQueue();
void prepareData();
void readData();
void addToQueue();
int readSingleRegister(uint8_t reg);
void writeSingleRegister(uint8_t reg, int regData);
uint16_t mergeBytes(int highByte, int lowByte);
void selectAnalogInput(int analogInput);
void selectIDAC(int analogInput);
float convertData(uint16_t data, int Vref, int PGAgain);
}
#endif  // ETHERCAT_SLAVES_PRESSURESOLES_H
