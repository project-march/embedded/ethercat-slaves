/*
ADS114S08 Product information & Datasheet: https://www.ti.com/product/ADS114S08.
The datasheet contains all register addresses, SPI commands and timing characteristics.
 */

#include <mbed.h>
#include "pressureSole.h"
#include "DBS_pindefs.h"
#include "ADS114S08Commands.h"

#define WAIT_TIME_CSSC (20) //Wait time first sclk rising edge, after cs falling edge, in nanoseconds
#define WAIT_TIME_SCCS (20) //Wait time cs rising edge, after final sclk falling edge, in nanoseconds

SPI psSPI(DBS_SPI3_MOSI, DBS_SPI3_MISO, DBS_SPI3_SCK); // mosi, miso, sclk
DigitalOut cs(DBS_SPI3_NCS);
DigitalOut reset(DBS_SPI3_RST);
InterruptIn dataReady(DBS_SPI3_DRDY);

EventQueue dataQueue;
Thread dataThread;

float convertedData[SOLE_SENSORS];

namespace pressureSole
{
/**
 * Makes the pressure soles and the ADC useable, by configuring the ADC and setting up a thread to handle the incoming
 * data.
 */
void startSoles(){
  // Initialise and start analog to digital conversion
  pressureSole::spiInit();
  pressureSole::startConv();

  // dataReady.fall will trigger when the ADC has new converted data available.
  // addToQueue() will add one readData() request to dataThread.
  // The global variable 'data' will be updated when there is processing power available.
  dataThread.start(callback(&dataQueue, &EventQueue::dispatch_forever));
  dataReady.fall(pressureSole::addToQueue);
}

/**
 * Reset the ADS114S08 and configure the SPI
 */
void spiInit()
{
  // Configure SPI
  reset = 1; // RESET needs to be high for functioning
  wait_us(WAIT_TIME_RST); // ADS114S0B needs time to reset
  psSPI.frequency(SPIFREQUENCY);
  psSPI.format(8, 1); // SPI mode for ADS114S0B

  // Make ADC operational
  csOn();
  psSPI.write(RESETCOMMAND);  // resetting device is recommended after every power down
  wait_us(WAIT_TIME_RST); // ADS114S0B needs time to reset
  psSPI.write(WAKECOMMAND);  // wake-up from power down mode

  // Turn on internal voltage reference
  writeSingleRegister(0x45, 0x12);
  writeSingleRegister(0x46, 0x05); // IDAC magnitude to 50 microAmpere
  writeSingleRegister(0x41, DUMMYBYTE);

  // Single-shot conversion mode
  writeSingleRegister(0x24, 0x34);

  // Initialise the ADC to read AIN0 first
  selectIDAC(IDACMUX_CONFIG[0]);
  selectAnalogInput(INPMUX_CONFIG[0]);

  csOff();
}

/**
 * Deselect the ADS114S08 to be able to communicate through SPI
 */
void csOff()
{
  wait_ns(WAIT_TIME_SCCS);
  cs = 1;
}

/**
* Select the ADS114S08 to allow other devices to communicate through SPI
*/
void csOn()
{
  cs = 0;
  wait_ns(WAIT_TIME_CSSC);
}

/**
 * Give the command that will start the analog to digital conversion in the ADC, continuous mode is default
 */
void startConv()
{
  csOn();
  psSPI.write(STARTCOMMAND); // Enable (analog-to-digital) conversion mode
  csOff();
}

/**
 * Write to a single register, and obtain its response
 * @param reg ADS114S08 register address
 * @return register value
 */
int readSingleRegister(uint8_t reg)
{
  csOn();
  psSPI.write(reg);
  psSPI.write(DUMMYBYTE);                 // number of register to read, minus 1
  int response = psSPI.write(DUMMYBYTE);  // obtain response
  csOff();
  return response;
}

/**
 * Overwrite a single register of the ADC
 * @param reg Write Address of a register
 * @param regData The new configuration, 8 bits
 */
void writeSingleRegister(uint8_t reg, int regData)
{
  psSPI.write(reg);
  psSPI.write(DUMMYBYTE);   // Number of registers, minus -1
  psSPI.write(regData);     // The register value will change to this byte
}

/**
 * Configure the ADC to sample the specified AIN
 * Example:
 * (analogInput = 0x08) would mean source one on AIN0 and source two on AIN8
 * @param analogInput 8 bytes, bit 0-3 AINp, 4-7 AINn
 */
void selectAnalogInput(int analogInput){
  writeSingleRegister(INPMUXWRITE, analogInput);
}

/**
 * Select the AIN port to which the two current excitation source of the ADC is connected
 * Example:
 * (analogInput = 0xB6) would mean source one on AIN11 (B in hex) and source two on AIN6
 * @param analogInput  8 bytes, bit 0-3 first source, 4-7 second source
 */
void selectIDAC(int analogInput){
  writeSingleRegister(0x47, analogInput); // Select the ports
}

/**
 * Reading the different AIN ports (Analog Input), different register settings are required. This function is called
 * after reading the data.
 * Firstly, this function increments the global variable 'selectedInput', so that a new input will be selected.
 * Secondly, this function rewrites certain register so that the settings correspond to the selected input.
 */
void prepareData(){
  csOn();

  // Increment 'selectedInput'
  if (selectedInput < SOLE_SENSORS){
    selectedInput++;
  } else {
    selectedInput = 0;
  }

  // Register settings to select the correct input
  selectIDAC(IDACMUX_CONFIG[selectedInput]);
  selectAnalogInput(INPMUX_CONFIG[selectedInput]);
  csOff();
  }

/**
 * This function updates the global variable 'data', with the new measurements from the ADC. The ADC outputs two
 * 8 bit bytes, which are merged to one 16 bit byte. This 16 bit byte is in two's complement and needs conversion to
 * voltage.
 */
void readData()
{
  const int dataBytes = 2;      // analog input is mapped to two 8 bit bytes
  int rawData[dataBytes];

  csOn();
  psSPI.write(DATACOMMAND);
  for (int j = 0; j < dataBytes; j++)
  {
    rawData[j] = psSPI.write(DUMMYBYTE);
  }

  // Data is received in two 8 bit bytes, forming one 16 bit byte
  uint16_t data = pressureSole::mergeBytes(rawData[0], rawData[1]);

  // Convert data to voltages
  convertedData[selectedInput] = pressureSole::convertData(data, VREF_SOLES, PGA_GAIN);

  csOff();
}

/**
 * This function is called when a new conversion happened. This function adds readData() to the queue, so that the
 * processor performs the more demanding readData() when it is ready. After reading the data, the ADC is configured to
 * read out the next analog input port, and start a new conversion.
 */
void addToQueue(){
  dataQueue.call(readData);
  dataQueue.call(prepareData);
  dataQueue.call(startConv);
}

/**
 * Return the global variable data.
 * @return Most recent measurements
 */
float* getData()
{
  return convertedData;
}

/**
 * Merge two 8 bit bytes into one 16 bit byte.
 * @param highByte bit 0 to 7
 * @param lowByte bit 8 to 15
 * @return Merged 16 bit byte
 */
uint16_t mergeBytes(int highByte, int lowByte)
{
  int mergedBytes = ((highByte << 8) | lowByte) ;
  return mergedBytes;
}

/**
 * Convert two's complement to positive and negative values, that denote the voltage difference between the input ports.
 * Note that the conversion is linear for the positive values, and for the negative values separately.
 * For more detail check the data sheet (link at beginning of the file).
 * @param data The bits outputted by the ADC
 * @param Vref The voltage put on the resistors
 * @param PGAgain The Programmable Amplifier Gain in the ADC
 * @return The voltage in Volts
 */
float convertData(uint16_t data, int Vref, int PGAgain){
  float convertedData;

  // The ADC outputs can be mapped to a range of -Vref to +Vref, where the first bit decides the sign,
  // so the 15 LSB (data & 0x7FFF) will determine where in the range the measurement is.
  float conversionRatio;

  // Check if first bit is 1 or 0
  if (data >> 15)
  {
    // First bit = 1, data is negative
    conversionRatio = ((float)(0x8000 - (data & 0x7FFF)) / 0x8000);
    convertedData = -Vref / PGAgain * conversionRatio;
  } else {
    // First bit = 0, data is positive (or zero)
    conversionRatio = ((float)(data & 0x7FFF) / 0x8000);
    convertedData = Vref / PGAgain * conversionRatio;
  }
  return convertedData;
}
}
