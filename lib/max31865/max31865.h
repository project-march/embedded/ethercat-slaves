/**
 * @defgroup max31865 MAX31865
 * @brief Resistor to digital conversion IC by Maxim Integrated.
 * 
 * Datasheet: https://datasheets.maximintegrated.com/en/ds/MAX31865.pdf
 * 
 * This RTD to digital converter is inteded to read out the temperature out of a PT100 sensor,
 * the configuration is set as a 3-wired PT100 sensor but 2 or 4 wires can also be used. 
 * This library is based around the Mbed framework (tested on version 6) and intended to be
 * used in the Joints/Motors. There are multiple ways to compute the temperature out of the PT100 resistance,
 * one is used here but others can be found in the datasheet. 
 * @{
 */


#ifndef MBED_MAX31865_H
#define MBED_MAX31865_H
 
#include "mbed.h"
#include <stdint.h>

#define MAX31856_CONFIG_REG            0x00
#define MAX31856_CONFIG_BIAS           0x80
#define MAX31856_CONFIG_MODEAUTO       0x40
#define MAX31856_CONFIG_MODEOFF        0x00
#define MAX31856_CONFIG_1SHOT          0x20
#define MAX31856_CONFIG_3WIRE          0x10
#define MAX31856_CONFIG_24WIRE         0x00
#define MAX31856_CONFIG_FAULTSTAT      0x02
#define MAX31856_CONFIG_FILT50HZ       0x01
#define MAX31856_CONFIG_FILT60HZ       0x00
 
#define MAX31856_RTDMSB_REG           0x01
#define MAX31856_RTDLSB_REG           0x02
#define MAX31856_HFAULTMSB_REG        0x03
#define MAX31856_HFAULTLSB_REG        0x04
#define MAX31856_LFAULTMSB_REG        0x05
#define MAX31856_LFAULTLSB_REG        0x06
#define MAX31856_FAULTSTAT_REG        0x07
 
 
#define MAX31865_FAULT_HIGHTHRESH     0x80
#define MAX31865_FAULT_LOWTHRESH      0x40
#define MAX31865_FAULT_REFINLOW       0x20
#define MAX31865_FAULT_REFINHIGH      0x10
#define MAX31865_FAULT_RTDINLOW       0x08
#define MAX31865_FAULT_OVUV           0x04

#define RTD_A 3.9083e-3
#define RTD_B -5.775e-7

typedef enum max31865_numwires { 
  MAX31865_2WIRE = 0,
  MAX31865_3WIRE = 1,
  MAX31865_4WIRE = 0
} max31865_numwires_t;
 
class max31865 {
    public:
    
    max31865(SPI& spi, DigitalOut cs);
    
    /**
     * @brief Initializes the max chip
     * 
     * @param x: the number of wires used by the RTD sensor
     */
    void Begin(max31865_numwires_t x = MAX31865_2WIRE);
    int ReadFault();
    void ClearFault();

    /**
     * @brief reads the resistor value from the max chip
     * 
     * @returns the resistor value 
     */    
    int ReadRTD();
    bool sensorPresent= true;

    /**
     * @brief Set the amount of wire the RTD sensor uses
     * 
     * @param wires: the number of wires
     */
    void SetWires(max31865_numwires_t wires);

    /**
     * @brief sets if the max chip should auto convert the RTD value
     * 
     * @param b: true to enable autoconvert
     */
    void AutoConvert(bool b);

    /**
     * @brief enables if a bais should be applied to the RTD value
     * 
     * @param b: true to encable bias
     */    
    void EnableBias(bool b);

    /**
     * @brief gives the temperature readout from the sensor
     * 
     * @param RTDnominal: resistance value at 0 degrees. (100 for pt100 RTD)
     * @param refResistor: resistance value of reference resistor
     * @param rtdVal: can be used if RTD value is read out earlier. Set to 0 if not used.
     * 
     * @return temperature measured
     */    
    float temperature(float RTDnominal, float refResistor, uint16_t rtdVal = 0);

    private:
    SPI& spi;
    DigitalOut cs;
    
    /**
     * @brief Read out N amount of registers from the max chip
     * 
     * @param adress: memory dress that needs to be read
     * @param buffer: buffer used to store the returned value
     * @param n: amount of bytes to read
     */    
    void ReadRegistorN(int address, int buffer[], int n);
    
    /**
     * @brief Read out 8 amount of registers from the max chip
     * 
     * @param adress: memory dress that needs to be read
     * 
     * @return value returned by the chip
     */    
    int ReadRegistor8(int address);

    /**
     * @brief Read out 16 amount of registers from the max chip
     * 
     * @param adress: memory dress that needs to be read
     * 
     * @return value returned by the chip
     */    
    int ReadRegistor16(int address);
    
    /**
     * @brief write integer to a register from the max chip
     * 
     * @param adress: memory dress that needs to be written
     * @param reg: value that needs to be written
     */    
    void WriteRegistor(int address, int reg);

    /**
     * @brief tranfer data using the SPI bus
     * 
     * @param x: data to be transefered
     */  
    int spiXfer(int x);
};
 
 
#endif