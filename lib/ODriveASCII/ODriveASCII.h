/** 
 * @defgroup odriveascii ODriveASCII
 * @brief Serial communication library for ODrive
 * 
 * ODrive firmware: custom (extra commands).
 * 
 * For more information about the protocol see: https://docs.odriverobotics.com/ascii-protocol.html
 * @{
 */
#ifndef ODRIVEASCII
#define ODRIVEASCII

#ifdef PLATFORM_NATIVE
#include <mock/mbed.hpp>
#else
#include <mbed.h>
#endif

#include <math.h>

/// Size of the character buffer, value is based on some of the longest commands found
#define BUFFER_SIZE 128
/// Size of the larger buffer (for larger commands)
#define LARGE_BUFFER_SIZE 256
/// Size of the medium buffer, for smaller commands
#define MEDIUM_BUFFER_SIZE 32
/// Some commands use only a few characters, large buffers would be a waste of space
#define SMALL_BUFFER_SIZE 8
/** 
 * @brief Defines the amount of decimals for the floating point
 * conversion. A value of `10e3f` means 4 decimals.
 * A value of 10eNf` means N+1 decimals for an integer N.*/
#define DECIMAL_MULTIPLIER 10e3f
/// Time to wait between set state function and testing the new state in function setState(us)
#define STATE_WAIT_TIME 10e5

#define TORQUEDEBUG

/// Class to interface with the ODrive over the ASCII protocol
class ODriveASCII {
public:
    /**
     * @brief States in which the ODrive motor can be situated.
     * 
     * This is far from a complete list of all states. These are the states most used by us.
     */
    enum MotorState: uint8_t {
        UNDEFINED = 0,                  ///< will fall through to idle
        IDLE = 1,                       ///< disable PWM and do nothing
        STARTUP_SEQUENCE = 2,           ///< the actual sequence is defined by the config.startup_... flags
        FULL_CALIBRATION_SEQUENCE = 3,  ///< run all calibration procedures, then idle
        MOTOR_CALIBRATION = 4,          ///< run motor calibration
        SENSORLESS_CONTROL = 5,         ///< run sensorless control
        ENCODER_INDEX_SEARCH = 6,       ///< run encoder index search
        ENCODER_OFFSET_CALIBRATION = 7, ///< run encoder offset calibration
        CLOSED_LOOP_CONTROL = 8         ///< run closed loop control
    };

    // Constructor
    #ifdef PLATFORM_NATIVE
        /**
         * @brief Construct a new ODriveASCII object
         * 
         * @param serial Serial interface to ODrive
         */
        ODriveASCII(MockSerial* serial);
    #else
        #if defined MBED_MAJOR_VERSION && MBED_MAJOR_VERSION < 6
        /**
         * @brief Construct a new ODriveASCII object
         * 
         * @param serial Serial interface to ODrive
         */
        ODriveASCII(UARTSerial* serial);
        #else
        /**
         * @brief Construct a new ODriveASCII object
         * 
         * @param serial Serial interface to ODrive
         */
        ODriveASCII(BufferedSerial* serial);
        #endif
    #endif

    // Function(s) for operating the ODrive

    /** 
     * @brief Request the position and velocity of an axis
     * @param axis: The axis number, 0 or 1 (uint8_t)
     * @param position: place to put the position value (float*)
     * @param velocity: place to put the velocity value (float*) 
     */
    void requestFeedback(uint8_t axis, float* position, float* velocity);
    
    /**
     * @brief Reads the custom command made to return the exact variables needed by MARCH to be send through reads a single axis
     * 
     * @param axis The axis number, 0 or 1 (uint8_t)
     * @param state Pointer to a place to store the state of given axis
     * @param absolutePosition Pointer to a place to store the absolute position (absolute encoder) of given axis
     * @param motorPosition Pointer to a place to store the motor position (incremental encoder) of given axis
     * @param velocity Pointer to a place to store the velocity of given axis
     * @param current Pointer to a place to store the current of given axis
     * @param temperature Pointer to a place to store the temperature of given axis
     */
    bool customMARCHCommand(uint8_t axis, int32_t* state, int32_t* absolutePosition, int32_t* motorPosition, 
                            float* velocity, float* current, float* temperature, float* measured_torque);

    
    void requestMARCHData();

    /**
     * @brief Reads the custom command made to return the exact variables needed by MARCH to be send through reads both axes
     * 
     * @param axis0state Pointer to a place to store the state of axis 0
     * @param axis0absolutePosition Pointer to a place to store the absolute position (absolute encoder) of axis 0
     * @param axis0motorPosition Pointer to a place to store the motor position (incremental encoder) of axis 0
     * @param axis0velocity Pointer to a place to store the velocity of axis 0
     * @param axis0current Pointer to a place to store the current of axis 0
     * @param axis0temperature Pointer to a place to store the temperature of axis 0
     * @param axis1state Pointer to a place to store the state of axis 1
     * @param axis1absolutePosition Pointer to a place to store the absolute position (absolute encoder) of axis 1
     * @param axis1motorPosition Pointer to a place to store the motor position (incremental encoder) of axis 1
     * @param axis1velocity Pointer to a place to store the velocity of axis 1
     * @param axis1current Pointer to a place to store the current of axis 1
     * @param axis1temperature Pointer to a place to store the temperature of axis 1
     */
    bool customMARCHCommand(int32_t* axis0state, int32_t* axis0odriveerror, int32_t* axis0motorPosition, 
                            float* axis0velocity, float* axis0current, float* axis0temperature, float* axis0torque,
                            int32_t* axis1state, int32_t* axis1odriveerror, int32_t* axis1motorPosition, 
                            float* axis1velocity, float* axis1current, float* axis1temperature, float* axis1torque);

     /**
     * @brief Reads the semi-custom command which returns error values, not all possible errors, for a single axis
     * @param axis: The axis number, 0 or 1 (uint8_t)
     * @param error: Axis error for the selected axis
     * @param motor: Motor error as defined by ODrive
     * @param encoder: Encoder error as defined by ODrive
     * @param controller: Controller error as defined by ODrive
     */
    void customErrorCommand(uint8_t axis, uint32_t* error, uint32_t* motor, uint32_t* encoder, uint32_t* controller);
    
    /** 
     * @brief Calculates checksum of the message 
     * @param message: string containing the message
     * @return checksum value
     */
    int calculateChecksum(const string message);

    bool MessageIsCorrect(const char* inputBuffer, int bufferLength);
    /** 
     * @brief Set new torque set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param torque: Is the desired torque in [Nm] (float) 
     */
    void setTorque(uint8_t axis, float torque);

    /** 
     * @brief Set new trajectory set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param destination: Is the goal position, in [turns] (float) 
     */
    void setTrajectory(uint8_t axis, float destination);

    /** 
     * @brief Set new Position set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param position: Is the desired position, in [turns] (float)
     */
    void setPosition(uint8_t axis, float position);

    /** 
     * @brief Set new Position set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param position: Is the desired position, in [turns] (float)
     * @param velocity_lim: Is the velocity limit, in [turns/s] (float, 0.0f)
     * @param torque_lim: Is the torque limit, in [Nm] (float, 0.0f) 
     */
    void setPosition(uint8_t axis, float position, float velocity_lim, float torque_lim);

    /**
     * @brief Set new Torque set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param force: Is the desired torque, in [Nm] (float)
     */
    void setMoment(uint8_t axis, float torque);

    /** 
     * @brief Set new Torque set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param force: Is the desired torque, in [Nm] (float)
     * @param velocity_lim: Is the velocity limit, in [turns/s] (float, 0.0f)
     * @param torque_lim: Is the torque limit, in [Nm] (float, 0.0f) 
     */
    void setMoment(uint8_t axis, float torque, float velocity_lim, float torque_lim);

    /** 
     * @brief Set new Torque set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param position_weight: Is the position weight
     * @param torque_weight: Is the torque weight
     */
    void setFuzzyWeights(uint8_t axis, float position_weight, float torque_weight);

            /** 
     * @brief Set new Torque set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param position: the position setpoint
     * @param torque: the torque setpoint
     * @param position_weight: Is the position weight
     * @param torque_weight: Is the torque weight
     */
    void setFuzzy(uint8_t axis, float position, float torque, float position_weight, float torque_weight);

        /** 
     * @brief Set new Torque set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param position: the position setpoint
     * @param torque: the torque setpoint
     * @param position_weight: Is the position weight
     * @param torque_weight: Is the torque weight
     * @param velocity_lim: Is the velocity limit, in [turns/s] (float, 0.0f)
     * @param torque_lim: Is the torque limit, in [Nm] (float, 0.0f) 
     */
    void setFuzzy(uint8_t axis, float position, float torque, float position_weight, float torque_weight, float velocity_lim, float torque_lim);


    /**
     * @brief Set the Abs Postion object of axis
     * 
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param absolutePosition: Is the absolute position, in [Todo] (int)
     */
    bool setAbsPostion(uint8_t axis, int32_t absolutePosition);

    /**
     * @brief Set the pid values for the position
     * 
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param positionP: Is the P gain for PID tuning position control, in float
     * @param positionI: Is the I gain for PID tuning position control, in float
     * @param positionD: IIs the D gain for PID tuning position control, in float
     */
    bool setPositionPID(uint8_t axis, float positionP, float positionI, float positionD);

        /**
     * @brief Set the pid values for the torque
     * 
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param momentP: Is the P gain for PD tuning torque control, in float
     * @param momentD: IIs the D gain for PD tuning torque control, in float
     */
    bool setMomentPD(uint8_t axis, float momentP, float momentD);
    
    /** 
     * @brief Set new velocity set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param velocity: Is the desired velocity in [turns/s] (float)
     */
    void setVelocity(uint8_t axis, float velocity);
    /** 
     * @brief Set new velocity set-point to axis
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param velocity: Is the desired velocity in [turns/s] (float)
     * @param torque_ff: Is the torque feed-forward term, in [Nm] (float, 0.0f) 
     */
    void setVelocity(uint8_t axis, float velocity, float torque_ff);

    // Function(s) for changing parameters of the ODrive
    /** 
     * @brief Read an integer parameter specified in property parameter 
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param property: Name of the property, as seen in ODrive Tool (string)
     * @param value: Pointer to an integer, used to return the read parameter (int*) 
     */
    void readParameter(uint8_t axis, const string property, int* value);

    /** 
     * @brief Read a float parameter specified in property parameter 
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param property: Name of the property, as seen in ODrive Tool (string)
     * @param value: Pointer to an float, used to return the read parameter (float*) 
     */
    void readParameter(uint8_t axis, const string property, float* value);
    
    /** 
     * @brief Read a bool parameter specified in property parameter 
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param property: Name of the property, as seen in ODrive Tool (string)
     * @param value: Pointer to an bool, used to return the read parameter (bool*) 
     */
    void readParameter(uint8_t axis, const string property, bool* value);

    /** 
     * @brief Set een integer typed property
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param property: Name of the property, as seen in ODrive Tool (string)
     * @param value: Value of the property to be written (int) 
     */
    void writeParameter(uint8_t axis, const string property, int value);

    /** 
     * @brief Set een float typed property
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param property: Name of the property, as seen in ODrive Tool (string)
     * @param value: Value of the property to be written (float) 
     */
    void writeParameter(uint8_t axis, const string property, float value);

    // Function(s) for system commands
    /** 
     * @brief Change run state to a desired state
     * @param axis: Is the axis number, 0 or 1 (uint8_t)
     * @param newState: The new desired axis state (MotorState)
     * @return confirmation whether the new state is reached 
     */
    bool setState(uint8_t axis, MotorState newState);

    /** 
     * @brief Save the current configuration to ODrive 
     */
    void saveConfig();

    /** 
     * @brief Erase the current configuration from ODrive 
     */
    void eraseConfig();

    /** 
     * @brief Reboot the ODrive 
     */
    void reboot();

    /** 
     * @brief Clear all errors of the ODrive 
     */
    void clearErrors();

    /**
     * @brief Check whether an axis of the ODrive still needs to perform encoder index search
     * @param axis: Number of the axis, either 0 or 1
     * @return Returns a boolean. 
     *         If true is returned, then the index of the encoder is already found
     *         If false is returned, then the ODrive still needs to perform encoder index serach
     */
    bool isEncoderIndexFound(int axis);

    /**
     * @brief Check whether the encoder is pre calibrated
     * @param axis: Number of the axis, either 0 or 1
     * @return Returns a boolean indiciating whether the encoder is pre calibrated
     */
    bool isEncoderPreCalibrated(int axis);

    /**
     * @brief Check whether the absolute encoder gpio pin is valid (either 7 or 8)
     * @param axis: Number of the axis, either 0 or 1
     * @return Returns a boolean indiciating whether absolute encoder gpio pin is valid (either 7 or 8)
     */
    bool isAbsoluteGPIOPinValid(int axis);

protected:
    // Functions

    /** 
     * @brief Write message to serial/UART connection of the ODrive
     * @param message to be send (string)
     * @return Gives true when the interface is writeable and message is set in buffer, false otherwise (bool) 
     */
    void writeToSerial(const string message);

    /** 
     * @brief Reads integer value from the serial interface
     * @return the retrieved value from serial interface (uint32_t) 
     */
    int32_t readInt();

    /** 
     * @brief Reads float value from the serial interface
     * @return The retrieved value from the serial interface (float) 
     */
    float readFloat();

    /** 
     * @brief Reads bool value from the serial interface
     * @return The retrieved value from the serial interface (bool) 
     */
    bool readBool();

    /** 
     * @brief Reads the response of a command to a character array
     * @param buffer array of length BUFFER_SIZE (char*)
     * @return Whether an end of line is found, indicating the end of the response 
     */
    bool readCharArray(char* buffer);

    /** 
     * @brief Used after readCharArray to find the float starting from startPosition until the next `\n`, `\r` or ` `
     * @param buffer: the string in which the float is searched (char*)
     * @param startPosition: starting position within the buffer, character on startPosition is used in conversion (uint32_t)
     * @param result: pointer to variable in which the resulting conversion is stored (float*)
     * @return End of the found float 
     */
    uint32_t findFloat(const char* buffer, uint32_t startPosition, float* result);

    /** 
     * @brief Used after readCharArray to find the integer starting from startPosition until the next `\n`, `\r` or ` `
     * @param buffer: the string in which the integer is searched (char*)
     * @param startPosition: starting position within the buffer, character on startPosition is used in conversion (uint32_t)
     * @param result: pointer to variable in which the resulting conversion is stored (int32_t*)
     * @return End of the found integer 
     */
    uint32_t findInt(const char* buffer, uint32_t startPosition, int32_t* result);

    /**
     * @brief Convert a float to string (not implemented in all versions of Mbed)
     * 
     * @param f Input float
     * @return String of the inputted float
     */
    string floatToString(float f);
private:
    // Variables
    #ifdef PLATFORM_NATIVE
        /**
         * @brief Serial interface for testing purposes
         */
        MockSerial* oDriveSerial;
    #else
        #if MBED_MAJOR_VERSION < 6
        /**
         * @brief Serial interface to the ODrive. Used for compatability with Mbed 5.
         */
        UARTSerial* oDriveSerialmbed5;
        #else
        /**
         * @brief Serial interface to the ODrive.
         */
        BufferedSerial* oDriveSerial;
        #endif
    #endif
};
///@} odriveascii
#endif // ODRIVEASCII
