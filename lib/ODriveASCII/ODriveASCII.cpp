#include "ODriveASCII.h"

// Public functions
#if defined MBED_MAJOR_VERSION && MBED_MAJOR_VERSION < 6
ODriveASCII::ODriveASCII(UARTSerial* serial){
    oDriveSerialmbed5 = serial;
    oDriveSerialmbed5->set_blocking(false);
}
#else
#ifdef PLATFORM_NATIVE
ODriveASCII::ODriveASCII(MockSerial* serial){
#else
ODriveASCII::ODriveASCII(BufferedSerial* serial){
#endif
    oDriveSerial = serial;
    oDriveSerial->set_blocking(false);
}
#endif

void ODriveASCII::requestFeedback(uint8_t axis, float* position, float* velocity){
    if(axis != 0 && axis != 1){return;}
    const string message = "f " + std::to_string(axis) + "\n";
    writeToSerial(message);
    char buffer[LARGE_BUFFER_SIZE];
    readCharArray(buffer);
    uint32_t pos = 0;
    pos = findFloat(buffer, pos, position);
    pos = findFloat(buffer, pos+1 /*step over whitespace*/, velocity);
}

bool ODriveASCII::customMARCHCommand(uint8_t axis, int32_t* state, int32_t* absolutePosition, int32_t* motorPosition, 
                                    float* velocity, float* current, float* temperature, float* measured_torque){
                                        
    if(axis != 0 && axis != 1){return false;}
    bool command_succesful = true;
    string command = "m " + std::to_string(axis);
    const string message = command + "*" + std::to_string(calculateChecksum(command)) + "\n";
    writeToSerial(message);
    char readBuffer[LARGE_BUFFER_SIZE];
    command_succesful = readCharArray(readBuffer);
    command_succesful = MessageIsCorrect(readBuffer, LARGE_BUFFER_SIZE);
    if(!command_succesful){
        return false;
    }
    uint32_t pos = 0;
    pos = findInt(readBuffer, pos, state);
    pos = findInt(readBuffer, pos+1, absolutePosition);
    pos = findInt(readBuffer, pos+1, motorPosition);
    pos = findFloat(readBuffer, pos+1, velocity);
    pos = findFloat(readBuffer, pos+1, current);
    pos = findFloat(readBuffer, pos+1, temperature);
    pos = findFloat(readBuffer, pos+1, measured_torque);
    // printf("value in measured torque is now: %f \n", measured_torque);
    return true;
}

int ODriveASCII::calculateChecksum(const string message){
    int checksum = 0;
    for (char const &c: message) {
       checksum ^= c;
    }
    return checksum;
}


bool ODriveASCII::MessageIsCorrect(const char* inputBuffer, int bufferLength){

    uint8_t checksum = 0;
    size_t checksum_start = LARGE_BUFFER_SIZE;
    //Calculate the checksum of the message end find the checksum value send with it
    for (size_t i = 0; i < bufferLength; ++i) 
        if (inputBuffer[i] == '*') {
            checksum_start = i + 1;
            //message has ended checksum start contains the adress of the checksum value
            break;
        } else {
            checksum ^= inputBuffer[i];
        }
    

    unsigned int received_checksum;
    int numscan = sscanf(&inputBuffer[checksum_start], "%u", &received_checksum);
    return (numscan >= 1) && (received_checksum == checksum);
}

void ODriveASCII::requestMARCHData(){                              
    string command = "m ";
    const string message = command + "*" + std::to_string(calculateChecksum(command)) + "\n";
    writeToSerial(message);
}

bool ODriveASCII::customMARCHCommand(int32_t* axis0state, int32_t* axis0odriveerror, int32_t* axis0motorPosition, 
                                     float* axis0velocity, float* axis0current, float* axis0temperature, float* axis0torque,
                                     int32_t* axis1state, int32_t* axis1odriveerror, int32_t* axis1motorPosition, 
                                     float* axis1velocity, float* axis1current, float* axis1temperature, float* axis1torque
){
    bool command_succesful = true;                                    
    char readBuffer[LARGE_BUFFER_SIZE];
    command_succesful = readCharArray(readBuffer);
    command_succesful = MessageIsCorrect(readBuffer, LARGE_BUFFER_SIZE);
    if(!command_succesful){
        return false;
    }

    uint32_t pos = 0;
    pos = findInt(readBuffer, pos, axis0state);
    pos = findInt(readBuffer, pos+1, axis0odriveerror);
    pos = findInt(readBuffer, pos+1, axis0motorPosition);
    pos = findFloat(readBuffer, pos+1, axis0velocity);
    pos = findFloat(readBuffer, pos+1, axis0current);
    pos = findFloat(readBuffer, pos+1, axis0temperature);
    pos = findFloat(readBuffer, pos+1, axis0torque);
    pos = findInt(readBuffer, pos+1, axis1state);
    pos = findInt(readBuffer, pos+1, axis1odriveerror);
    pos = findInt(readBuffer, pos+1, axis1motorPosition);
    pos = findFloat(readBuffer, pos+1, axis1velocity);
    pos = findFloat(readBuffer, pos+1, axis1current);
    pos = findFloat(readBuffer, pos+1, axis1temperature);
    pos = findFloat(readBuffer, pos+1, axis1torque);
    return true;
}

// Transform the provided float to a string with at 4 decimal numbers after
// the decimal. The value is floored, not rounded, before displaying the 4 decimals.
string ODriveASCII::floatToString(float f) {
    if (std::isnan(f) || std::isinf(f)) {
        return string("0.0");
    }

    // Get the part before the decimal point
    int32_t n = (int32_t)f;

    // Get the part behind the decimal point
    float decimal = (f - n);

    // Take the absolute value of the value behind the decimal point
    if (decimal < 0) {
        decimal *= -1;
    }

    int32_t decimal_int = (int32_t)(decimal * DECIMAL_MULTIPLIER);

    char floatString[MEDIUM_BUFFER_SIZE];
    // If the float is less than zero, then we prepend a minus sign. To avoid
    // a double minus sign, we multiply n by -1 if it is negative.
    // This is necessary to represent values in the range (-1,0) correctly.
    if (f < 0) {
        n *= -1;
        sprintf(floatString, "-%li.%04li", n, decimal_int);
    } else {
        sprintf(floatString, "%li.%04li", n, decimal_int);
    }

    return string(floatString);
}

void ODriveASCII::customErrorCommand(uint8_t axis, uint32_t* error, uint32_t* motor, uint32_t* encoder, uint32_t* controller){
    // Enforce a correct axis number
    if(axis != 0 && axis != 1){return;}
    // ASCII instruction to be send to the ODrive
    const string message = "x " + std::to_string(axis) + "\n";
    writeToSerial(message);
    // Read the complete respond from the ODrive
    char readBuffer[LARGE_BUFFER_SIZE];
    readCharArray(readBuffer);
    uint32_t pos = 0;
    // Error values are unsigned integers an therefore needs to be cast from a regular 32bit integer to a unsigned integer.
    int32_t readError = 0;
    pos = findInt(readBuffer, pos, &readError);
    *error = (uint32_t)readError;
    pos = findInt(readBuffer, pos+1, &readError);
    *motor = (uint32_t)readError;
    pos = findInt(readBuffer, pos+1, &readError);
    *encoder = (uint32_t)readError;
    pos = findInt(readBuffer, pos+1, &readError);
    *controller = (uint32_t)readError;
}

void ODriveASCII::setTorque(uint8_t axis, float torque){
    // c axis current
    if(axis != 0 && axis != 1){return;}
    const string message = "c " + std::to_string(axis) + " " + floatToString(torque) + "\n";
    writeToSerial(message); 
}


void ODriveASCII::setTrajectory(uint8_t axis, float destination){
    // t axis destination
    if(axis != 0 && axis != 1){return;}
    const string message = "t " + std::to_string(axis) + " " + floatToString(destination) + "\n";
    writeToSerial(message);
}


void ODriveASCII::setPosition(uint8_t axis, float position){
    if(axis != 0 && axis != 1){return;}
    setPosition(axis, position, 0.0f, 0.0f);
}


void ODriveASCII::setPosition(uint8_t axis, float position, float velocity_lim, float torque_lim){
    // p axis position velocity_lim torque_lim
    if(axis != 0 && axis != 1){return;}
    const string message = "p " + std::to_string(axis) + " " +
                               floatToString(position) + " " +
                           floatToString(velocity_lim) + " " +
                             floatToString(torque_lim) + "\n";
    writeToSerial(message);
}

void ODriveASCII::setMoment(uint8_t axis, float torque){
    // TORQUEDEBUG LINE - this will stop the DieBo from sending the torque
    // #ifdef TORQUEDEBUG
    // printf("Set torque of axis %i to: %f", axis, torque);
    // #endif

    if(axis != 0 && axis != 1){return;}
    setMoment(axis, torque, 0.0f, 0.0f);
}

void ODriveASCII::setMoment(uint8_t axis, float torque, float velocity_lim, float torque_lim){
    // p axis position velocity_lim torque_lim
    if(axis != 0 && axis != 1){return;}
    const string message = "b " + std::to_string(axis) + " " +
                               floatToString(torque) + " " +
                           floatToString(velocity_lim) + " " +
                             floatToString(torque_lim) + "\n";
    writeToSerial(message);
}

void ODriveASCII::setFuzzyWeights(uint8_t axis, float position_weight, float torque_weight){
    // p axis position velocity_lim torque_lim
    if(axis != 0 && axis != 1){return;}
    const string message = "d " + std::to_string(axis) + " " +
                               floatToString(position_weight) + " " +
                             floatToString(torque_weight) + "\n";
    writeToSerial(message);
}

void ODriveASCII::setFuzzy(uint8_t axis, float position, float torque, float position_weight, float torque_weight){
    if(axis != 0 && axis != 1){return;}
    setFuzzy(axis, position, torque, position_weight, torque_weight, 0.0f, 0.0f);
}

void ODriveASCII::setFuzzy(uint8_t axis, float position, float torque, float position_weight, float torque_weight, float velocity_lim, float torque_lim){
    // p axis position velocity_lim torque_lim
    if(axis != 0 && axis != 1){return;}
    const string message = "F " + std::to_string(axis) + " " +
                             floatToString(position) + " " +
                             floatToString(torque) + " " +
                             floatToString(position_weight) + " " +
                             floatToString(torque_weight) + " " +
                             floatToString(velocity_lim) + " " +
                             floatToString(torque_lim) + "\n";
    writeToSerial(message);
}

bool ODriveASCII::setAbsPostion(uint8_t axis, int32_t absolutePosition){
    if(axis != 0 && axis != 1){return false;}
        const string message = "a " + std::to_string(axis) + " " +
                               floatToString(static_cast<float>(absolutePosition)) + "\n";
    writeToSerial(message);

    int32_t result = readInt();
    if(result != absolutePosition){
        printf("sent wrong result: %i, should be: %i", result, absolutePosition);
        return false;
    }
    printf("sent correct result: %i, should be: %i", result, absolutePosition);
    return true;

}

bool ODriveASCII::setPositionPID(uint8_t axis, float positionP, float positionI, float positionD){
    if(axis != 0 && axis != 1){return false;}
        const string message = "P " + std::to_string(axis) + " " +
                                  floatToString(positionP) + " " + 
                                  floatToString(positionI) + " " + 
                                  floatToString(positionD) + "\n";
    writeToSerial(message);

    // Read the complete respond from the ODrive
    char readBuffer[LARGE_BUFFER_SIZE];
    readCharArray(readBuffer);

    uint32_t pos = 0;
    // Error values are unsigned integers an therefore needs to be cast from a regular 32bit integer to a unsigned integer.
    float readValue = 0;
    pos = findFloat(readBuffer, pos, &readValue);
    if(readValue != positionP){ //TODO: Add offset to get rid of floating point error
        printf("Failed P: %f", readValue);
        printf("Needed to be: %f\n", positionP);
        return false;
    }
    pos = findFloat(readBuffer, pos+1, &readValue);
    if(readValue != positionI){
        printf("Failed I: %f\n", readValue);
        printf("Needed to be: %f\n", positionI);
        return false;
    }
    pos = findFloat(readBuffer, pos+1, &readValue);
    if(readValue != positionD){
        printf("Failed D: %f", readValue);
        printf("Needed to be: %f\n", positionD);
        return false;
    }
    return true;
}

bool ODriveASCII::setMomentPD(uint8_t axis, float momentP, float momentD){
    if(axis != 0 && axis != 1){return false;}
        const string message = "T " + std::to_string(axis) + " " +
                                  floatToString(momentP) + " " + 
                                  floatToString(momentD) + "\n";
    writeToSerial(message);

    // Read the complete respond from the ODrive
    char readBuffer[LARGE_BUFFER_SIZE];
    readCharArray(readBuffer);

    uint32_t pos = 0;
    // Error values are unsigned integers an therefore needs to be cast from a regular 32bit integer to a unsigned integer.
    float readValue = 0;
    pos = findFloat(readBuffer, pos, &readValue);
    if(readValue != momentP){
        printf("Failed P: %f", readValue);
        printf("Needed to be: %f\n", momentP);
        return false;
    }
    pos = findFloat(readBuffer, pos+1, &readValue);
    if(readValue != momentD){
        printf("Failed D: %f", readValue);
        printf("Needed to be: %f\n", momentD);
        return false;
    }
    return true;
}


void ODriveASCII::setVelocity(uint8_t axis, float velocity){
    if(axis != 0 && axis != 1){return;}
    setVelocity(axis, velocity, 0.0f);
}


void ODriveASCII::setVelocity(uint8_t axis, float velocity, float torque_ff){
    // v axis velocity torque_ff
    if(axis != 0 && axis != 1){return;}
    const string message = "v " + std::to_string(axis) + " " +
                               floatToString(velocity) + " " +
                              floatToString(torque_ff) + "\n";
    writeToSerial(message);
}


void ODriveASCII::readParameter(uint8_t axis, const string property, int* value){
    // r property
    if(axis != 0 && axis != 1){return;}
    const string message = "r axis" + std::to_string(axis) + "." +
                                                  property + "\n";
    writeToSerial(message);
    *value = readInt();
}


void ODriveASCII::readParameter(uint8_t axis, const string property, float* value){
    // r property
    if(axis != 0 && axis != 1){return;}
    const string message = "r axis" + std::to_string(axis) + "." +
                                                  property + "\n";
    writeToSerial(message);
    *value = readFloat();
}

void ODriveASCII::readParameter(uint8_t axis, const string property, bool* value){
    // r property
    if(axis != 0 && axis != 1){return;}
    const string message = "r axis" + std::to_string(axis) + "." +
                                                  property + "\n";
    writeToSerial(message);
    *value = readBool();
}


void ODriveASCII::writeParameter(uint8_t axis, const string property, int value){
    // w property value
    if(axis != 0 && axis != 1){return;}
    const string message = "w axis" + std::to_string(axis) + "." +
                                                  property + " " +
                                     std::to_string(value) + "\n";
    writeToSerial(message);
}


void ODriveASCII::writeParameter(uint8_t axis, const string property, float value){
    // w property value
    if(axis != 0 && axis != 1){return;}
    const string message = "w axis" + std::to_string(axis) + "." +
                                                  property + " " +
                                      floatToString(value) + "\n";
    writeToSerial(message);
}

bool ODriveASCII::setState(uint8_t axis, MotorState newState){
    if(axis != 0 && axis != 1){return false;}
    writeParameter(axis, "requested_state", (int)newState);
    wait_us(STATE_WAIT_TIME);
    int acquiredState = 0;
    readParameter(axis, "current_state", &acquiredState);
    return newState == (MotorState)(acquiredState);
}

void ODriveASCII::saveConfig(){
    const string message = "ss\n";
    writeToSerial(message);
}


void ODriveASCII::eraseConfig(){
    const string message = "se\n";
    writeToSerial(message);
}


void ODriveASCII::reboot(){
    const string message = "sr\n";
    writeToSerial(message);
}

void ODriveASCII::clearErrors(){
    writeParameter(0, "controller.input_torque", (float) 0.0);
    writeParameter(1, "controller.input_torque", (float) 0.0);

    const string message = "sc\n";
    writeToSerial(message);
}

bool ODriveASCII::isEncoderIndexFound(int axis) {
  bool encoder_index_found;
  readParameter(axis, "encoder.index_found", &encoder_index_found);
  return encoder_index_found;
}

bool ODriveASCII::isEncoderPreCalibrated(int axis) {
  bool encoder_is_precalibrated;
  readParameter(axis, "encoder.config.pre_calibrated", &encoder_is_precalibrated);
  return encoder_is_precalibrated;
}

bool ODriveASCII::isAbsoluteGPIOPinValid(int axis) {
  int absolute_gpio_pin;
  readParameter(axis, "encoder.config.abs_spi_cs_gpio_pin", &absolute_gpio_pin);
  return (absolute_gpio_pin == 7 || absolute_gpio_pin == 8);
}


// Private functions
void ODriveASCII::writeToSerial(const string message){
    #if defined MBED_MAJOR_VERSION && MBED_MAJOR_VERSION < 6
    while(!oDriveSerialmbed5->writable()){
        wait_us(10);
    }
    oDriveSerialmbed5->write((uint8_t*)message.c_str(), message.length());
    #else
    while(!oDriveSerial->writable()){
        wait_us(10);
    }
    oDriveSerial->write(message.c_str(), message.length());
    #endif
}


int32_t ODriveASCII::readInt(){
    char buffer[LARGE_BUFFER_SIZE] {0};
    int32_t result = 0;
    if(readCharArray(buffer)){
        findInt(buffer, 0, &result);
    }
    return result;
}


float ODriveASCII::readFloat(){
    char buffer[LARGE_BUFFER_SIZE] {0};
    float result = 0;
    if(readCharArray(buffer)){
        findFloat(buffer, 0, &result);
    }
    return result;
}

bool ODriveASCII::readBool(){
    char buffer[LARGE_BUFFER_SIZE] {0};
    if(readCharArray(buffer)){
        if (buffer[0] == '1') {
            return true;
        }
        else if (buffer[0] == '0') {
            return false;
        }
    }

    // What to return if no bool is read
    return false;
}

bool ODriveASCII::readCharArray(char* buffer){
    int iterations = 0;
    int bytes = 0;
    int index = 0;
    while(iterations < 100){
        #if defined MBED_MAJOR_VERSION && MBED_MAJOR_VERSION < 6
        bytes = oDriveSerialmbed5->read((uint8_t*)&buffer[index], LARGE_BUFFER_SIZE-index);
        #else
        bytes = oDriveSerial->read(&buffer[index], LARGE_BUFFER_SIZE-index);
        #endif

        if(bytes > 0){
            index += bytes;
            if(buffer[index-1] == '\n'){
                return true;
            }
        }
        iterations++;
        wait_us(50);
    }
    return false;
}

uint32_t ODriveASCII::findFloat(const char* buffer, uint32_t startPosition, float* result){
    char floatBuffer[MEDIUM_BUFFER_SIZE] = {0};
    uint32_t floatBufferPosition = 0;
    uint32_t currentPosition = startPosition;
    uint32_t size = 0;

    while((buffer[currentPosition] >= '0' && buffer[currentPosition] <= '9') || (buffer[currentPosition] == '-') || (buffer[currentPosition] == '.')){
        floatBuffer[floatBufferPosition] = buffer[currentPosition];
        floatBufferPosition++;
        currentPosition++;
        size++;
    }
     
    *result = (float)atof(floatBuffer);
    return currentPosition; // Respons is whitespace/breakline/carriage return location
}

uint32_t ODriveASCII::findInt(const char* buffer, uint32_t startPosition, int32_t* result){
    char integerBuffer[MEDIUM_BUFFER_SIZE] = {0};
    uint32_t integerBufferPosition = 0;
    uint32_t currentPosition = startPosition;
    uint32_t size = 0;

    while((buffer[currentPosition] >= '0' && buffer[currentPosition] <= '9') || (buffer[currentPosition] == '-')){
        integerBuffer[integerBufferPosition] = buffer[currentPosition];
        integerBufferPosition++;
        currentPosition++;
        size++;
    }
     
    *result = (int32_t)atoi(integerBuffer);
    return currentPosition;
}
