#ifndef MOCK_MBED_HPP
#define MOCK_MBED_HPP

#include <cstdint>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <string>

using std::string;
/**
 * @ingroup test
 * @brief Mock interface for Mbed (Buffered)Serial
 */
class MockSerial {
    public:
        /// Constructor
        MockSerial() = default;
        /// Read from `m_read_buffer`
        int read(char* buffer, int read_length);
        /// Write to `m_read_buffer`
        void write(const char* buffer, int write_length);
        /// Return the `m_readable`
        bool readable();
        /// Return the `m_writable`
        bool writable();
        /// Implemented for a complete implementation, but does nothing
        void set_blocking(bool blocking);

        // These methods are used for testing
        /// Set the `m_readable` state
        void set_readable(bool readable);
        /// Set the `m_writable` state
        void set_writable(bool writable);
        /// Set the `m_read_buffer` contents
        void set_buffer_content(string buffer);

        /// Directly return pointer to `m_read_buffer`
        const string& get_read_buffer();
        /// Directly return pointer to `m_write_buffer`
        const string& get_write_buffer();
    private:
        /// The current readable state of the mock serial
        bool m_readable = true;
        /// The current writable state of the mock serial
        bool m_writable = true;
        /// Read buffer of the mock serial
        string m_read_buffer;
        /// Write buffer of the mock serial
        string m_write_buffer;
};

void wait_us(int us);

#endif
