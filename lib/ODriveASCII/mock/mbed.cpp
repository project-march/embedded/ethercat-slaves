#include <mock/mbed.hpp>
#include <iostream>

int MockSerial::read(char* buffer, int read_length) {
    int bytes_to_be_copied = std::min(read_length, (int)m_read_buffer.size());
    strncpy(buffer, m_read_buffer.c_str(), bytes_to_be_copied);
    m_read_buffer.erase(0, bytes_to_be_copied);
    return bytes_to_be_copied;
}

void MockSerial::write(const char* buffer, int write_length) {
    m_write_buffer = string(buffer, write_length);
}

bool MockSerial::readable() {
    return m_readable;
}
bool MockSerial::writable() {
    return m_writable;
}

void MockSerial::set_blocking(bool blocking) {}

void MockSerial::set_readable(bool readable) {
    m_readable = readable;
}

void MockSerial::set_writable(bool writable) {
    m_writable = writable;
}

void MockSerial::set_buffer_content(string buffer) {
    m_read_buffer = buffer;
    m_read_buffer.reserve(1024);
}

const string& MockSerial::get_read_buffer() {
    return m_read_buffer;
}

const string& MockSerial::get_write_buffer() {
    return m_write_buffer;
}

void wait_us(int us) {};
