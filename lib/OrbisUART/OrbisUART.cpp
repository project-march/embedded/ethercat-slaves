#include "OrbisUART.h"

OrbisUART::OrbisUART(PinName tx, PinName rx, int baud) 
: BufferedSerial(tx, rx, baud), baudrate(baud), write_buffer(), read_buffer(){
            set_blocking(false);
};

uint8_t OrbisUART::ReadBuffer(){
    char message_type;
    volatile ssize_t bytes_read = 1;
    //First check the message type to determine how the message should be interpeted
    bytes_read = read(&message_type, 1);

    //Read the buffer until it is empty
    while(bytes_read > 0){
        
        //The way the messages are received and can be interpeted are given in Datasheets/BRD01_09_EN_data_sheet.pdf
        switch (message_type)
        {
        //position request
        case POS_REQUEST:
            bytes_read = read(this->read_buffer, 2);
            //Filter out first two bits containing status data
            this->abs_position = ((this->read_buffer[1] & 0xFC) >> 2) | (this->read_buffer[0] << 6);
            this->encoder_status = this->read_buffer[1] & 0x03;
            break;
        //position request + detailed status
        case STATUS_REQUEST:
            bytes_read = read(this->read_buffer, 3);
            this->abs_position = ((this->read_buffer[1] & 0xFC) >> 2) | (this->read_buffer[0] << 6);
            this->encoder_status = (this->read_buffer[0] & 0x03) | (this->read_buffer[2] & 0xFC);
            break;
        //position request + temperature
        case TEMP_REQUEST:
            bytes_read = read(this->read_buffer, 4);
            this->abs_position = ((this->read_buffer[1] & 0xFC) >> 2) | (this->read_buffer[0] << 6);
            this->encoder_status = this->read_buffer[0] & 0x03;
            //two bits containing temperature data
            this->temperature = (this->read_buffer[2] << 8) | (this->read_buffer[3]);
            break;
        //If message is unkown flush the buffer
        default:
            sync();
            bytes_read = 0;
            break;
        }

        bytes_read = read(&message_type, 1);
    }

    return 0;
};

ssize_t OrbisUART::PositionRequest(){
    this->write_buffer[0] = POS_REQUEST;
    return this->write(this->write_buffer, 1);
};

uint16_t OrbisUART::GetPosition(){
    //Check if there is something in the buffer
    if(readable()){
        //empty buffer to get latest infromation
        this->ReadBuffer();
    }

    return this->abs_position;
};

uint8_t OrbisUART::StatusRequest(){
    this->write_buffer[0] = STATUS_REQUEST;
    return this->write(this->write_buffer, 1);
};

uint8_t OrbisUART::GetEncoderStatus(){
    //Check if there is something in the buffer
    if(this->readable()){
        //empty buffer to get latest infromation
        this->ReadBuffer();
    }

    return this->encoder_status;
};

uint8_t OrbisUART::TemperatureRequest(){
    this->write_buffer[0] = TEMP_REQUEST;
    return this->write(this->write_buffer, 1);
};

int16_t OrbisUART::GetTemperature(){
    //Check if there is something in the buffer
    if(this->readable()){
        //empty buffer to get latest infromation
        this->ReadBuffer();
    }
    return this->temperature;
};