/**
 * @defgroup Orbis UART library
 * @brief Library for interfacing with RLS ORBIS encoder using UART communication
 * 
 * Library now implements the most basic functionality. Namely all the main messges that contain the position and status.
 * Some advanced features are avaible to increase data rate and get more messages but do not seem necessary at the moment.
 */

#ifndef ORBISUART
#define ORBISUART

#include <mbed.h>

#define MAX_WRITE_BUFFER 5
#define MAX_READ_BUFFER 5

#define UNLOCK_SEQ_B1 0xCD
#define UNLOCK_SEQ_B2 0xEF
#define UNLOCK_SEQ_B3 0x89
#define UNLOCK_SEQ_B4 0xAB

#define SET_POS_OFFSET 0x5A
#define SET_BAUDRATE 0x42
#define CONT_RES_SET 0x54
#define CONT_RES_START 0x53
#define CONT_RES_STOP 0x50
#define SAVE_CONFIG 0x63
#define RESET_CONFIG 0x72

#define POS_REQUEST '1'
#define STATUS_REQUEST 'd'
#define TEMP_REQUEST 't'


class OrbisUART : private BufferedSerial{
    private:

        uint16_t abs_position;
        int baudrate;
        uint8_t encoder_status;
        int16_t temperature;
        char write_buffer[MAX_WRITE_BUFFER];
        uint8_t read_buffer[MAX_READ_BUFFER];

        /**
         * @brief (NOT IMPLEMENTED!) Sends sequence to Orbis to enable programming of configuration
         * 
         */
        void SendUnlockingSequence();

        /**
         * @brief Read entire buffer and store last received messages
         * 
         * @return 0 if bytes have been read; 1 if nothing is read
         */
        uint8_t ReadBuffer();

    public:

        //construcor
        OrbisUART(PinName tx, PinName rx, int baud=MBED_CONF_PLATFORM_DEFAULT_SERIAL_BAUD_RATE);
        //destructor
        ~OrbisUART() = default;

        /**
         * @brief Returns last received position
         * 
         * @return 2 byte position value
         */
        uint16_t GetPosition();

        /**
         * @brief Returns encoder status
         * 
         * @return 1 bytes of information about the encoder status
         */
        uint8_t GetEncoderStatus();

        /**
         * @brief Returns temperature of Orbis. 
         *        Temperature is multiplied by 10 with a tolerance of +-5 degrees
         * 
         * @return 2 bytes of temperature information
         */
        int16_t GetTemperature();

        /**
         * @brief Request the status from the Aksim
         * 
         * @return The number of bytes written, negative error on failure
         */

        uint8_t StatusRequest();

        /**
         * @brief Request the temperature from the Aksim
         * 
         * @return The number of bytes written, negative error on failure
         */

        uint8_t TemperatureRequest();

        /**
         * @brief Request the position from the Aksim
         * 
         * @return The number of bytes written, negative error on failure
         */

        ssize_t PositionRequest();

        /**
         * @brief (NOT IMPLEMENTED) Could be used to shortern position request
         * 
         */
        uint8_t ShortPositionRequest();

        /**
         * @brief (NOT IMPLEMENTED) Could be used to change baudrate of communication 
         * 
         */
        uint8_t ChangeBaudrate(int new_baudrate);

        /**
         * @brief (NOT IMPLEMENTED) Self calibration could improve encoder accuracy
         * 
         */
        uint8_t SelfCalibration();

        /**
         * @brief (NOT IMPLEMENTED) Could be used to get postion data without sending a request
         * 
         */
        uint8_t StartContinuousResponse();

        /**
         * @brief (NOT IMPLEMENTED) Needed to stop continuous response mode
         * 
         */
        uint8_t StopContinuousResponse();

};

#endif //ORBISUART