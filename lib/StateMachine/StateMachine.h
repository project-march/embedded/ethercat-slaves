#ifndef STATEMACHINE_H
#define STATEMACHINE_H

#ifdef PLATFORM_NATIVE
#include "mock/timer.hpp"
#else
#include <mbed.h>
#endif
#include <string>
#include "States.h"

/**
 * @defgroup statemachine StateMachine PDB
 * @brief A state machine which generates correct outputs based on inputs and the current state.
 * 
 * @section state_transitions State Transitions
 * 
 * @dot
 * digraph G {
 *  node [shape=record];
 *  init [label="Init"];
 *  lvon [label="LVOn"];
 *  operational [label="Operational"];
 *  shutdown [label="Shutdown"];
 *  init -> lvon [label="Low voltage net OK"];
 *  lvon -> operational [label="Low voltage net OK and not returned from Operation"];
 *  operational -> shutdown [label="Computer is off and sampled long enough"]
 * }
 * @enddot
 * 
 * The init state is there to make sure the PDB stays on and should therefore never be returned to.
 * So there are no transitions towards the Init state.
 * 
 * From Init to LVOn: The low voltage net should be operating correctly before connecting the devices to it.
 * A timer is started to make sure the current sensors determining the state of the computer had enough time to sample good data.
 * 
 * From LVOn to Operational: The same requirements should be met as the transition from Init to LVOn. 
 * Due to design issues the HV net cannot be turned on after the Computer starts (EtherCAT slaves need to be powered on before starting to the master).
 * Also does the state machine check if the previous state was not Operational, preventing the computer from powering down and directly booting up again.
 * 
 * From Operational to Shutdown: We would have liked to create a long press to turn off function, however, due to the current hardware implementation is this not possible.
 * This transition now occurs when the computer is offline.
 * @{
 */


/// @brief Time interval for blinking On/Off button LED in milliseconds
#define BLINK_TIME_LED 250
/// @brief Time On/Off button needs to be pressed to force shutdown in milliseconds
#define ON_OFF_BUTTON_TIME_PRESSED_LONG 6000
/// @brief Time On/Off button needs to be pressed to turn the exoskeleton on or off in miliseconds
#define ON_OFF_BUTTON_TIME_PRESSED_SHORT 2000
/// @brief Minimal time between LV_on and Shutdown in milliseconds. Needed for current sensor sampling.
#define MINIMAL_LV_ON_TIME 500

/// State Machine class for PDB
class StateMachine {
protected:
  /**
   * @brief Update variables for timers and possible other internal states.
   * Mainly updates the timer to determine a long or short press on the On/Off button.
   * @note This function is not used, since the on/off button turned out to be working differently then expected.
   */
  void updateInternalStates(bool onOffButtonState);

  State currentState = State::Init_s;   ///< The state in which the statemachine is currently active
  Timer onOffButtonTimer;               ///< Timer for the on/off button
  Timer stateTimer;                     ///< Timer to prevent too quick shutdown
  Timer ledTimer;                       ///< Timer for making the LED blink
  // Output variables
  bool onOffButtonLedState = false;     ///< State of the onOffButtonLED
  bool keepPDBOn = false;               ///< Instruction to keep PDB powered
  bool LVon = false;                    ///< Instruction to turn the LV on
  bool HVon = false;                    ///< Status of the requested HV state
  bool computerOn = false;              ///< Keep de computer on

  // Internal variables
  bool onOffButtonPressedLong = false;  ///< Whether button pressed long 
  bool onOffButtonPressedShort = false; ///< Variable if button is pressed for short time
  bool returnedFromOperation = false;   ///< Variable whether transition is returned from Operation

public:
  StateMachine();
  /**
   * @brief Main loop for updating the state based on a few input parameters.
   * @param onOffButtonPressed State of the button is currently pressed (true) or not (false).
   * @param computerOnline Status of the computer, true = on, false = off.
   * @param lowVoltageOK Low voltage net is working correctly and can possible be turned on.
   * 
   * For a more detailed description of the states and transitions, see the Statemachine PDB page.
   */
  void updateState(bool onOffButtonPressed, bool computerOnline, bool lowVoltageOK);

  // Actions based on state
  /**
   * @brief Returns the current state of the state machine in string form.
   * @return Current state as string.
   */
  std::string getState();

  /**
   * @brief Returns the desired state of the LED based on the state machine.
   * @return Current desired LED state, true = on, false = off.
   */
  bool getOnOffButtonLedState();
  
  /**
   * @brief Returns the desired state of the PDB based on the state machine.
   * @return Current desired PDB state, true = on, false = off.
   */
  bool getKeepPDBOn();

  /**
   * @brief Returns the desired state of the LV net based on the state machine.
   * @return Current desired LV net state, true = on, false = off.
   */
  bool getLVOn();

  /**
   * @brief Returns the desired state of the HV nets based on the state machine.
   * @return Current desired HV nets state, true = on, false = off.
   */
  bool getHVOn();

  /**
   * @brief Returns the desired state of the computer based on the state machine.
   * @return Current desired computer state, true = on, false = off.
   */
  bool getComputerOn();
};
/// @}
#endif  // STATEMACHINE_H