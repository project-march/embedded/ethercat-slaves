#ifndef STATES_H
#define STATES_H

/**
 * @addtogroup statemachine
 * # States
 * 
 * The state of the StateMachine determines which outputs can be turned on and off.
 * State transitions determine correct operation and are checked in the forementioned class.
 * 
 * | State name  | State description |
 * |:-----------:|:-----------------:|
 * | Init        | Initial state of the state machine directly after the creation of the class/variable |
 * | LVOn        | Only the low voltage (LV) net is on with EtherCAT slaves |
 * | Operational | Everything needed for full operation is on and active |
 * | Shutdown    | The last state reached, all outputs will be turned off gracefully and the PDB will soon shutdown itself |
 * @{
 */

/// Different possible states of state machine
enum State
{
  Init_s, ///< Initial state directly at boot.
  LVOn_s, ///< In this state only the low voltage nets are turned on.
  Operational_s, ///< The PDB and thereby the exoskeleton is fully operational (everything receives power).
  Shutdown_s ///< Disable all outputs.
};

///@}

#endif  // STATES_H