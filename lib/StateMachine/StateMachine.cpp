#include "StateMachine.h"

// Constructor
StateMachine::StateMachine() {
  this->ledTimer.reset();
  this->onOffButtonTimer.reset();
}

void StateMachine::updateInternalStates(bool onOffButtonState){
  // If button is pressed make sure the timer is and or keeps running
  if(onOffButtonTimer.read_ms() > ON_OFF_BUTTON_TIME_PRESSED_LONG){
    this->onOffButtonPressedLong = true;
  }
  if(onOffButtonState){
    this->onOffButtonTimer.start();
    // Long holding press is also forced shutdown
    if(onOffButtonTimer.read_ms() > ON_OFF_BUTTON_TIME_PRESSED_LONG){
      this->onOffButtonPressedLong = true;
    }
  }else{
    // If the button is released determine the amount of time the button is pressed
    int buttonTimePressed = onOffButtonTimer.read_ms();
    if(buttonTimePressed > ON_OFF_BUTTON_TIME_PRESSED_SHORT && buttonTimePressed < ON_OFF_BUTTON_TIME_PRESSED_LONG){
      this->onOffButtonPressedShort = true;
    }
    // Reset timers to prevent too large timings on next button press
    this->onOffButtonTimer.stop();
    this->onOffButtonTimer.reset();
  }
}

void StateMachine::updateState(bool onOffButtonState, bool computerOnline, bool lowVoltageOK){
  //updateInternalStates(onOffButtonState);
  switch (this->currentState)
  {
    case Init_s:
      // Set values depending on state
      this->onOffButtonLedState = true;
      this->keepPDBOn = true;
      this->LVon = false;
      this->HVon = false;
      this->computerOn = false;

      // Transistion from Init to LVOn
      if(1){
        this->currentState = State::LVOn_s;
        stateTimer.start();
      }

      // Transition from Init to Shutdown
      if(onOffButtonPressedLong || onOffButtonPressedShort){
        this->currentState = State::Shutdown_s;
        this->onOffButtonPressedLong = false;
        this->onOffButtonPressedShort = false;
      }
      break;
    case LVOn_s:
      // Set values depending on state
      this->onOffButtonLedState = true;
      this->keepPDBOn = true;
      this->LVon = true;
      this->HVon = false;
      this->computerOn = false;

      // Transition from LVOn to Operational
      if (lowVoltageOK && !returnedFromOperation){
        this->currentState = State::Operational_s;
      }

      // Transition from LVOn to Shutdown
      if(onOffButtonPressedLong || onOffButtonPressedShort){
        this->currentState = State::Shutdown_s;
        this->onOffButtonPressedLong = false;
        this->onOffButtonPressedShort = false;
      }
      break;
    case Operational_s:
      // Set values depending on state
      this->onOffButtonLedState = true;
      this->keepPDBOn = true;
      this->LVon = true;
      this->HVon = true;
      this->computerOn = true;

      // Transition from Operational to Shutdown
      if(!computerOnline && stateTimer.read_ms() > MINIMAL_LV_ON_TIME){
        this->currentState = State::Shutdown_s;
        this->onOffButtonPressedLong = false;
      }
      if(stateTimer.read_ms() > MINIMAL_LV_ON_TIME){
        stateTimer.stop();
      }
      break;
    case Shutdown_s:
      // Set values depending on state
      this->onOffButtonLedState = false;
      this->keepPDBOn = false;
      this->LVon = false;
      this->HVon = false;
      this->computerOn = false;
  }
}

std::string StateMachine::getState(){
  switch (this->currentState){
    case Init_s:
      return "Init_s";
    case LVOn_s:
      return "LVOn_s";
    case Operational_s:
      return "Operational_s";
    case Shutdown_s:
      return "Shutdown_s";
    default:
      return "Default";
  }
}

bool StateMachine::getOnOffButtonLedState(){
  return this->onOffButtonLedState;
}

bool StateMachine::getKeepPDBOn(){
  return this->keepPDBOn;
}

bool StateMachine::getLVOn(){
  return this->LVon;
}

bool StateMachine::getHVOn(){
  return this->HVon;
}

bool StateMachine::getComputerOn(){
  return this->computerOn;
}
