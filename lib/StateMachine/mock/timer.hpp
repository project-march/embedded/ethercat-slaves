#ifndef MOCK_TIMER_H
#define MOCK_TIMER_H

/**
 * @defgroup mocktimer MockTimer
 * @ingroup test
 *
 * This module is part of the mock versions of regular Mbed implementations.
 * 
 * The timer is far from perfect and not feature compatible with Mbed 6 and up.
 *
 * For now it is only used in the TestStateMachine.
 * @{
 */

/// Mock implementation of the Mbed timer
class Timer {
    public:
        /// Empty constructor
        Timer();
        /// Sets the internal variables asif the timer is started
        void start();
        /// Will return true if timer was running at the time of calling stop
        bool stop();
        /// Resets the millisecond count
        void reset();
        /// Sets the amount of milliseconds the timer has elapsed; Mock timer specific.
        void set_ms(int ms);
        /// Return the amount of "elapsed" milliseconds
        int read_ms();
        /// Returns whether the Timer has ever been started.
        bool did_start();
    private:
        int milliSeconds = 0;
        bool isStarted = false;
        bool isRunning = false;
};
/// @} mocktimer
#endif // MOCK_TIMER_H