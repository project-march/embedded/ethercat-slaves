#include "timer.hpp"

Timer::Timer(){}

void Timer::start(){
    this->isStarted = true;
    this->isRunning = true;
}

bool Timer::stop(){
    if(this->isRunning){
        this->isRunning = false;
        return true;
    }else{
        return false;
    }
}

void Timer::reset(){
    this->milliSeconds = 0;
}

void Timer::set_ms(int ms){
    this->milliSeconds = ms;
}

int Timer::read_ms(){
    return this->milliSeconds;
}

bool Timer::did_start(){
    return this->isStarted;
}