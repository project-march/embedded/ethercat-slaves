/* * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * * * *\
 *  Library implementing the software interface for the LAN9252 EtherCAT chip.          *
 *  Hardware consists of a 64-pin chip.                                                 *
 *      Connection is done in the hardware of the chip.                                 *
 *  Requires a device to be specified, assumes standard connections for the chip for    *
 *      that device.                                                                    *
 *                                                                                      *
 *  Created by P. Verton for the MARCH 3 team of Project MARCH                          *
 *  Adapted by M. van der Marel for the MARCH 4 team of Project MARCH                   *
 *  Date: 04-APR-2018                                                                   *
\* * * * * * * * * * * * * * * * * * * * * *  * * * * * * * * * * * * * * * * * * * * * */

#ifndef ETHERCAT_
#define ETHERCAT_

#include <mbed.h>
#include "Ethercat_defines.h"
#include "utypes.h"

/**
 * @defgroup ethercat EtherCAT library
 * @brief EtherCAT library for interfacing with the LAN9252 IC in SPI mode
 * 
 * This library is a combination a partial implementation of the EtherCAT slave stack
 * and an interface for the LAN9252 IC. The LAN9252 IC can only be communicated with,
 * if the IC boots in SPI slave mode (not HBI or standalone mode).
 * 
 * The EtherCAT stack implementation only has the bare necessity to operate in CAN over EtherCAT mode (CoE).
 * It creates an RxPDO and TxPDO in `utypes.h` (PDO), which are send and received every cycle (cycle time is determined by master).
 * The library has a small addition to set a timout interrupt, 
 * the interrupt needs to be handled by the slave itself (not in this library).
 * 
 * Unfortunatly some key features are still missing from the library such as:
 *  - SDO (commands)
 *  - Smaller PDO objects (now only 4 byte types are supported)
 *  - Dynamic PDO
 *  - State machine
 *  - Sync Manager (SM) synchronization
 *  - Many more...
 * @{
 */

#ifdef DEBUG_DEC
#undef DEBUG_DEC
#endif
#if MBED_MAJOR_VERSION >= 6
/// Interface for sending debug information to a serial itnerface (e.g. computer)
#define DEBUG_DEC int debugLevel = 0
#else
/// Interface for sending debug information to a serial itnerface for older Mbed versions (e.g. computer)
#define DEBUG_DEC Serial *pc = NULL, int debugLevel = 0
#endif

/// Union to convert four bytes to a 32 bit integer
typedef union _UINT
{
  uint32_t Int;
  int8_t Byte[4];
} UINT;
/// Union to convert two bytes to a 16 bit integer
typedef union _UWORD
{
  uint16_t Word;
  uint8_t Byte[2];
} UWORD;

/// Buffer for TxPDO messages
typedef union _bufferMiso
{
  _Rbuffer Struct;
  char Byte[MAX_PDO_SIZE];
} bufferMiso;

/// Buffer for RxPDO messages
typedef union _bufferMosi
{
  _Wbuffer Struct;
  char Byte[MAX_PDO_SIZE];
} bufferMosi;

/// @brief EtherCAT & LAN9252 interface
class Ethercat
{
public:
  /// Constructor
  Ethercat(PinName mosi, PinName miso, PinName sclk, PinName nChipSelect = NC, int PDORXsize = DEFAULT_PDO_SIZE,
           int PDOTXsize = DEFAULT_PDO_SIZE, DEBUG_DEC);

  // Public member functions
  /// Updates all input variables and sends out all output variables to the EtherCAT device.
  void update(DEBUG_DEC);

  /// Clears the global MOSI buffer to all zeros.
  void clear_mosi_buffer(DEBUG_DEC);

  // Public class members
  /// CoE transmit buffer
  static bufferMiso pdoTx;
  /// CoE receive buffer
  static bufferMosi pdoRx;

  /**
   * @brief Sets the correct registers to create interrupt when watchdog expires
   * @note Make sure to enable the watchdog on the correct Syncmanager
   * This function enables the interrupt on the hardware pin.
   * The pin is configured as default low and high on interrupt in push-pull configuration
   * Also does it set the ECAT interrupt as possible hardware interrupt
   * Then in the AL register, the watchdog is set as cascading ECAT interrupt
   * Last is the watchdog configured to set an interrupt at 50ms of being disconnected
   * The Syncmanager is not set, this must be done by setting bit 6 in the control register of the SM
   */
  void set_watchdog(DEBUG_DEC);

private:
  // Private member functions
  /** 
   * @brief Initializes the EtherCAT device and all variables.
   * @return A status code indicating how well it went.
   * @return `0`: indicates successful initialisation.
   * @return `-1`: indicates it couldn't read a test register.
   * @return `-2`: indicates a timeout when waiting for the ready flag.
   */ 
  int init(DEBUG_DEC);

  /**
   * @brief Writes to the specified register.
   * @param address Address of register to write to
   * @param data Data to write to the specified register
   */ 
  void write_register(uint16_t address, uint32_t data, DEBUG_DEC);

  /**
   * @brief Reads from the specified register.
   * 
   * @param address Address of register to read from
   * @return Data pointer to read into
   */
  uint32_t read_register(uint16_t address, DEBUG_DEC);

  /**
   * @brief Writes to the specified register indirectly.
   * @param address Address of register to write to
   * @param data Data to write to the specified register
   */

  void write_register_indirect(uint16_t address, uint32_t data, DEBUG_DEC);

  /**
   * @brief Reads from the specified register.
   * 
   * @param address Address of register to read from
   * @return Data pointer to read into
   */   
  uint32_t read_register_indirect(uint16_t address, DEBUG_DEC);

  /// Reads the EtherCAT process ram into the global MOSI buffer
  void read_process_ram(DEBUG_DEC);

  /// Writes the global MISO buffer into the EtherCAT process ram
  void write_process_ram(DEBUG_DEC);

  // Private class members
  /// SPI interface for LAN9252 IC
  SPI* m_chip;
  // Pin for enabling/disbaling connection to/from LAN9252
  DigitalOut* m_nChipSelect;
  int status;
  /// Size of the receive PDO
  int PDORX_size;
  /// Size of the transmit PDO
  int PDOTX_size;
};
/// @}
#endif  // ETHERCAT_