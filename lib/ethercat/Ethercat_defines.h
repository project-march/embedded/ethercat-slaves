#ifndef ETHERCAT_DEFINES_
#define ETHERCAT_DEFINES_

/**
 * @addtogroup ethercat
 * @{
 */
/// Macro to create a bit a specific position
#define BIT(x) (1 << (x))

#define MAX_PDO_SIZE 0x80      ///< Max size of a SyncManager
#define DEFAULT_PDO_SIZE 0x20  ///< Default size
//---- LAN9252 registers --------------------------------------------------------------------------

//---- access to EtherCAT registers -------------------

#define ECAT_CSR_DATA 0x0300  ///< EtherCAT CSR Interface Data Register
#define ECAT_CSR_CMD 0x0304   ///< EtherCAT CSR Interface Command Register

//---- access to EtherCAT process RAM -----------------

#define ECAT_PRAM_RD_ADDR_LEN 0x0308  ///< EtherCAT Process RAM Read Address and Length Register
#define ECAT_PRAM_RD_CMD 0x030C       ///< EtherCAT Process RAM Read Command Register
#define ECAT_PRAM_WR_ADDR_LEN 0x0310  ///< EtherCAT Process RAM Write Address and Length Register
#define ECAT_PRAM_WR_CMD 0x0314       ///< EtherCAT Process RAM Write Command Register

#define ECAT_PRAM_RD_FIFO 0x0000  ///< EtherCAT Process RAM Read Data FIFO
#define ECAT_PRAM_WR_FIFO 0x0020  ///< EtherCAT Process RAM Write Data FIFO

#define SM2_ADDR 0x1100  ///< SyncManager 2 Address (MOSI)
#define SM3_ADDR 0x1180  ///< SyncManager 3 Address (MISO)

//---- EtherCAT registers -----------------------------

#define AL_STATUS 0x0130          ///< AL status
#define WDOG_STATUS 0x0440        ///< watch dog status
#define WDOG_TPD 0x0420           ///< watchdog time process data
#define WDOG_PDI 0x0410           ///< watchdog time PDI

//---- LAN9252 registers ------------------------------

#define HW_CFG 0x0074             ///< hardware configuration register
#define BYTE_TEST 0x0064          ///< byte order test register
#define BYTE_TEST_RES 0x87654321  ///< byte order test expected result
#define RESET_CTL 0x01F8          ///< reset register
#define ID_REV 0x0050             ///< chip ID and revision
#define AL_EVENT_MASK 0x0204      ///< mask bits for enabling AL Event Interrupts
#define INT_EN 0x005C             ///< mask bits for interrupt pin
#define INT_CFG 0x0054            ///< interrupt configuration

//---- LAN9252 flags ------------------------------------------------------------------------------

#define ECAT_CSR_BUSY 0x80        ///< EtherCAT busy flag
#define PRAM_READ_BUSY BIT(31)    ///< Busy bit during read operations
#define PRAM_READ_AVAIL BIT(0)    ///< Available bit during read operations
#define PRAM_WRITE_BUSY BIT(31)   ///< Busy bit during write operations
#define PRAM_WRITE_AVAIL BIT(0)   ///< Available bit during write operations
#define READY 0x08000000          ///< Ready for operation

#define DIGITAL_RST 0x00000001    ///< Value to initiate a digital reset
#define ETHERCAT_RST 0x00000040   ///< Value to initiate a EtherCAT stack reset

//---- EtherCAT flags -----------------------------------------------------------------------------

// EtherCAT state machine

#define ESM_INIT 0x01             ///< init state
#define ESM_PREOP 0x02            ///< pre-operational state
#define ESM_BOOT 0x03             ///< bootstrap state
#define ESM_SAFEOP 0x04           ///< safe-operational state
#define ESM_OP 0x08               ///< operational state

//--- ESC commands --------------------------------------------------------------------------------

#define ESC_WRITE 0x80            ///< Write command for ESC registers
#define ESC_READ 0xC0             ///< Read command for ESC registers

//---- SPI ----------------------------------------------------------------------------------------

#define COMM_SPI_READ 0x03        ///< Read command for SPI interface
#define COMM_SPI_WRITE 0x02       ///< Write command for SPI interface
#define COMM_SPI_FASTREAD 0x0B    ///< Fast read command for SPI interface

#define DUMMY_BYTE 0xFF           ///< Byte for empty read commands SPI

///@}
#endif  // ETHERCAT_DEFINES_