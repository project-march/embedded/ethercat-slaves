/**
 * @defgroup tinybms tinyBMS
 * @brief UART communication with the tinyBMS battery management system
 * @authors R. Friendwijk, M. Haarman, J. Bouman
 * 
 * tinyBMS type: s516 - 150A/750A
 * 
 * tinyBMS version: 2.1
 * 
 * tinyBMS datasheet: https://www.energusps.com/web/binary/saveas?filename_field=datas_fname&field=datas&model=ir.attachment&id=21070
 * 
 * tinyBMS communication protocols: https://www.energusps.com/web/binary/saveas?filename_field=datas_fname&field=datas&model=ir.attachment&id=21208
 * 
 * tinyBMS user manual: https://www.energusps.com/web/binary/saveas?filename_field=datas_fname&field=datas&model=ir.attachment&id=21072
 * 
 * tinyBMS quick start-guide: https://www.energusps.com/web/binary/saveas?filename_field=datas_fname&field=datas&model=ir.attachment&id=21071
 * 
 * # Example code
 * @code
 * static BufferedSerial BMS_bufferedSerial(LPC_TX, LPC_RX); //set RX/TX ports for pins to tinyBMS communication cable port
 * tinyBMS_serial tinyBMS(&BMS_bufferedSerial); //create tinyBMS class with pointer to BMS_bufferedSerial class
 * float voltage = 0.0;
 * float current = 0.0;
 * float estSOC = 0.0;
 * float intTemp = 0.0;
 * float extTemp1 = 0.0;
 * float extTemp2 = 0.0;
 * string status = "";
 * while(true) {
 *     if(tinyBMS.readData()) {
 *         voltage = tinyBMS.tinyBMS_voltage();
 *         current = tinyBMS.current();
 *         estSOC = tinyBMS.estSOC();
 *         intTemp = tinyBMS.intTemp();
 *         extTemp1 = tinyBMS.extTemp1();
 *         extTemp2 = tinyBMS.extTemp2();
 *         status = tinyBMS.status();
 *     }else{
 *         printf("BMS not connected\n");
 *     }
 *     //thread_sleep_for(500); //delay for debug purposes, please comment when uploading
 * }
 * @endcode
 * 
 * @{
 */

#include <mbed.h>

#define tinyBMS_baudrate 115200 ///< baudrate value for tinyBMS uart communication (see communication protocol sheet)
#define tinyBMS_wordSize 8 ///< wordSizevalue for tinyBMS uart communication (see communication protocol sheet)
#define tinyBMS_stopBit 1 ///< stopbit value for tinyBMS uart communication (see communication protocol sheet)
#define tinyBMS_parityBit BufferedSerial::None ///< parityBit value for tinyBMS uart communication (see communication protocol sheet)
#define tinyBMS_receiveDataIterationLimit 10 ///< ammount of attempts to try and obtain the response for each given request
#define tinyBMS_byte1 0xAA ///< each request and response will start with this byte
#define tinyBMS_estSOCDivider 1000000 ///< divide the by the BMS returned state of charge by this number to get a correct percentage value
#define tinyBMS_intTempDivider 10 ///< divide the by the MBS returned temperature by this number to get a correct temperature value
#define tinyBMS_iterationWaitTime_us 10 ///< Time between retries in microseconds 

//settings for blocking tinyBMS output (broadcast time to disabled) (register 342, see communication protocol, set to zero)
#define tinyBMS_broadcast_register_342_LSB 0x56 ///< Least significant byte of the broadcast setting addres
#define tinyBMS_broadcast_register_342_MSB 0x01 ///< Most significant byte of the broadcast setting addres
#define tinyBMS_broadcast_register_342_PL 0b00000100 ///< Value to be stored in broadcast register to disable broadcasting


//the bytes for requesting specific data from the tinyBMS
#define tinyBMS_write_single_register_commandByte 0x0D ///< Command byte for single register setting
#define tinyBMS_commandByte_voltage 0x14 ///< Command byte for voltage
#define tinyBMS_commandByte_current 0x15 ///< Command byte for current
#define tinyBMS_commandByte_estSOC 0x1A ///< Command byte for estimated state of charge
#define tinyBMS_commandByte_temp 0x1B ///< Command byte for temperature sensor
#define tinyBMS_commandByte_status 0x18 ///< Command byte for BMS status


//response length for specific data from the tinyBMS according to the datasheet [bytes]
#define tinyBMS_response_length_voltage 8 ///< Response length of the voltage message
#define tinyBMS_response_length_current 8 ///< Response length of the current message
#define tinyBMS_response_length_estSOC 8 ///< Response length of the estimated state of charge message
#define tinyBMS_response_length_temp 11 ///< Response length of the temperature message
#define tinyBMS_response_length_status 6 ///< Response length of the status message

//the bytes in a status response. each byte indicates a different BMS status
#define tinyBMS_statusByte_charging 0x91 ///< Charging state
#define tinyBMS_statusByte_charged 0x92 ///< Fully charged state
#define tinyBMS_statusByte_discharging 0x93 ///< Discharging state
#define tinyBMS_statusByte_regeneration 0x96 ///< Regeneration state
#define tinyBMS_statusByte_idle 0x97 ///< Idle state
#define tinyBMS_statusByte_error 0x9B ///< Error state


/// @brief States in which the tinyBMS can be.
enum class tinyBMS_state {charging, charged, discharging, regeneration, idle, bms_error};

/// @brief Class for interfacing with the tinyBMS
class tinyBMS_serial {
    private:
        int tinyBMS_nBytes; //nr. of bytes returned by the response

        //The following variables will contain the values returned by the BMS
        float batteryPackVoltage; //battery pack voltage
        float batteryLoadCurrent; //battery load current
        float batteryEstSOC; //battery's estimated state of charge
        float batteryTemp[3] = {0.0}; //onboard temperature
        tinyBMS_state batteryStatus;

        bool debug; //true -> debug mode is on; false -> debug mode is off.
        BufferedSerial* tinyBMS; //the tinyBMS serial variable.
        
        uint8_t tinyBMS_rx_buffer[16]; //buffer the tinyBMS read uses to place the response from the BMS into. 
        void tinyBMS_sendRequest(uint8_t commandByte); //sends the request belonging to the commandByte to the BMS. Correct commandbyte in communications protocol sheet. 
        bool tinyBMS_receiveResponse(uint8_t commandByte, int bufferSize); //function that reads data. returns true if requested data is obtained within iterations limit, false if not
        void tinyBMS_set_zero_broadcast();

        bool tinyBMS_readVoltage(); //reads cell package voltage, returns true if data, false if not.
        bool tinyBMS_readCurrent(); //reads battery load current, returns true if data, false if not.
        bool tinyBMS_readEstSOC(); //reads estimated State Of Charge, returns true if data, false if not.
        bool tinyBMS_readTemp(); //reads BMS and sensor temperatures, returns true if data, false if not.
        bool tinyBMS_readStatus(); //reads BMS status, returns true if data, false if not.

        //the tinyBMS uses a CRC (Cyclic Redundancy Check) to verify that data has been send without errors. The checksum is based on a standard MODBUS CRC
        //This is partly explained in the tinyBMS communications protocol sheet par 1.2. 
        //More info on MODBUS (16): https://en.wikipedia.org/wiki/Modbus 
        uint16_t tinyBMS_CRC16 (const uint8_t* data, uint16_t length); //function to calculate a CRC provided by tinyBMS.
        uint16_t tinyBMS_crcTable[256] = { //table with correct Cyclic Redundancy Check values obtained from the tinyBMS communications protocol sheet.
            0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
            0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
            0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
            0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
            0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
            0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
            0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
            0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
            0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
            0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
            0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
            0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
            0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
            0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
            0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
            0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
            0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
            0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
            0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
            0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
            0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
            0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
            0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
            0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
            0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
            0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
            0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
            0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
            0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
            0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
            0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
            0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
        };
    
        

    public:
        /**
         * @brief Construct a new tinyBMS serial object
         * 
         * @param tinyBMS Serial interface on which the BMS is connected
         * @param blocking Whether the connection will block waiting for a response
         */
        tinyBMS_serial(BufferedSerial* tinyBMS, bool blocking = false);

        /**
         * @brief Ability to set the BMS communication style after constructor is called
         * 
         * @param blocking Whether the conenctio will block waiting for a response
         */
        void tinyBMS_set(bool blocking = false);
        
        /**
         * @brief Retrieves all data from the BMS
         * 
         * This function will request all the information which can be retrieved from this class.
         * In order to function properly, this function needs to be called regularly (a fixed interval is not mandatory),
         * such that the information inside the class is updated to the most recent.
         * 
         * @return `true` if any of the data requested is updated.
         * @return `false` if none of the data requested is updated.
         */
        bool tinyBMS_readData();

        /**
         * @brief Checks if the tinyBMS is connected.
         * 
         * This check is implemented by running the `readData()` function.
         * If the tinyBMS is connected, all updates are also updated.
         * 
         * @return `true` if tinyBMS data is updated/connected
         * @return `false` if tinyBMS gives no response to multiple requests.
         */
        bool tinyBMS_connected();

        /**
         * @brief Gives the last obtained voltage.
         * 
         * @return A float with the latest voltage.
         */
        float tinyBMS_voltage();

        /**
         * @brief Gives the last obtained current value.
         * 
         * @return Current value in float type.
         */
        float tinyBMS_current();

        /**
         * @brief Gives the last obrained estimated State of Charge value
         * 
         * @return Percentage of the current state of charge (battery percentage)
         */
        float tinyBMS_estSOC();

        /**
         * @brief Gives the temperature of the BMS board itself
         * 
         * @return Temperature of the BMS own temperature in degrees Celcius.
         */
        float tinyBMS_intTemp();

        /**
         * @brief Returns the temperature of the first sensor inside the battery package.
         * 
         * @return Temperature of the first sensor in degrees Celcius
         */
        float tinyBMS_extTemp1(); 

        /**
         * @brief Gives the temperature of the second sensor inside the battery package.
         * 
         * @return Temperature of the second sensor in degrees Celcius.
         */
        float tinyBMS_extTemp2();

        /**
         * @brief Gives the last obtained battery status.
         * 
         * @return Battery status in `tinyBMS_state` type.
         */
        tinyBMS_state tinyBMS_status();

        /**
         * @brief Turns debus statements on (will utilise the printf function)
         * 
         * @param debug Whether debug statements need to be printed (true = on)
         */
        void set_debug(bool debug = true);
};

///@}
