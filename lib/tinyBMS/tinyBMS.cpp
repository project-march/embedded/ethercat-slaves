/*
UART communication with the tinyBMS battery management system
tinyBMS type: s516 - 150A/750A
tinyBMS version: 2.1
tinyBMS datasheet: https://www.energusps.com/web/binary/saveas?filename_field=datas_fname&field=datas&model=ir.attachment&id=21070
tinyBMS communication protocols: https://www.energusps.com/web/binary/saveas?filename_field=datas_fname&field=datas&model=ir.attachment&id=21208
tinyBMS user manual: https://www.energusps.com/web/binary/saveas?filename_field=datas_fname&field=datas&model=ir.attachment&id=21072
tinyBMS quick start-guide: https://www.energusps.com/web/binary/saveas?filename_field=datas_fname&field=datas&model=ir.attachment&id=21071

Author: R. Friendwijk & M. Haarman
Date: 20-04-2021
Team: MVI
*/

#include <mbed.h>
#include "tinyBMS.h"


tinyBMS_serial::tinyBMS_serial(BufferedSerial* tinyBMS, bool blocking) {
  this->tinyBMS = tinyBMS;
  this->tinyBMS_set(blocking);
  this->debug = false;
  this->batteryPackVoltage = 0.0;
  this->batteryLoadCurrent = 0.0;
  this->batteryEstSOC = 0.0;
  this->batteryStatus = tinyBMS_state::idle;
}

void tinyBMS_serial::tinyBMS_set(bool blocking) {
  //Setup main BMS connection
  this->tinyBMS->set_baud(tinyBMS_baudrate);
  this->tinyBMS->set_format(
    tinyBMS_wordSize, //word size
    tinyBMS_parityBit, //parity bit
    tinyBMS_stopBit //stop bit
  );
  this->tinyBMS->set_blocking(blocking); //blocking true sets each read to wait until data is received. false does not wait for the correct response. 
  this->tinyBMS_set_zero_broadcast();
}

void tinyBMS_serial::tinyBMS_set_zero_broadcast() {
  uint8_t message[9];

  message[0] = tinyBMS_byte1;
  message[1] = tinyBMS_write_single_register_commandByte;
  message[2] = tinyBMS_broadcast_register_342_PL;
  message[3] = tinyBMS_broadcast_register_342_LSB;
  message[4] = tinyBMS_broadcast_register_342_MSB;
  message[5] = 0x00;
  message[6] = 0x00;
 
  uint16_t cWord = this->tinyBMS_CRC16(message, 7);
  message[7] = cWord & 0xFF;
  message[8] = (cWord >> 8) & 0xFF;

  this->tinyBMS->write(message, 9);
}

void tinyBMS_serial::tinyBMS_sendRequest(uint8_t commandByte) {
  uint8_t command[2] = {tinyBMS_byte1, commandByte}; //request command for the pack voltage
  uint16_t cWord = this->tinyBMS_CRC16(command, sizeof(command)); //calculate CRC
  uint8_t dataFrame[4] = {command[0], command[1], 0 , 0}; //setup dataframe
  dataFrame[2] = cWord & 0xFF;
  dataFrame[3] = cWord >> 8;
  this->tinyBMS->write(&dataFrame, sizeof(dataFrame)); //send dataframe
}

bool tinyBMS_serial::tinyBMS_receiveResponse(uint8_t commandByte, int bufferSize) {
  int tryIteration = 0;
  while((this->tinyBMS_nBytes = this->tinyBMS->read(&this->tinyBMS_rx_buffer, bufferSize)) && tryIteration < tinyBMS_receiveDataIterationLimit) {
    tryIteration++;
    if(this->tinyBMS_rx_buffer[0] == tinyBMS_byte1 && this->tinyBMS_rx_buffer[1] == commandByte) { //verify that the buffer contains the desired info
      return  true; //there is data, hence tinyBMS_receiveResponse should return true
      break;
    }
  }
  return false; //maxamount of iterations has been exceeded, no data found hence tinyBMS_receiveResponse should return false. 
}

bool tinyBMS_serial::tinyBMS_readVoltage() {
  //send request
  this->tinyBMS_sendRequest(tinyBMS_commandByte_voltage);
 
  //receive response
  if(this->tinyBMS_receiveResponse(tinyBMS_commandByte_voltage, tinyBMS_response_length_voltage)) {
    uint32_t hex = this->tinyBMS_rx_buffer[2] | (this->tinyBMS_rx_buffer[3] << 8) | (this->tinyBMS_rx_buffer[4] << 16) | (this->tinyBMS_rx_buffer[5] << 24);
    this->batteryPackVoltage = *((float*)&hex); //this has room for optimisation in future iterations. pointer construction seems off but fine for now.
    if(this->debug) {printf("voltage = %f\n", this->batteryPackVoltage);}
    return true;
  }
  return false;
}    

bool tinyBMS_serial::tinyBMS_readCurrent() {
  //send request
  this->tinyBMS_sendRequest(tinyBMS_commandByte_current);
 
  //receive response
  if(this->tinyBMS_receiveResponse(tinyBMS_commandByte_current, tinyBMS_response_length_current)) {
    uint32_t hex = this->tinyBMS_rx_buffer[2] | (this->tinyBMS_rx_buffer[3] << 8) | (this->tinyBMS_rx_buffer[4] << 16) | (this->tinyBMS_rx_buffer[5] << 24);
    this->batteryLoadCurrent = -1 * (*((float*)&hex));//this has room for optimisation in future iterations. pointer construction seems off but fine for now.
    if(this->debug) {printf("current = %f\n", this->batteryLoadCurrent);}
    return true;
  }
  return false;

}

bool tinyBMS_serial::tinyBMS_readEstSOC(){
  //send request
  this->tinyBMS_sendRequest(tinyBMS_commandByte_estSOC);
 
  //receive response
  if(this->tinyBMS_receiveResponse(tinyBMS_commandByte_estSOC, tinyBMS_response_length_estSOC)) {
    uint32_t hex = this->tinyBMS_rx_buffer[2] | (this->tinyBMS_rx_buffer[3] << 8) | (this->tinyBMS_rx_buffer[4] << 16) | (this->tinyBMS_rx_buffer[5] << 24);
    this->batteryEstSOC = ((float) hex) / tinyBMS_estSOCDivider;
    if(this->debug) {printf("est SOC = %f\n", this->batteryEstSOC);}
    return true;
  }
  return false;
}

bool tinyBMS_serial::tinyBMS_readTemp(){
  //send request
  this->tinyBMS_sendRequest(tinyBMS_commandByte_temp);
  //receive response
  if(this->tinyBMS_receiveResponse(tinyBMS_commandByte_temp, tinyBMS_response_length_temp)) {
    //BMS Onboard temperature sensor
    uint16_t hex = this->tinyBMS_rx_buffer[3] | (this->tinyBMS_rx_buffer[4] << 8);
    this->batteryTemp[0] = ((float)hex) / tinyBMS_intTempDivider;
    //battery cell temperature sensor 1
    hex = this->tinyBMS_rx_buffer[5] | (this->tinyBMS_rx_buffer[6] << 8);
    this->batteryTemp[1] = ((float)hex) / tinyBMS_intTempDivider;
    //battery cell temperature sensor 2
    hex = this->tinyBMS_rx_buffer[7] | (this->tinyBMS_rx_buffer[8] << 8);
    this->batteryTemp[2] = ((float)hex) / tinyBMS_intTempDivider;

    if(debug) {
      printf("int Temp = %f\n", this->batteryTemp[0]);
      printf("ext Temp 1 = %f\n", this->batteryTemp[1]);
      printf("ext Temp 2 = %f\n", this->batteryTemp[2]);
    }
    return true;
  }

 
  return false;
}

bool tinyBMS_serial::tinyBMS_readStatus(){
  //send
  this->tinyBMS_sendRequest(tinyBMS_commandByte_status);
 
  //receive response
  if(this->tinyBMS_receiveResponse(tinyBMS_commandByte_status, tinyBMS_response_length_status)) {
    uint16_t status = this->tinyBMS_rx_buffer[2] | this->tinyBMS_rx_buffer[3] << 8;
    switch(status){
      case tinyBMS_statusByte_charging: 
        this->batteryStatus = tinyBMS_state::charging;
        if(debug) {printf("Status: Charging\n");}
      break;
      case tinyBMS_statusByte_charged: 
        this->batteryStatus = tinyBMS_state::charged;
        if(debug) {printf("Status: Fully charged\n");}
      break;
      case tinyBMS_statusByte_discharging: 
        this->batteryStatus = tinyBMS_state::discharging;
        if(debug) {printf("Status: Discharging\n");}
      break;
      case tinyBMS_statusByte_regeneration: 
        this->batteryStatus = tinyBMS_state::regeneration;
        if(debug) {printf("Status: Regenerating\n");}
      break;
      case tinyBMS_statusByte_idle: 
        this->batteryStatus = tinyBMS_state::idle;
        if(debug) {printf("Status: Idle\n");}
      break;
      case tinyBMS_statusByte_error: 
        this->batteryStatus = tinyBMS_state::bms_error;
        if(debug) {printf("Status: error\n");}
      break;
    }
    
    return true;
  }
  return false;
}

bool tinyBMS_serial::tinyBMS_readData() {
  return this->tinyBMS_readVoltage() | 
  this->tinyBMS_readCurrent() | 
  this->tinyBMS_readEstSOC() | 
  this->tinyBMS_readTemp() | 
  this->tinyBMS_readStatus();
}

bool tinyBMS_serial::tinyBMS_connected() {
  return this->tinyBMS_readData();
}

float tinyBMS_serial::tinyBMS_voltage() {
  return this->batteryPackVoltage;
}

float tinyBMS_serial::tinyBMS_current() {
  return this->batteryLoadCurrent;
}

float tinyBMS_serial::tinyBMS_estSOC() {
  return this->batteryEstSOC;
}

float tinyBMS_serial::tinyBMS_intTemp() {
  return this->batteryTemp[0];
}

float tinyBMS_serial::tinyBMS_extTemp1() {
  return this->batteryTemp[1];
}

float tinyBMS_serial::tinyBMS_extTemp2() {
  return this->batteryTemp[2];
}

tinyBMS_state tinyBMS_serial::tinyBMS_status() {
  return this->batteryStatus;
}


uint16_t tinyBMS_serial::tinyBMS_CRC16(const uint8_t* data, uint16_t length) {//function is copied from the tinyBMS communications protocol sheet. 
  uint8_t tmp;
  uint16_t crcWord = 0xFFFF;
  while (length--){
      tmp = *data++ ^ crcWord;
      crcWord >>= 8;
      crcWord ^= this->tinyBMS_crcTable[tmp];
  }
  return crcWord;
        
}

void tinyBMS_serial::set_debug(bool debug) {
  this->debug = debug;
}

