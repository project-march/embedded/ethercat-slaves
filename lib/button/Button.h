#ifndef BUTTON_H
#define BUTTON_H

#include <mbed.h>

/// Abstraction layer to debounce a button based on a single pin input.
class Button
{
private:
  Timer debounceTimer;
  DigitalIn input;
  uint64_t debounceTime;

public:
  /// Constructor with debouncetime and pin
  Button(PinName pin, PinMode mode, uint64_t debounceTime);
  /// Read With a debouncing, depending on the debounceTime given to the constructor
  bool debounceRead(bool initialbutton);
  /// Read the input as directly by Mbed
  bool read();
};

#endif  // BUTTON_H