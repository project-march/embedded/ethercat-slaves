#include "PCA9534.h"

// Constructor
PCA9534::PCA9534(PinName SDA_PIN, PinName SCL_PIN) : bus(SDA_PIN, SCL_PIN)
{
  this->PCA9534_read = (PCA9534_address << 1) | 0x01;   // Shift left and set LSB to one
  this->PCA9534_write = (PCA9534_address << 1) & 0xFE;  // Shift left and set LSB to zero

  // Write the correct settings
  // Set configuration settings
  uint16_t configuration = (configuration_setting << 8) | CONFIGURATION_REGISTER;
  bus.write(PCA9534_write, (char*)&configuration, 2);

  // Set polarity setting
  uint16_t polarity = (polarity_setting << 8) |  POLARITY_REGISTER; 
  bus.write(PCA9534_write, (char*)&polarity, 2);

  // Write initial values
  this->turnOffAll();
}

// This helper function sets a bit with given index in the HVControlPins
void PCA9534::setBit(uint8_t index)
{
  this->HVControlPins |= (1 << index);
}

// This helper function clears a bit with given index in the HVControlPins
void PCA9534::clearBit(uint8_t index)
{
  this->HVControlPins &= (~(1 << index));
}

// This helper function gets and returns a bit with given index from a given word
bool PCA9534::getBit(uint8_t word, uint8_t index)
{
  return ((word >> index) & 0x01);
}

// This function writes the two bytes in HVControlPins to the PCA8575
bool PCA9534::write()
{
  uint16_t data = (HVControlPins << 8) | OUTPUT_REGISTER;
  int written = bus.write(this->PCA9534_write, (char*)&data, 2);
  return written == 0; // 0 on succes (ack), nonzero on failure (nack)
}

// This function reads the current status of the output pins on the PCA9534
uint8_t PCA9534::read()
{
  uint8_t read_register = OUTPUT_REGISTER;
  bus.write(PCA9534_read, (char*)&read_register, 1);
  uint8_t read_data = 0;
  bus.read(this->PCA9534_read, (char*)&read_data, 1);
  return read_data;
}

// ---------------- Public functions ----------------

// This functions reads and returns one specific output pin status from the PCA9534
bool PCA9534::read(uint8_t pin)
{
  uint8_t read_data = this->read();
  return !this->getBit(read_data, pin);
}

// This function turns on one specific output of the PCA9534
void PCA9534::turnOn(uint8_t pin)
{
  this->clearBit(pin);  // Logical 0 means HV on
  this->write();
}

// This function turns off one specific output pin on the PCA9534
void PCA9534::turnOff(uint8_t pin)
{
  this->setBit(pin);  // Logical 1 means HV off
  this->write();
}

// This function turns off all output pins on the PCA9534
void PCA9534::turnOffAll()
{
  this->HVControlPins = 0x00;
  this->write();
}

// This function turns on all output pins on the PCA9534
void PCA9534::turnOnAll()
{
  this->HVControlPins = 0xff;
  this->write();
}

// This function reads and returns all output states from the PCA9534
uint8_t PCA9534::readAllOn()
{
  return this->read();
}

// This function sets all HVs based on the code given via the PCA9534
void PCA9534::setAll(uint8_t code)
{
  // Loop through all HVOn pins and turn on HV depending on code
  this->HVControlPins = code;
  this->write();
}

// This function does the same as setAll, but turns on all pins seperately
// Staged startup is designed with the purpose of minimizing inrush currents
void PCA9534::setAllStagedStartup(uint8_t code)
{
  // Loop through all pins and turn on the pins depending on code
  for (int i = 0; i < TOTAL_NO_PINS; i++)
  {
    if (this->getBit(code, i))
    {
      // Turn on pin
      this->clearBit(i);
      wait_us(100000);  // Wait for a while before turning on a new pin
    }
    else
    {
      // Turn off pin
      this->setBit(i);
    }
    this->write();
  }
}