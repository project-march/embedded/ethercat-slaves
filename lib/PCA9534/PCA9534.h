#ifndef PCA9534_H
#define PCA9534_H

#include <mbed.h>
/**
 * @class PCA9534
 * @author J.Q. Bouman MARCH VI
 * This library is not complete yet. The current implementation is based around the use as an output.
 * For a complete implementation the input configuration needst to be added.
 * Also more type safety can be implemented by creating a custom pin enum.
 * 
 * This library can also be extended for the use of different ICs of the same family.
 * 
 * Datasheet: https://www.ti.com/lit/ds/symlink/pca9534.pdf
 */

#define INPUT_REGISTER 0x00
#define OUTPUT_REGISTER 0x01
#define POLARITY_REGISTER 0x02
#define CONFIGURATION_REGISTER 0x03

#define TOTAL_NO_PINS 8

/// Class to interface with the PCA9534 IO extender IC
class PCA9534
{
private:
  /// I2C interface
  I2C bus;
  /// Address based on hardware pins
  const uint8_t PCA9534_address = 0x21;
  /// In- and output configuration (0 is output, 1 is input (default: 1))
  const uint8_t configuration_setting = 0x00;
  /// Polarity inversion setting (0 regular, 1 inverse (default: 0))
  const uint8_t polarity_setting = 0x00;
  /// PCA read addres configured constructor
  uint8_t PCA9534_read;
  /// PCA write address configured in constructor
  uint8_t PCA9534_write;
  /// Status of the pints
  uint8_t HVControlPins;

  /// Set a specific pin in HVControlPins
  void setBit(uint8_t index);
  /// Clear a specific pin in HVControlPins
  void clearBit(uint8_t index);
  /// Get the value of a specific bit
  bool getBit(uint8_t word, uint8_t index);

  /// Write to interface (returns true when succesfull)
  bool write();
  /// Read value from bus
  uint8_t read();

public:
  /**
   * @brief Construct a new PCA9534 object
   * 
   * @param SDA_PIN Data pin for the I2C interface.
   * @param SCL_PIN Clock pin for the I2C interface.
   */
  PCA9534(PinName SDA_PIN, PinName SCL_PIN);

  /**
   * @brief Reads the current status of the pin in output configuration.
   * 
   * @param pin Number of the pin.
   * @return true is on.
   * @return false is off.
   */
  bool read(uint8_t pin);

  /**
   * @brief Turns a specific pin on.
   * 
   * @param pin Specific pin number.
   */
  void turnOn(uint8_t pin);

  /**
   * @brief Turns off a specific pin of IC.
   * 
   * @param pin 
   */
  void turnOff(uint8_t pin);

  /**
   * @brief Turn on all the output pins.
   */
  void turnOnAll();

  /**
   * @brief Turn off all the ouput pins.
   */
  void turnOffAll();

  /**
   * @brief Reads the current output configuration of all the pins.
   * 
   * @return uint8_t Representation of all pins as bits (directly copied from the IC).
   */
  uint8_t readAllOn();

  /**
   * @brief Set all the output pins according to the code.
   * 
   * @param code Every bit represents a pin.
   */
  void setAll(uint8_t code);

  /**
   * @brief Like the setAll function, all pins are set according to the code. However these changes are set with a small delay.
   * 
   * @param code Every bit represents a pin.
   * 
   * The delay is hard coded as 100ms.
   */
  void setAllStagedStartup(uint8_t code);
};

#endif  // PCA9534_H
