#ifndef THERMISTOR_
#define THERMISTOR_

#include <mbed.h>

/// Simple thermistor interface
class Thermistor
{
public:
  /// Constructor with PTV AnalogIn pin
  Thermistor(PinName pin);
  /// Start reading the ptc
  uint8_t read();

private:
  AnalogIn thermistorPin;

  const float referenceResistance = 100;
  const float cutoffResistance = 570;
  const float cuttoffVoltageRatio = cutoffResistance / (cutoffResistance + referenceResistance);
};

#endif  // THERMISTOR_
